        
        <footer class="footer clearfix">
			<div class="container">
            	<div class="row">
                    <div class="col-md-6">
                        <p>Made with Likewater  &copy; <?php echo date('Y'); ?><a href="#">suarezrem.com</a> All rights reserved</p>
                    </div><!--/.col-md-6-->
                    
                    <div class="col-md-6 footer-box">
                        <ul class="footer-icon">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-vk"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-xing"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul><!--/.team-icon-->
                    </div><!--/.footer-box-->
                    
                </div><!--/.row-->			
			</div><!--/.container-->
		</footer><!--/.footer--> 
        
		<!-- The javascript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="js/modernizr.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
<!--        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=YOUR_API_KEY&sensor=true"></script>-->

		<script type="text/javascript" src="js/jquery.easing.js"></script>
		<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="js/jQuery.BlackAndWhite.js"></script>
		<script type="text/javascript" src="js/isotope.pkgd.js"></script>
		<script type="text/javascript" src="js/jquery.fitvids.js"></script>
		<script type="text/javascript" src="js/contact.js"></script>
		<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
		<script type="text/javascript" src="js/owl.carousel.js"></script>
		<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
		<script type="text/javascript" src="js/jquery.nav.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo.js"></script>
		<script type="text/javascript" src="js/jquery.sticky.js"></script>
		<script type="text/javascript" src="js/ticker.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
     	
        <!--video background script start-->
		<script type="text/javascript" src="js/video.js"></script>
		<script type="text/javascript" src="js/bigvideo.js"></script>
        
		<!-- for collection slider -->
		<script type="text/javascript" src="js/product_slider.js"></script>
        


	</body>
</html>