<?php
/**********************************************
File	:init.php
Purpose	:Initial functions
Done On :28-07-2008
@Author	:Anith M.S
**********************************************/
ob_start();
session_start();
set_error_handler("customError");//setting error handler


require_once	('smarty/libs/Smarty.class.php');	err_status("Smarty Class Included");
require_once	('includes/db.php');				err_status("Db connected");
require_once	('classes/dbclass.php');			err_status("Db Class dbclass.php Included");
require_once	('classes/siteclass.php');			err_status("site Class siteclass.php Included");
require_once	('classes/functions.php');			err_status("function Class function.php Included");
require_once	('config.php');						err_status("Config File config.php");

//header("location:index.php");exit;
		
$site					=	"http://www.http://www.maxfit.com";
$smarty					= 	new Smarty;err_status("Smarty class object 'smarty' created");
$smarty->compile_check	= 	true;

$cls_db		=	new dbclass;err_status("Db class object 'cls_db' created");
$cls_site	=	new siteclass($site,"sebinja_member","cls_db","aim_user");err_status("site class object 'cls_site' created");
$cls_fun	=	new funclass("cls_db","cls_site");err_status("function class object 'cls_fun' created");

$smarty->assign("cls_db",$cls_db);
$smarty->assign("cls_site",$cls_site);
$smarty->assign("cls_fun",$cls_fun);

$smarty->assign("glb_site",$glb_site);

//Time Zone Setting for India
date_default_timezone_set('Asia/Calcutta');//PHP
mysql_query("SET time_zone = '+05:30'");//MY SQL


//setting error status
if($_REQUEST['debug']	==	"1") $_SESSION['debug']	=	"1";
if($_REQUEST['debug']	==	"0") $_SESSION['debug']	=	"0";

function header_view($title = "")
	{
		if(!$title)	$title	=	$site;
		define("_HEAD_TITLE",$title);
		include("header.php");
	}
	
function header_view1($title = "")
	{
		if(!$title)	$title	=	$site;
		define("_HEAD_TITLE",$title);
		include("header1.php");
	}
function customError($error_level,$error_message,$error_file,$error_line,$error_context)
 	{ 
	 	if($error_level	<>	8)
	 		{
				 if(isset($_SESSION['debug']))
			 		{
						echo "<br><b>error_level:</b> : $error_level";
					 	echo "<br><b>error_message:</b> : $error_message";
					 	echo "<br><b>error_file:</b> : $error_file";
					 	echo "<br><b>error_line:</b> : $error_line";
					}
			}
	}
function err_status($msg)
	{
		if($_SESSION['debug'])	echo "<br>".$msg;
	}
?>
