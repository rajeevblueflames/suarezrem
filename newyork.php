<?php
	require_once 'init.php';err_status("init.php included");  
	$newyork_arr	=	$cls_db->getdbcontents_sql("select * from suarezrem_newyork");
	$meta_contents	=	$cls_db->getdbcontents_sql("Select * from suarezrem_newyork_tags");

?>
	
<meta name="description" content="<?php echo $meta_contents[0]['meta_description']; ?>" />
<meta name="keywords" content="<?php echo $meta_contents[0]['meta_keyword']; ?>" />
<title><?php echo $meta_contents[0]['meta_title']; ?></title>
	
<?php
	header_view1("Suarezrem- Newyork");err_status("header included");

?>      
<div class="spacing40 clearfix clearboth"></div>


<!--Design START--> 
<div style="padding-bottom:0px;" id="newyork" class="content clearfix">
<div class="container">
	<div class="row">

		<div class="col-md-12" style="text-align: center;">
			<h3 class="content-title1" >- New York -</h3>
				<h3 class="boxs-title1">The Greatest City In The World</h3>
		</div><!--/.col-md-8-->
<?php
	if($newyork_arr)
	{
		foreach($newyork_arr as $key=>$val)
		{


?>
		<div class="spacing40 clearfix clearboth"></div>
		<div class="col-md-12" style="text-align: center">
			<img src="newyorkImages/<?php echo $val['image']; ?>" >
		</div>
		<div class="spacing40 clearfix clearboth"></div>

		<div class="col-md-12 boxs-relative">
			<div class="headingUnderline"><h3 class="boxs-title"><b><?php echo $val['title']; ?></b> </h3></div>
				<p class="textJustification">				

					<?php echo $val['description']; ?>
				</p>
							
		</div>
		<?php
		}
		}
		?>
		 
		</div><!--/.col-md-4-->
		<div class="spacing20 clearfix clearboth"></div>
	</div><!--/row-->
</div><!--/container-->
<!--Design END-->
<?php
	require_once("footer1.php");
?>
	
	