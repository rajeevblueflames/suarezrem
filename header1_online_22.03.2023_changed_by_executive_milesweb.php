<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="">
    
        <!-- Le styles -->
        <link href="https://www.suarezrem.com/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://www.suarezrem.com/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://www.suarezrem.com/css/owl.carousel.css" rel="stylesheet">
        <link href="https://www.suarezrem.com/css/magnific-popup.css" rel="stylesheet">
        <link href="https://www.suarezrem.com/css/style.css" rel="stylesheet">
        <link href="https://www.suarezrem.com/css/style.local.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://www.suarezrem.com/modal_popup/css/style.css" />
		<link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700" rel="stylesheet"> 
        <link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic,600italic,600&amp;subset=latin,greek-ext,cyrillic-ext,greek,vietnamese,cyrillic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    
        <!-- Favicons -->
        <link rel="shortcut icon" href="https://www.suarezrem.com/images/favicon.ico"><!--favicon image-->
    </head>

	<body>
    
    	<!-- Preloader -->
        <!--<div id="preloader">
            <div id="status"><img src="images/black-logo.png" alt="loading content.."></div>
        </div>-->
        
		<!--HOME START-->
		<div id="home" class="clearfix">
			<!--HEADER START-->
			<nav class="header home-section clearfix ">
					<div class="for-sticky" style="background-color: #000;">
						<div class="container nav-box">
							<div class="logo">
								<a href="https://www.suarezrem.com/">
									<img class="logo1" alt="logo" src="https://www.suarezrem.com/images/logo.png">
								</a>
							</div>
							<div class="menu-box hidden-xs hidden-sm">
								<ul class="navigation">
									<li class="current"><a href="https://www.suarezrem.com/#d#home">HOME</a></li>
                                    <li><a href="../newyork">NEWYORK</a></li>
                                    <li><a href="https://www.suarezrem.com/#development">TRANSACTION RESUME</a></li>
									<li><a href="../advantage">ADVANTAGE SUAREZ</a></li>
                                    <li><a href="https://www.suarezrem.com/#team1">TEAM</a></li>
									<li><a href="../allied-services">ALLIED SERVICES</a></li>
									<li><a href="../gallery">GALLERY</a></li>
									<li><a href="https://www.suarezrem.com/#contact">CONTACT</a></li>
								</ul>
							</div><!--/.menu-box-->
							<div class="box-mobile hidden-lg hidden-md">
								<div class="menu-btn" data-toggle="collapse" data-target=".nav-collapse">
									<span class="fa fa-bars"></span>
								</div>
								<ul class="nav-collapse mobile-menu hidden-lg hidden-md"></ul>
							</div><!--/.box-mobile-->	
						</div><!--/.container-->
					</div><!--/.for-sticky-->
			</nav><!--/.header-->
			<!--HEADER END-->
 
 			<!--VIDEO HOME START-->
<!--
			<div class="slider-box clearfix">
				<div class="slider home-slider clearfix" data-auto-play="8000">
				
                    <video autoplay loop muted width="100%" playsinline class="video-background">
                      <source src="video_playback.mp4" type="video/mp4">
                    </video>
                    
				</div>
			</div>
-->
			<!--VIDEO HOME END-->
            
		</div><!--/home end-->
		<!--HOME END--> 
        