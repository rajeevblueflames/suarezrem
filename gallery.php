<?php
	require_once 'init.php';err_status("init.php included");  
	$gallery_arr	=	$cls_db->getdbcontents_sql("select * from suarezrem_gallery");
	$meta_contents	=	$cls_db->getdbcontents_sql("Select * from suarezrem_gallery_tags");

?>
<meta name="description" content="<?php echo $meta_contents[0]['meta_description']; ?>" />
<meta name="keywords" content="<?php echo $meta_contents[0]['meta_keyword']; ?>" />
<title><?php echo $meta_contents[0]['meta_title']; ?></title>

<?php	
	header_view1("Suarezrem- Allied Services");err_status("header included");

?> 
<div class="spacing40 clearfix clearboth"></div>

	<!--Development Experience Start-->
		  <div style="padding-bottom:0px;" id="gallery" class="content clearfix">
        	<div class="container">
            	<div class="row">
                    <div class="col-md-12">
                    	<h3 class="content-title1" style="">Gallery </h3>
<!--						<h4 class="" style="text-align: center;">New York’s Premium Tenant Representative Boutique THE SUAREZ REM WAY</h4>-->
                    </div><!--/.col-md-8-->
                    
                    <div class="spacing40 clearfix clearboth"></div>
                    <?php
						if($gallery_arr)
						{
							foreach($gallery_arr as $key=>$val)
							{
					?>
					
        			<div class="col-md-12 boxs-relative">
<!--                        <h3 class="boxs-title">We believe</h3>-->
                        <img src="galleryImages/<?php echo $val['image'] ?>" alt="<?php echo $val['title']; ?>">
                        <p class="textJustification">
                        	<?php echo $val['title']; ?>
                        </p>
                    </div><!--/.col-md-4-->
					<?php
							}
						}
					?>
                    <hr>
                    <!--/.boxs-relative-->
                    <div>&nbsp;</div>
        	</div><!--/container-->
        	</div>  
		</div>
		<!--Development experience Ends-->
		<!--Team Start-->
<?php
	require_once("footer1.php");
?>
		