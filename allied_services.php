<?php
	require_once 'init.php';err_status("init.php included");  
	
	
	$services_arr	=	$cls_db->getdbcontents_sql("select * from suarezrem_services");
	$meta_contents	=	$cls_db->getdbcontents_sql("Select * from suarezrem_service_tags");

?> 
<meta name="description" content="<?php echo $meta_contents[0]['meta_description']; ?>" />
<meta name="keywords" content="<?php echo $meta_contents[0]['meta_keyword']; ?>" />
<title><?php echo $meta_contents[0]['meta_title']; ?></title>
<?php
	header_view1("Suarezrem- Allied Services");err_status("header included");
?>
<div class="spacing40 clearfix clearboth"></div>
<div style="padding-bottom:0px;" id="services" class="content clearfix">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="content-title1">Allied Services</h3>
				<h4 class="" style="text-align: center;line-height: 1.5;"><b>Our Allied Value-Add Services help our clients define their go-to-market strategies and achieve them though effective planning and consistent engagement.</b></h4>
			</div><!--/.col-md-8-->

			<div class="spacing20 clearfix clearboth"></div>
			<hr/>
	<?php
		if($services_arr)
		{
			foreach($services_arr as $key=>$val)
			{
	?>
			<div class="boxs-relative clearfix" style="background-color: #f9f5f5;">
				<div  class="col-md-4">
					<img src="serviceImages/<?php echo $val['image']; ?>" alt="<?php echo $val['title']; ?>" style="margin-top:10%;">
				</div>
				<div class="col-md-8">
					<div class="bordering clearfix">
						<h3 class="boxs-title1"><?php echo $val['title']; ?></h3>
						<p class="textJustification">
							<?php echo $val['description']; ?>
						</p>
					</div><!--/.bordering-->
					<div class="spacing20 clearfix"></div>
				</div><!--/.col-md-8-->
			</div>  
	<?php
			}
		}
	?>
		<div class="spacing20 clearfix"></div>
		</div>
	</div>
</div>
<?php
	require_once("footer1.php");
?>
	