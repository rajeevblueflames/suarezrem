        
        <footer class="footer clearfix">
			<div class="container">
            	<div class="row">
                    <div class="col-md-6">
                        <p>Made with Likewater  &copy; <?php echo date('Y'); ?><a href="#">suarezrem.com</a> All rights reserved</p>
                    </div><!--/.col-md-6-->
                    
                    <div class="col-md-6 footer-box">
                        <ul class="footer-icon">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-vk"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-xing"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul><!--/.team-icon-->
                    </div><!--/.footer-box-->
                    
                </div><!--/.row-->			
			</div><!--/.container-->
		</footer><!--/.footer--> 
        
		<!-- The javascript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="js/modernizr.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
<!--        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=YOUR_API_KEY&sensor=true"></script>-->

		<script type="text/javascript" src="js/jquery.easing.js"></script>
		<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script> 
        <script type="text/javascript" src="js/jQuery.BlackAndWhite.js"></script>
		<script type="text/javascript" src="js/isotope.pkgd.js"></script>
		<script type="text/javascript" src="js/jquery.fitvids.js"></script>
		<script type="text/javascript" src="js/contact.js"></script>
		<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
		<script type="text/javascript" src="js/owl.carousel.js"></script>
		<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
		<script type="text/javascript" src="js/jquery.nav.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo.js"></script>
		<script type="text/javascript" src="js/jquery.sticky.js"></script>
		<script type="text/javascript" src="js/ticker.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
     	
        <!--video background script start-->
		<script type="text/javascript" src="js/video.js"></script>
		<script type="text/javascript" src="js/bigvideo.js"></script>
        
		<!-- for collection slider -->
		<script type="text/javascript" src="js/product_slider.js"></script>
        

        <div class="audioStickyIcons">
            <img style="cursor:pointer; display:none;" id="audio_sound_yes" src="images/audio_sound_yes.png">
            <img style="cursor:pointer;" id="audio_sound_no" src="images/audio_sound_no.png">
        </div>

        <div style="display:none;">
            <audio id="audioplay" controls loop>
              <source src="/audio.ogg" type="audio/ogg">
              <source src="audio.mp3" type="audio/mpeg">
            </audio>
        </div>
        
        <script>
//            $(document).ready(function(){
//              $("#audio_sound_yes").click(function(){
//                $('#audioplay').get(0).play();
//                $('#audio_sound_yes').hide();
//                $('#audio_sound_no').fadeIn();
//              });
//        
//              $("#audio_sound_no").click(function(){
//                $('#audioplay').get(0).pause();
//                $('#audio_sound_no').hide();
//                $('#audio_sound_yes').fadeIn();
//              });
//            });
			$(document).ready(function(){
              $("#audio_sound_yes").click(function(){
				var bool = $("#audioplay").prop("muted");
        		$("#audioplay").prop("muted",!bool);
                $('#audio_sound_yes').hide();
                $('#audio_sound_no').fadeIn();
              });
        
              $("#audio_sound_no").click(function(){
				$('#audioplay').get(0).play();
				var bool = $("#audioplay").prop("muted");
        		$("#audioplay").prop("muted",!bool);
                $('#audio_sound_no').hide();
                $('#audio_sound_yes').fadeIn();
              });
            });
        </script>

        
		<script>
		//video background setting  
		var BV = new $.BigVideo({
			container: $('#home')
		});	
		BV.init();
		if (Modernizr.touch) {
			//replace the data-background into background image
			/*$(".img-bg").each(function() {
				var imG = $(this).data('background');
				$(this).css('background-image', "url('" + imG + "') "
	
				);
			});*/
			$(".img-bg").each(function() {
				var imG = $(this).data('background');
				$(this).css('background-image', "url('" + imG + "') "
	
				);
			});
		} else {
			$(".img-bg").each(function() {
				var imG = $(this).data('background');
				$(this).css('background-image', "url('" + imG + "') "
	
				);
			});
		
		}
		</script>
	</body>
</html>