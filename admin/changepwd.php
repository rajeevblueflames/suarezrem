<?php
/**************************************************************************************
Created by :Anith M.S
Created on :31-08-2009
Name       :changepwd.php
Purpose    :Password change option for admin
**************************************************************************************/
require_once 'init.php';err_status("init.php included");
header_view("Brook Wood - Change Password");err_status("header included");

$adminid			=	$_SESSION[$cls_site->get_sessname()];
$newpassword		=	$_POST["newpass"];
$oldpassword		=	$_POST["oldpass"];
$confirmpassword	=	$_POST["confirmpass"];
$change				=	$_POST["change"];
if(isset($change))
	{	
		err_status("inside of isset");
		
		$sql	=	"select * from suarezrem_admin where id='$adminid'";
		$res	=	mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res)>0)
			{
				$row			=	mysql_fetch_object($res);
				$password		=	stripslashes($row->password);
				if($password	==	$oldpassword)
					{			
						err_status("Admin is authenticated");
						if($newpassword	==	$confirmpassword)
							{
								err_status("New passwords got matched");
								$newpassword	= mysql_real_escape_string($newpassword);
								$sqlone			=	"update suarezrem_admin set password='$newpassword' where id='$adminid'";
								$resone			=	mysql_query($sqlone) or die(mysql_error());
								
								$_SESSION['sess_message']="<span class='label label-success'>Password changed successfully</span>";
								err_status("Updated successfully");		
								header("location:changepwd.php");
								exit;
													
							}	
						else
							{	
								err_status("Passwords does not match");
								$_SESSION['sess_message']="New password and confirm password does not match";
							}
					}
				else
					{	
						err_status("Old password entered was not correct");
						$_SESSION['sess_message']="Old password entered was not correct";
					}		
			}
	}	
	
if(isset($_POST["btn_cancel"]) || ($_POST["btn_ecancel"]))
	{
		header("location:changepwd.php");exit;		
	}
	
$smarty->assign("TPL_MESS",$_SESSION['sess_message']);	
$_SESSION['sess_message']="";	
$smarty->assign("TPL_MAX",20);					
$smarty->display('changepwd.tpl.html');
?>

