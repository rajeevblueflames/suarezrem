<?php
/**************************************************************************************
Created by :Sreejith vr
Created on :22-0-2012
Name       :productsimage.php
Purpose    :Listing Images
**************************************************************************************/
require_once 'init.php';err_status("init.php included");
header_view("Listing Gallery Images");err_status("header included");
$adminid		=	$_SESSION[$cls_site->get_sessname()];
$tpls			=	array();
$def_data		=	array();

$tpls["norecords"]				=	"No Records Found!";//page details
$tpls["heading"]				=	"Listing Gallery Images";
$tpls["pagename"]				=	"gallery.php";
$tpls["tplpagename"]			=	"gallery_manage.tpl.html";
//$tpls["parentpagename"]			=	"kids_care.php";

$tpls["addcaption"]				=	"Add Gallery Images";//add area

$tpls["edithead"]				=	"Edit Gallery Images";//edit area


$tpls["listcaption"]			=	"Title";//listing area
$tpls["pics"]					=	"Image";
$tpls["datedoj"]				=	"Date Added";
$tpls["ip"]						=	"IP";
$tpls["listoptions"]			=	"Options";

$tpls["imagepath"]				=	"../galleryImages";	

$tpls["parenttable"]			=	"suarezrem_gallery";	
$def_data["parenttable"]		=	"suarezrem_gallery";	
$def_data["table"]				=	"suarezrem_gallery";	
$tpls["imagepath"]				=	"../galleryImages";	


//*******************************************************************************************//

$edit			=	$_REQUEST["edit"];
$add			=	$_REQUEST["add"];
$cid			=	$_REQUEST["cid"];

if($cid	!=	"")
	$_SESSION["cid"] 	=	"";
if($_SESSION["cid"] 	==	"")
	$_SESSION["cid"]	=	$cid;
$cid			=	$_SESSION["cid"];
//
//$cat_id			=	$cls_db->getdbcontents_sql("SELECT * from ".$def_data["parenttable"]);
//$smarty->assign("cat_arr",$cat_id);
//if(!$cat_id)	
//	{
//		$_SESSION["sess_err"]	=	"<span class='label label-warning'>This category is not exist.</span>";
//		header("location:".$tpls["parentpagename"]);exit;		
//	}
	
$ip 			=	$_SERVER['REMOTE_ADDR'];
$date_added		=	"escape now() escape";
if(isset($_POST["addpres"]))	
	{
		header("location:".$tpls["pagename"]."?add=st");exit;
	}

if(isset($_POST["btn_cancel"]) || ($_POST["btn_ecancel"]))	
	{
		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;		
	}
	
if(isset($_POST["back"]))	
	{
		header("location:".$tpls["parentpagename"]);exit;
	}




if(isset($_POST['btn_delete']))
	{
		err_status("perform deletion");
		extract($_POST); err_status("deletion successfull");
		$del_arr	=	$_POST['checkone'];
		foreach($del_arr as $key=>$val)
			{
				$val_arr	=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where id='$val'");
				$img_val	=	$val_arr[0]['image'];
				unlink($tpls["imagepath"]."/".$img_val);	
				unlink($tpls["imagepath"]."/thumb/".$img_val);
			}
		$cls_site->db_delete_combo($def_data["table"],"id",$_POST['checkone']);
			$val_subarr	=	$cls_db->getdbcontents_sql("Select * from ".$def_data["subtable"]." where kid='$del_arr'");
		$cls_site->db_delete_combo($def_data["subtable"],"kid",$_POST['checkone']);
		header("location:".$tpls["pagename"]);
		
	}	
		
	
//..... Activate and inactivate status.................	
if($_REQUEST["statuschange"]!="")
	{
		 $cls_db->db_query("update ".$def_data["table"]." set status= !status  where id='".$_REQUEST["statuschange"]."'");
		header('location:'.$tpls["pagename"].'?n_page='.$_SESSION["newprg"]);exit;
	}
//*******************************************************************************************//
//..........EDIT VIEW..................	
if($edit !=	"" && !isset($_POST["btn_ecancel"]))
	{
		err_status("Got variable for edit");
		$edit_value		=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]."  where id='$edit'");				
		$smarty->assign("edit_arr",$edit_value);
		$galpics		=	$edit_value[0]["image"];
	}	
//*******************************************************************************************//
//..........PRESENTATION  ADDING..................	
if(isset($_POST["Submit"]))
	{
		err_status("inside of post of submit");
		foreach($_POST as $key	=>	$val) if(!is_array($val))	 $_POST[$key]	=	trim($val);extract($_POST);
		$ip 			=	$_SERVER['REMOTE_ADDR'];
		$date_added		=	"escape now() escape";
		//$date_added		=	date('Y-m-d');
	
		$cnt_img		=	count($title);
		for($i=0;$i<$cnt_img;$i++)
			{
				$title_val		=	$title[$i];
				$upload			=	$cls_site->create_upload(10,"jpeg,jpg,gif,png");
				$photos			=	$upload->copy_spec("photo_file",$tpls["imagepath"],$i,2);
				if($photos)		$upload->img_resize("150","120",$tpls["imagepath"]."/thumb");
				chmod($tpls["imagepath"]."/$photos",0755);	
				chmod($tpls["imagepath"]."/thumb/$photos",0755);	
				$pref_cnt		=	$cls_db->getdbcontents_sql("SELECT max(`preference`)+1 as maxpref FROM ".$def_data["table"]);
			    $pref_cnt		=	$pref_cnt[0]["maxpref"];	
				if($pref_cnt=="")
					{
						$pref_cnt=1;
					}
				if($title )
					{
						$fields			=	"title,image,ip,date_added,preference";
						$values			=	"title_val,photos,ip,date_added,pref_cnt";
						$add_id			=	$cls_db->db_insert($def_data["table"],$fields,$values);
						$cls_log->log_insert($def_data["table"],$add_id,$fields);
						$_SESSION["sess_err"]	=	"<span class='label label-success'>Gallery image details added successfully</span>";
					}
				else
					$_SESSION["sess_err"]	=	"<span class='label label-warning'>Sorry, there was a problem uploading some of your file(s).</span>";
			}
		header("location:".$tpls["pagename"]);exit;		
			
	}	
//*******************************************************************************************//
//..........PRESENTATION  EDITING..................
if(isset($_POST["Update"]))
	{
		err_status("inside of post of Update");
		foreach($_POST as $key	=>	$val) if(!is_array($val))	 $_POST[$key]	=	trim($val);extract($_POST);
		if($etitle == "")	$_SESSION["sess_err"]	=	"<span class='label label-warning'>Please enter all details correctly</span>"; // || $photo == "" 
		else
			{
				$add_arr		=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where title='$etitle' and id!='$edit'");
				if(!$add_arr)
					{
						$ip 			=	$_SERVER['REMOTE_ADDR'];
						$upload			=	$cls_site->create_upload(10,"jpeg,jpg,gif,png");
						$image			=	$upload->copy("photo",$tpls["imagepath"],2);
						if($image)		$upload->img_resize("100","120",$tpls["imagepath"]."/thumb");
						chmod($tpls["imagepath"]."/$image",0755);	
						chmod($tpls["imagepath"]."/thumb/$image",0755);	
						
						if(!$image)		$image		=	$galpics;
						else
							{
								unlink($tpls["imagepath"]."/".$galpics);	
								unlink($tpls["imagepath"]."/thumb/".$galpics);
							}
						$etitle			=	mysql_real_escape_string($etitle);
						$fields			=	"image,title,ip,date_added";			
						$args			=	"image='$image',title='$etitle',ip='$ip' where id='$edit'";
						//	$cls_log->log_setupdate($def_data["table"],$edit,$fields);
						$add_id			=	$cls_db->db_update($def_data["table"],$args,1);
						$_SESSION["sess_err"]	=	"<span class='label label-success'>".$tpls["addcaption"]." updated successfully</span>";
						header('location:'.$tpls["pagename"].'?n_page='.$_SESSION["newprg"]);exit;
					}
				else
					$_SESSION["sess_err"]	=	"span class='label label-warning'>This ".$tpls["listcaption"]." is already exist!</span>";
			}
	}

//*******************************************************************************************//

if(isset($_REQUEST['down']))
	{		
		$down_id		=	$_REQUEST['down']; 
		$arr		=	$cls_db->getdbcontents_sql("select * from ".$def_data["table"]." where id='$down_id'");
		$down_pref	=	$arr[0][preference];  
		$result3	=	$cls_db->getdbcontents_sql("select max(preference) from ".$def_data["table"]." where preference<'$down_pref'   ");
		$new_pref 	=	$result3[0]['max(preference)'];
		$result4	=	$cls_db->getdbcontents_sql("select * from  ".$def_data["table"]." where preference='$new_pref'");
		$new_id		= 	$result4[0]['id'];
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$new_pref' where id='$down_id'");
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$down_pref' where id='$new_id'");
		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;	
	}
	
//priority down	
if(isset($_REQUEST['up']))
	{		
		$up_id		=	$_REQUEST['up']; 
		$arr=$cls_db->getdbcontents_sql("select * from ".$def_data["table"]." where id='$up_id'");
		$up_pref		=	$arr[0][preference];  
		$result3		=	$cls_db->getdbcontents_sql("select min(preference) from ".$def_data["table"]." where preference>'$up_pref'   ");
		$new_pref 		=	$result3[0]['min(preference)'];
		$result4		=	$cls_db->getdbcontents_sql("select * from  ".$def_data["table"]." where preference='$new_pref'");
		$new_id			= 	$result4[0]['id'];
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$new_pref' where id='$up_id'");
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$up_pref' where id='$new_id'");		
		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;	
	}	
$preference_arr	=	$cls_db->getdbcontents_sql("select max(preference) as max_pref,min(preference) as min_pref from ".$def_data["table"]."");
$smarty->assign('maxs',$preference_arr[0]['max_pref']);
$smarty->assign('mins',$preference_arr[0]['min_pref']);



$sql			=	"Select * from ".$def_data["table"]." order by preference desc";
$cnt_dat		=	$cls_db->getdbcount_sql($sql);
$spag11			=	$cls_site->create_paging("n_page",$cnt_dat,$global_pg_limit);
$link11			=	$spag11->s_get_links($_REQUEST["n_page"]);
$limi11			=	$spag11->s_get_limit($_REQUEST["n_page"]);	
$smarty->assign("paging",$link11);
$smarty->assign("cnt_dat",$cnt_dat);
$data_arr		=	$cls_db->getdbcontents_sql($sql.$limi11);
$smarty->assign('data_arr',$data_arr);
if(!$data_arr)	$smarty->assign("TPL_MESS","No Gallery image(s) available");
if($_SESSION["sess_err"])
	{
		$smarty->assign("TPL_MESS",$_SESSION["sess_err"]);
		$_SESSION["sess_err"]="";
	}
$smarty->assign("tpls",$tpls);
$smarty->display($tpls["tplpagename"]);
?>	
