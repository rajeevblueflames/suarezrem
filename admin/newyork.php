<?php
/**************************************************************************************
Created by :Sreejith vr
Created on :22-0-2012
Name       :newyork.php
Purpose    :Listing newyork Categories
**************************************************************************************/
require_once 'init.php';err_status("init.php included");
header_view("Newyork");err_status("header included");
$adminid		=	$_SESSION[$cls_site->get_sessname()];
$tpls			=	array();
$def_data		=	array();
$edit			=	$_REQUEST["edit"];
$add			=	$_REQUEST["add"];
$ip 			=	$_SERVER['REMOTE_ADDR'];
$date_added		=	"escape now() escape";
// $category_pro        =	$cls_db->getdbcontents_sql("SELECT * FROM suarezrem_newyork p INNER JOIN suarezrem_category c on p.cid=c.id");
// $smarty->assign("category_pro",$category_pro);

$tpls["norecords"]				=	"No Records Found!";//page details
$tpls["heading"]				=	"Manage Newyork";
$tpls["pagename"]				=	"newyork_manage.php";
$tpls["tplpagename"]			=	"newyork.tpl.html";
$tpls["childpagename"]			=	"newyorkimages.php";
$tpls["pagename1"]				=	"newyork.php";

$tpls["addcaption"]				=	"Newyork";//add area

$tpls["edithead"]				=	"Newyork";//edit area


$tpls["listcaption"]			=	"Newyork";//listing area
$tpls["pics"]					=	"Image";
$tpls["datedoj"]				=	"Date Added";
$tpls["ip"]						=	"IP";
$tpls["listoptions"]			=	"Options";

$tpls["width"]					=	"800";
$tpls["height"]					=	"800";

$tpls["subimagepath"]				=	"../newyorkImages/newyorkSubimages";	
$tpls["imagepath"]				=	"../newyorkImages";
//*******DATA BASE************************************************************************************//
$def_data["table"]				=	"suarezrem_newyork";	

$def_data["subtable"]			=	"suarezrem_newyork_images";

//*******************************************************************************************//



if($_REQUEST['n_page']	!=	"")  $_SESSION["newprg"]	=  $_REQUEST['n_page'];
if(isset($_POST["addpres"]))	
	{
		header("location:".$tpls["pagename"]."?add=st");exit;
	}
if(isset($_POST["btn_cancel"]) || ($_POST["btn_ecancel"]))	
	{
		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;		
	}
	
	
	
//..... Activate and inactivate status.................	
if($_REQUEST["statuschange"]!="")
	{
		 $cls_db->db_query("update ".$def_data["table"]." set status= !status  where id='".$_REQUEST["statuschange"]."'");
		header('location:'.$tpls["pagename"].'?n_page='.$_SESSION["newprg"]);exit;
	}
//*******************************************************************************************//
//..........EDIT VIEW..................	
if($edit !=	"" && !isset($_POST["btn_ecancel"]))
	{
		err_status("Got variable for edit");
		$edit_value		=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where id='$edit'");				
		$smarty->assign("edit_arr",$edit_value);
		$galpics		=	$edit_value[0]["image"];
	}	
//*******************************************************************************************//


if(isset($_POST['btn_delete']))
	{	
		err_status("perform deletion");
		extract($_POST); err_status("deletion successfull");
		$del_arr	=	$_POST['checkone'];
		foreach($del_arr as $key=>$val)
			{	
				
				
		///////********delete_category************************
				$sub_arr		=	$cls_db->getdbcontents_sql("Select * from suarezrem_newyork_images where cid='$val'");
				$sub_val		=	$sub_arr[0]['image'];
				$val_arr		=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where id='$val'");
				$img_val		=	$val_arr[0]['image'];
				unlink($tpls["subimagepath"]."/".$sub_val);	
				unlink($tpls["subimagepath"]."/thumb/".$sub_val);
				unlink($tpls["imagepath"]."/".$img_val);	
				unlink($tpls["imagepath"]."/thumb/".$img_val);
				
		////////**********delete_category***************************
		
			}
		$cls_site->db_delete_combo($def_data["table"],"id",$_POST['checkone']);
		$cls_site->db_delete_combo($def_data["subtable"],"id",$_POST['checkone']);
		
		
		header("location:".$tpls["pagename1"]);
		
	}	



if(isset($_REQUEST['up']))
	{	
	
		$up_id		=	$_REQUEST['up']; 
		$arr=$cls_db->getdbcontents_sql("select * from ".$def_data["table"]." where id='$up_id'");
		$up_pref		=	$arr[0]['preference'];  
		$result3		=	$cls_db->getdbcontents_sql("select min(preference) from ".$def_data["table"]." where preference>'$up_pref'");
		$new_pref 		=	$result3[0]['min(preference)'];
		$result4		=	$cls_db->getdbcontents_sql("select * from  ".$def_data["table"]." where preference='$new_pref'");
		$new_id			= 	$result4[0]['id'];
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$new_pref' where id='$up_id'");
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$up_pref' where id='$new_id'");		
		header('location:'.$tpls["pagename1"]);exit;	
	}
if(isset($_REQUEST['down']))
	{		

		$down_id		=	$_REQUEST['down']; 
		$arr		=	$cls_db->getdbcontents_sql("select * from ".$def_data["table"]." where id='$down_id'");
		$down_pref	=	$arr[0]['preference'];  
		$result3	=	$cls_db->getdbcontents_sql("select max(preference) from ".$def_data["table"]."  where preference<'$down_pref'");
		$new_pref 	=	$result3[0]['max(preference)'];
		$result4	=	$cls_db->getdbcontents_sql("select * from  ".$def_data["table"]." where preference='$new_pref'");
		$new_id		= 	$result4[0]['id'];
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$new_pref' where id='$down_id'");
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$down_pref' where id='$new_id'");
		header('location:'.$tpls["pagename1"]);exit;	
	}	
$preference_arr	=	$cls_db->getdbcontents_sql("select max(preference) as max_pref,min(preference) as min_pref from ".$def_data["table"]);
$smarty->assign('maxs',$preference_arr[0]['max_pref']);
$smarty->assign('mins',$preference_arr[0]['min_pref']);

$sql			=	"Select * from ".$def_data["table"]." order by preference desc";
$sql2			=	"SELECT * FROM suarezrem_newyork";
$cnt_dat		=	$cls_db->getdbcount_sql($sql);
$spag11			=	$cls_site->create_paging("n_page",$cnt_dat,$global_pg_limit);
$link11			=	$spag11->s_get_links($_REQUEST["n_page"]);
$limi11			=	$spag11->s_get_limit($_REQUEST["n_page"]);	
$smarty->assign("paging",$link11);
$smarty->assign("cnt_dat",$cnt_dat);
$data_arr		=	$cls_db->getdbcontents_sql($sql.$limi11);
$data_arr2		=	$cls_db->getdbcontents_sql($sql2);
$smarty->assign('data_arr',$data_arr);
$smarty->assign('data_arr2',$data_arr2);
if(!$data_arr)	$smarty->assign("TPL_MESS","No ".$tpls["listcaption"]." details available!");
if($_SESSION["sess_err"])
	{
		$smarty->assign("TPL_MESS",$_SESSION["sess_err"]);
		$_SESSION["sess_err"]="";
	}
$smarty->assign("tpls",$tpls);
$smarty->display($tpls["tplpagename"]);
?>	
