<?php
/**************************************************************************************
Page Name	:	init.php
Created by 	:	Anith M.S
Created on 	:	31-08-2009
Purpose		:	Initial functions
****************************************************************************************/
ob_start();
session_start();
set_error_handler("customError");//setting error handler
date_default_timezone_set('America/Los_Angeles');//setting time zone

require_once	('config.php');							err_status("Config Included");
require_once	('../includes/db.php');					err_status("Db connected");
require_once	('../smarty/libs/Smarty.class.php');	err_status("Smarty Class Included");
require_once	('../classes/dbclass.php');				err_status("Db Class dbclass.php Included");
require_once	('../classes/siteclass.php');			err_status("site Class siteclass.php Included");
require_once	('../classes/functions.php');			err_status("function Class function.php Included");

$smarty					= 	new Smarty;err_status("Smarty class object 'smarty' created");
$smarty->compile_check	= 	true;

$cls_db		=	new dbclass;err_status("Db class object 'cls_db' created");
$cls_site	=	new siteclass("suarezrem.com","sess_admin","cls_db","suarezrem_admin");err_status("site class object 'cls_site' created");
$cls_fun	=	new funclass("cls_db","cls_site");err_status("function class object 'cls_fun' created");
$cls_log	=	new logclass("cls_db","cls_site","suarezrem_adminlog");err_status("log class object 'cls_log' created");
$cls_db->setsite("cls_site");
$cls_db->setlog("cls_log");
$cls_site->setlog("cls_log");


$smarty->assign("cls_db",$cls_db);
$smarty->assign("cls_site",$cls_site);
$smarty->assign("cls_fun",$cls_fun);
$smarty->assign("cls_log",$cls_log);

//setting error status
if($_REQUEST['debug']	==	"1") $_SESSION['debug']	=	"1";
if($_REQUEST['debug']	==	"0") $_SESSION['debug']	=	"0";

function header_view($title="")
	{
		if(!$title)	$title	=	"brookwood.in";
		define("_HEAD_TITLE",$title);
		include("header.php");
	}
function header1_view($title="")
	{
		if(!$title)	$title	=	"brookwood.in";
		define("_HEAD_TITLE",$title);
		include("header1.php");
	}
function customError($error_level,$error_message,$error_file,$error_line,$error_context)
 	{ 
	 	if($error_level	<>	8)
	 		{
				 if(isset($_SESSION['debug']))

			 		{
						echo "<br><b>error_level:</b> : $error_level";
					 	echo "<br><b>error_message:</b> : $error_message";
					 	echo "<br><b>error_file:</b> : $error_file";
					 	echo "<br><b>error_line:</b> : $error_line";
					}
			}
	}
function err_status($msg)
	{
		if($_SESSION['debug'])	echo "<br>".$msg;
	}
?>
