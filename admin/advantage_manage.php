<?php
/**************************************************************************************
Created by :Reny
Created on :17-10-2012
Name       :advantage.php
Purpose    :Manage advantages
**************************************************************************************/
require_once 'init.php';err_status("init.php included");
header_view($site." - Advantage");err_status("header included");
	   
$arr=$cls_db->getdbcontents_sql("select * from suarezrem_advantage");
$smarty->assign('array',$arr);
$msg1="Advantage";
$smarty->assign('msg1',$msg1);
if(!$arr)
		{
			$msg="No Advantage(s) available";
			$smarty->assign('msg',$msg);
			
			
		}
/*if($_POST['btn_delete'])
		{
			$cls_site->db_delete_combo("suarezrem_advantage","id",$_POST['checkone']);
			$_SESSION["sess_err"]	=	"<span class='label label-success'>Deleted successfully</span>";
			header("location:advantage.php");
			
		}	*/
$title = "Advantage";

$smarty->assign('title',$title);

$meta_details	=	$cls_db->getdbcontents_sql("Select * from suarezrem_advantage_tags where id =1");
$smarty->assign('meta_details',$meta_details);
				
if($_POST['cancel'])
		{
			header('location:advantage.php');
			exit;
		}

$edit=$_REQUEST['edit'];
$data_arr1=$cls_db->getdbcontents_sql("select * from suarezrem_advantage where id='$edit' ");
$smarty->assign('data_arr1',$data_arr1);
			
$oldimage	=	$data_arr1[0]['image'];
$tb_meta	= "suarezrem_advantage_tags";

if($data_arr1)
{
	if($_POST['update'])
		{
			$edescription=$_POST['edescription'];

			$etitle=$_POST['etitle'];
			$upload			=	$cls_site->create_upload(10,"jpeg,jpg,gif,png");
			$image			=	$upload->copy("photo","../advantageImages",2);
			if($image)
				{


				$upload->img_resize("100","120","../advantageImages/thumb"); 
				chmod("../advantageImages/$image",0755);	
				chmod("../advantageImages/thumb/$image",0755);
				unlink("../advantageImages/$oldimage");
				unlink("../advantageImages/thumb/$oldimage");

				}
			else
				{	                                          
					$image	=	$oldimage;			
				}
//				$check=$cls_db->getdbcontents_sql("select * from suarezrem_advantage where image='$image' and id!=$edit");
//				if(!$check)
//					{
					 $etitle		= mysql_real_escape_string($etitle);
					 $edescription	= mysql_real_escape_string($edescription);

					 $args="title='$etitle',image='$image',content='$edescription' where id=$edit";
					//echo "update".$up="update suarezrem_advantage set title='$title',image='$image',description='$description',description2='$description2' where id=$edit ";exit;
					 $cls_db->db_update("suarezrem_advantage",$args);

					
					$_SESSION['sess_err']="<span class='label label-success'>Details updated sucesssfully</span>";

					header('location:advantage.php');
					exit;
			}
}
else
{
				
if($_POST['add'])
		{
			foreach($_POST as $key	=>	$val) if(!is_array($val))	 $_POST[$key]	=	trim($val);extract($_POST);
			
		/*	$cnt_img		=	count($_FILES['image']['name']);
			
			for($i=0;$i<$cnt_img;$i++)
				{
			*/
			$title=$_POST['title'];
			$description=$_POST['description'];
			$description2=$_POST['description2'];
			$image=$_POST['image'];
			
				$date_added			=	"escape now() escape";
				$ip 				=	$_SERVER['REMOTE_ADDR'];
			
					$upload			=	$cls_site->create_upload(10,"jpeg,jpg,gif,png");
					$image			=	$upload->copy("image","../advantageImages",2);
					$upload->img_resize("100","120","../advantageImages/thumb"); 
					chmod("../advantageImages/$image",0755);	
					chmod("../advantageImages/thumb/$image",0755);
					$date_added="escape now() escape";
					$pref_cnt	=	$cls_db->getdbcontents_sql("SELECT max(`preference`)+1 as maxpref FROM suarezrem_homeimage");
					$pref_cnt	=	$pref_cnt[0]["maxpref"];	
					//$date=date('Y-m-d');		
						if( $title && $description )//&& $image)
							{
								$fields			=	"title,image,content,,ip,date_added";
								$values			=	"title,image,description,ip,date_added";	
							//echo $ins= "insert into suarezrem_advantage($fields) values('$title','$image','$description','$ip','$date')";
								$add_id			=	$cls_db->db_insert("suarezrem_advantage",$fields,$values);
								$cls_log->log_insert($def_data["table"],$add_id,$fields);
							
								$fields			=	"meta_title,meta_keyword,meta_description";
								$values			=	"meta_title,meta_keyword,meta_description";		
								$add_id			=	$cls_db->db_insert($tb_meta,$fields,$values);
								$cls_log->log_insert($def_data["table"],$add_id,$fields);
								$_SESSION["sess_err"]	=	"<span class='label label-success'>Image details added successfully</span>";
							}
						else
						{
							$_SESSION["sess_err"]	=	"<span class='label label-warning'>Sorry, there was a problem uploading some of your file(s)</span>";
							header('location:advantage_manage.php?add=1');
						}
						
										
				//}
							
				
			 header('location:advantage.php');
			 exit;
		}
}
$statuschange		=	$_REQUEST["statuschange"];
if($statuschange)
		{
			$args	=	"status = !status where id='$statuschange' ";
			$cls_db->db_update("suarezrem_advantage",$args);	
			header('location:advantage.php');exit;		
		}

				
if($_SESSION['sess_err'])
		{
			$smarty->assign("TPL_MESS",$_SESSION['sess_err']);	
			$_SESSION['sess_err']="";	
		}	
				
$smarty->display('advantage_manage.tpl.html');
?>