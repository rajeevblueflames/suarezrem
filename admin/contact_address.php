<?php

/**************************************************************************************

Created by :Reny

Created on :15-11-2012

Name       :contact_address.php

Purpose    :Listing contact address details

**************************************************************************************/

require_once 'init.php';err_status("init.php included");

header_view("Contact Address");err_status("header included");

$adminid		=	$_SESSION[$cls_site->get_sessname()];

$tpls			=	array();

$def_data		=	array();

$sub_data		=	array();

$edit			=	$_REQUEST["edit"];

$add			=	$_REQUEST["add"];

$ip 			=	$_SERVER['REMOTE_ADDR'];

$date_added		=	"escape now() escape";



$tpls["norecords"]				=	"No Records Found!";//page details

$tpls["heading"]				=	"Manage Contact Address";

$tpls["pagename"]				=	"contact_address_manage.php";

$tpls["tplpagename"]			=	"contact_address.tpl.html";





$tpls["addcaption"]				=	"Contact Address";//add area



$tpls["edithead"]				=	"Contact Address";//edit area





$tpls["listcaption"]			=	"Contact Address";//listing area

$tpls["caption"]				=	"Large Title";

$tpls["datedoj"]				=	"Date Added";

$tpls["ip"]						=	"IP";

$tpls["listoptions"]			=	"Options";





//*******DATA BASE************************************************************************************//

$def_data["table"]				=	"suarezrem_cont_address";





//*******************************************************************************************//







if($_REQUEST['n_page']	!=	"")  $_SESSION["newprg"]	=  $_REQUEST['n_page'];

if(isset($_POST["addpres"]))	

	{

		header("location:".$tpls["pagename"]."?add=st");exit;

	}

if(isset($_POST["btn_cancel"]) || ($_POST["btn_ecancel"]))	

	{

		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;		

	}

	

	

	

//*******************************************************************************************//

//..........EDIT VIEW..................	

if($edit !=	"" && !isset($_POST["btn_ecancel"]))

	{

		err_status("Got variable for edit");

		$edit_value		=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where id='$edit'");				

		$smarty->assign("edit_arr",$edit_value);

		$galpics		=	$edit_value[0]["image"];

	}	

//*******************************************************************************************//

//..........PRESENTATION  ADDING..................	

if(isset($_POST["Submit"]))

	{

		err_status("inside of post of submit");

		foreach($_POST as $key	=>	$val) if(!is_array($val))	 $_POST[$key]	=	trim($val);extract($_POST);

		$ip 					=	$_SERVER['REMOTE_ADDR'];

		$date_added				=	"escape now() escape";

		$fields					=	"phone,address_title,address,fax,mobno1,mobno2,email,date_added";

		$values					=	"phone,add_title,address,fax,mobno1,mobno2,email,date_added";		

		$add_id					=	$cls_db->db_insert($def_data["table"],$fields,$values);

		$cls_log->log_insert($def_data["table"],$add_id,$fields);

		$_SESSION["sess_err"]	=	"<span class='label label-success'>Details added successfully</span>";

		header("location:".$tpls["pagename"]);exit;	

	}	

//*******************************************************************************************//

//..........PRESENTATION  EDITING..................	

if(isset($_POST["update"]))

	{

		err_status("inside of post of update");

		foreach($_POST as $key	=>	$val) if(!is_array($val))	 $_POST[$key]	=	trim($val);extract($_POST);

		$date_added				=	"escape now() escape";

		$args			=	"phone='$phone',address_title='$add_title2',address='$address',fax='$fax',mobno1='$mobno1',mobno2='$mobno2',email='$email',date_added=now() where id='$edit'";

		$add_id			=	$cls_db->db_update($def_data["table"],$args);

		$_SESSION["sess_err"]	=	"<span class='label label-success'>Details updated successfully</span>";

		header('location:'.$tpls["pagename"].'?n_page='.$_SESSION["newprg"]);exit;

	}

//*******************************************************************************************//





echo $sql			=	"Select * from ".$def_data["table"]."";

$cnt_dat		=	$cls_db->getdbcount_sql($sql);

$spag11			=	$cls_site->create_paging("n_page",$cnt_dat,$global_pg_limit);

$link11			=	$spag11->s_get_links($_REQUEST["n_page"]);

$limi11			=	$spag11->s_get_limit($_REQUEST["n_page"]);	

$smarty->assign("paging",$link11);

$smarty->assign("cnt_dat",$cnt_dat);

$data_arr		=	$cls_db->getdbcontents_sql($sql.$limi11);

$smarty->assign('data_arr',$data_arr);

if(!$data_arr)	$smarty->assign("TPL_MESS","<span class='label label-info'>No ".$tpls["listcaption"]." details available!</span>");


$smarty->assign("TPL_MESS",$_SESSION["sess_err"]);

$_SESSION["sess_err"]="";

$smarty->assign("tpls",$tpls);

$smarty->display($tpls["tplpagename"]);

?>	

