<?php /* Smarty version 2.6.19, created on 2021-12-29 00:31:32
         compiled from index.tpl.html */ ?>
<?php echo '
<script language="javascript" type="text/javascript">
function validate()
	{
		if(document.form1.txt_username.value	==	"")
			{
				alert("Please enter username");
				document.form1.txt_username.focus();
				return false;
			}	
			if(document.form1.txt_password.value	==	"")
			{
				alert("Please enter password");
				document.form1.txt_password.focus();
				return false;
			}
			return true;	
	}
	
</script>
'; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Suarezrem - Admin</title>
<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen, projection"/>

<!-- Bootstrap -->
<link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="vendors/nprogress/nprogress.css" rel="stylesheet">
<!-- Animate.css -->
<link href="vendors/animate.css/animate.min.css" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="build/css/custom.min.css" rel="stylesheet">

</head>
<body class="login">
  <div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
      <div class="animate form login_form">
        <section class="login_content">
          <form id="form1" name="form1" method="post" action="">
            <h1>Login Form</h1>
            <?php if ($this->_tpl_vars['TPL_MESSAGE']): ?><div style="color:#F00;"><?php echo $this->_tpl_vars['TPL_MESSAGE']; ?>
</div><?php endif; ?>
            <br>
            <div>
              <input type="text" name="txt_username" id="txt_username" class="form-control" placeholder="Username" required />
            </div>
            <div>
              <input type="password" name="txt_password" id="txt_password" class="form-control" placeholder="Password" required />
            </div>
            <div>
              <input name="login" type="submit" class="btn btn-default submit" value="Log in" onClick="return validate();"/>
              <!--<a class="reset_pass" href="#">Lost your password?</a>-->
            </div>

            <div class="clearfix"></div>

            <div class="separator">
              <!--<p class="change_link">New to site?
                <a href="#signup" class="to_register"> Create Account </a>
              </p>-->

              <div class="clearfix"></div>

              <div>
                <img src="../images/logo.png" style="max-width:250px;"  />
                <br>
                <br>
                <p>Copyright &copy; <?php echo $this->_tpl_vars['year']; ?>
 suarezrem.com All Rights Reserved.</p>
              </div>
            </div>
          </form>
          <?php echo '
          <script language="javascript" type="text/javascript">
          document.form1.txt_username.focus();
          </script> 
          '; ?>
                    
        </section>
      </div>

      <div id="register" class="animate form registration_form">
        <section class="login_content">
          <form id="form1" name="form1" method="post" action="">
            <h1>Create Account</h1>
            <div>
              <input type="text" name="txt_username" id="txt_username" class="form-control" placeholder="Username" required />
            </div>
            <div>
              <input type="email" name="email" id="email" class="form-control" placeholder="Email" required />
            </div>
            <div>
              <input type="password" name="txt_password" id="txt_password" class="form-control" placeholder="Password" required />
            </div>
            <div style="align-self: center; padding-left: 90px;">
              <input name="submit" type="submit" class="btn btn-default submit" value="Submit" onClick=""/>
            </div>

            <div class="clearfix"></div>

            <div class="separator">
              <p class="change_link">Already a member ?
                <a href="#signin" class="to_register"> Log in </a>
              </p>

              <div class="clearfix"></div>

              <div>
                <img src="images/logo/black-logo.png"  />
                <br>
                <br>
                <p>Copyright &copy; <?php echo $this->_tpl_vars['year']; ?>
 suarezrem.com All Rights Reserved.</p>
              </div>
            </div>
          </form>
        </section>
      </div>
    </div>
  </div>
</body>
</html>