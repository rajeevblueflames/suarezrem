<?php /* Smarty version 2.6.19, created on 2021-09-14 02:35:22
         compiled from contactus.tpl.html */ ?>
<?php echo '

<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


<link href="css/stylecal.css" rel="stylesheet" type="text/css" />

<script language="javascript" src="js/calendor.js"></script>

<script language="javascript" type="text/javascript">

function validate_add()

	{

		if(document.add_form.subcategory.value	==	"")

			{

				alert("Please select Category");

				document.add_form.subcategory.focus();

				return false;

			}	

		if(document.add_form.txt_item.value	==	"")

			{

				alert("Please enter Item");

				document.add_form.txt_item.focus();

				return false;

			}

		if(document.add_form.txt_price.value	==	"")

			{

				alert("Please enter Price");

				document.add_form.txt_price.focus();

				return false;

			}

		if(document.add_form.txt_description.value	==	"")

			{

				alert("Please enter description");

				document.add_form.txt_description.focus();

				return false;

			}

			return true;	

	}

function validate_edit()

	{

		if(document.frm_edit.subcategory.value	==	"")

			{

				alert("Please enter subcategory");

				document.frm_edit.subcategory.focus();

				return false;

			}	

		if(document.frm_edit.items.value	==	"")

			{

				alert("Please enter Item");

				document.frm_edit.items.focus();

				return false;

			}

			if(document.frm_edit.price.value	==	"")

			{

				alert("Please enter Price");

				document.frm_edit.price.focus();

				return false;

			}	

			if(document.frm_edit.txt_description.value	==	"")

			{

				alert("Please enter Description");

				document.frm_edit.txt_description.focus();

				return false;

			}	

			return true;	

	}



function getscat(ids)

	{ 

		getAjax(\'ajax_check.php?action=subcategory&ids=\'+ids,\'divscat\');

	}



var imcnt	=	0;

function addmoreCat()

	{ 

		var tbody = document.getElementById("addCat").getElementsByTagName("tbody")[0];

		imcnt++;

		var row1 = document.createElement("TR");			

		var row2 = document.createElement("TR");			

		trimid		=	\'tr_im\'+imcnt;		

		trimid1		=	\'1tr_im\'+imcnt;		

		row1.setAttribute(\'id\',trimid);

		row2.setAttribute(\'id\',trimid1);

		rmvimg	=	"<a href=\\"javascript:;\\"  class=\'addmoreText\' onclick=\\"removecity(\'" + trimid +"\',\'" + trimid1 + "\')\\">&nbsp;Remove</a>";

		var cell0= document.createElement("TD");

		var cell2= document.createElement("TD");

		cell0.setAttribute(\'valign\',\'top\');

		cell2.setAttribute(\'valign\',\'top\');

		cell0.innerHTML	=	" Dealers City / Area wise";

		

		cell2.innerHTML	=	"&nbsp;"+rmvimg;

		row1.appendChild(cell0);	

		row1.appendChild(cell2);

		tbody.appendChild(row1);	

	}

function checkAll()

{

	if(document.form_view.checkone)

		{

			document.form_view.checkone.checked=document.form_view.checkall.checked;

			for(i=0;i<document.form_view.checkone.length;i++)

			document.form_view.checkone[i].checked=document.form_view.checkall.checked;

		}

		else

		{

			alert("Nothing to select");

			document.form_view.checkall.checked=false;

			

		}

}

function Remove()

{

	

	flag = 0;

	if(document.form_view.checkone.checked)

		flag = 1;



	for(i=0;i<document.form_view.checkone.length;i++)

	{

		if(document.form_view.checkone[i].checked)

		{

			flag = 1;

			break;

		}

	}

	

if(flag == 1)

	{

		if(confirm("You are going to delete these records permenantly, Do you want to continue?"))

		{   

			

		  return true;

			

		}

		else

		{

			return false;

		}	

	}

	else

	{

		alert("No record(s) selected");

		return false;

	}	

}



</script>

'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Listing Contacts</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
				<?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><span class="label label-success"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</span></div><?php endif; ?>
            <form id="form_view" name="form_view" method="post" action="">
                <?php if ($this->_tpl_vars['data_arr']): ?>
                <table id="datatable" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th><input name="checkall" type="checkbox" id="checkall" onClick="return checkAll()" value="CHKALL"></th>
                      <th>Name</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Message</th>
                      <th>Date</th>
                    </tr>
                  </thead>
    
                  <tbody>
                  <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['data_arr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                    <tr>
                      <td><input name="checkone[]" type="checkbox" id="checkone" value="<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
"></td>
                      <td><?php echo $this->_tpl_vars['cls_site']->ufirst($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['name']); ?>
</td>
                      <td><?php echo $this->_tpl_vars['cls_site']->ufirst($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['phone']); ?>
</td>
                      <td><?php echo $this->_tpl_vars['cls_site']->ufirst($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['email']); ?>
</td>
                      <td><?php echo $this->_tpl_vars['cls_site']->ufirst($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['message']); ?>
</td>
                      <td><?php echo $this->_tpl_vars['cls_site']->displayTime($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['date_added']); ?>
</td>
                    </tr>
                    <?php endfor; endif; ?>
                  </tbody>
                </table>
                <?php endif; ?>
              <div><input name="btn_delete" type="submit" class="btn btn-primary" id="btn_delete" value="Delete" onClick="return Remove();"></div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

