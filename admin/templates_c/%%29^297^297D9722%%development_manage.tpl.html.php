<?php /* Smarty version 2.6.19, created on 2021-12-30 21:20:06
         compiled from development_manage.tpl.html */ ?>
<?php echo '

<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="../tiny_mce/tinymce.js"></script>

<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<script language="javascript" type="text/javascript">
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))   return false;
         return true;
      }
function checkAll()
	{
		if(document.form_view.checkone)
			{
				document.form_view.checkone.checked=document.form_view.checkall.checked;
				for(i=0;i<document.form_view.checkone.length;i++)
				document.form_view.checkone[i].checked=document.form_view.checkall.checked;
			}
		else
			{
				alert("Nothing to select");
				document.form_view.checkall.checked=false;
			
			}
	}
function Remove()
	{
		flag = 0;
		if(document.form_view.checkone.checked)	flag = 1;
		for(i=0;i<document.form_view.checkone.length;i++)
			{
				if(document.form_view.checkone[i].checked)
					{
						flag = 1;
						break;
					}
			}
		if(flag == 1)
			{
				if(confirm("You are going to change status, Do you want to continue?"))	  return true;
				else return false;
			}
		else
			{
				alert("No record(s) selected");
				return false;
			}	
	}
 function addCheck()
 	{
				
		if(document.form_add.title.value == "")
			{
				alert("Please enter a title");
				document.form_add.title.focus();
				return false;
			}
		 return true;
	}
 function editCheck()
 	{
				
		if(document.form_edit.etitle.value == "")
			{
				alert("Please enter a title");
				document.form_edit.etitle.focus();
				return false;
			}
		/*if(document.form_edit.photo.value == "")
			{
				alert("Please upload image");
				document.form_edit.photo.focus();
				return false;
			}*/
		 return true;
	}
</script>
'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $this->_tpl_vars['tpls']['heading']; ?>
</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
				<?php if ($_REQUEST['add'] != ""): ?>
        <form name="form_add" method="post" action="" enctype="multipart/form-data">	
    
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Add <?php echo $this->_tpl_vars['tpls']['addcaption']; ?>
</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          <tr class="odd pointer">
                            <td class=" ">
                            	Title<br />
                            	<input class="form-control" type="text" name="title" id="title" />
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          
                          <tr class="even pointer">
                            <td class=" ">
                            	Image<br />
                            	<input class="form-control"  type="file" name="photo_file" id="photo_file" />
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">
                            	Description<br />
                            	<textarea style="width:100%; height:300px;" name="description" id="description" rows="3" cols="30" class="mceEditor"></textarea>
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr>
                          	<td class=" ">
                                  <div class="form-group">
                                    <div class="col-xs-12 col-md-offset-3" align="left">
                                        <input type="submit" name="Submit" value="Submit" class="btn btn-success" onclick="return validate_add();" />
                                        <input type="submit" name="cancel" value="Cancel" class="btn btn-primary" />
                                    </div>
                                  </div>
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </form>
				<?php endif; ?>
				<?php if ($_REQUEST['edit'] != ""): ?>
        <form name="form_edit" method="post" action="" enctype="multipart/form-data">
    
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Edit <?php echo $this->_tpl_vars['tpls']['edithead']; ?>
</th>
                            <th class="column-title">&nbsp;</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                          <tr class="odd pointer">
                            <td class=" ">
                            	Title<br />
                                <input class="form-control" name="etitle" type="text" id="etitle"  value="<?php echo $this->_tpl_vars['cls_site']->uwords($this->_tpl_vars['edit_arr'][0]['title']); ?>
"/>
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        
                          <tr class="even pointer">
                            <td class=" ">
                            	Image<br />
                            	<input type="file" class="form-control" name="photo" id="photo" value="<?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['edit_arr'][0]['image']); ?>
"/>
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">
                            	Description<br />
                            	<textarea style="width:100%; height:300px;" name="edescription" id="edescription" rows="3" cols="30" class="mceEditor">
                                    <?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['edit_arr'][0]['description']); ?>

                                </textarea>
                       		</td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr>
                          	<td class=" ">
                                  <div class="form-group" style="padding-left:100px;">
                                    <div class="col-xs-12 col-md-offset-3">
                                        <input type="submit" name="Update" value="Update" class="btn btn-success" onclick="return edit_val();" />
                                        <input type="submit" name="cancel" value="Cancel" class="btn btn-primary"/>
                                    </div>
                                  </div>
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </form>
                <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
