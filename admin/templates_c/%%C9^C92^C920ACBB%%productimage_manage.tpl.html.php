<?php /* Smarty version 2.6.19, created on 2021-09-15 11:37:11
         compiled from productimage_manage.tpl.html */ ?>
<?php echo '

<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<script language="javascript" type="text/javascript">
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))   return false;
         return true;
      }

var vdcnt	=	0;
function addMore()
	{
		var tbody = document.getElementById("addmoreimgs").getElementsByTagName("tbody")[0];
		vdcnt++;
		var row1 = document.createElement("TR");			
		trvdid		=	\'tr_vd\'+vdcnt;	
		row1.setAttribute(\'id\',trvdid);
		rmvvid	=	"<a href=\\"javascript:;\\" onclick=\\"removeVideos(\'" + trvdid + "\')\\">Remove</a>";
		var cell2= document.createElement("TD");
		var cell3= document.createElement("TD");
		cell2.innerHTML = "Title"; 	
		cell3.innerHTML = "<input name=\'title[]\' type=\'text\' id=\'title[]\' maxlength=\'50\'/>"; 	
		var cell4= document.createElement("TD");
		var cell5= document.createElement("TD");
		cell4.innerHTML = "Image"; 			
		cell5.innerHTML = "<input type=\'file\' name=\'photo_file[]\' id=\'photo_file[]\'/>"; 	
		var cell6= document.createElement("TD");
		cell6.innerHTML = rmvvid; 			
		row1.appendChild(cell2);	
		row1.appendChild(cell3);
		row1.appendChild(cell4);
		row1.appendChild(cell5);
		row1.appendChild(cell6);
		
		tbody.appendChild(row1);
	}
function removeVideos(id1)
	{
		document.getElementById("addmoreimgs").getElementsByTagName("tbody")[0].removeChild(document.getElementById(id1));		
	}

function checkAll()
	{
		if(document.form_view.checkone)
			{
				document.form_view.checkone.checked=document.form_view.checkall.checked;
				for(i=0;i<document.form_view.checkone.length;i++)
				document.form_view.checkone[i].checked=document.form_view.checkall.checked;
			}
		else
			{
				alert("Nothing to select");
				document.form_view.checkall.checked=false;
			
			}
	}
function Remove()
	{
		flag = 0;
		if(document.form_view.checkone.checked)	flag = 1;
		for(i=0;i<document.form_view.checkone.length;i++)
			{
				if(document.form_view.checkone[i].checked)
					{
						flag = 1;
						break;
					}
			}
		if(flag == 1)
			{
				if(confirm("You are going to change status, Do you want to continue?"))	  return true;
				else return false;
			}
		else
			{
				alert("No record(s) selected");
				return false;
			}	
	}
 function addCheck()
 	{
				
	var v_arr		=	document.getElementsByName("category[]");	
	var v_arr_file	=	document.getElementsByName("photo_file[]");
	for (var m = 0; m < v_arr.length; m++)
		{ 
			if (v_arr[m].value=="")
				{
					alert("Please enter category");
					v_arr[m].focus();
					return false;
				}
		}
		 return true;	}
 function editCheck()
 	{
				
		if(document.form_edit.ecategory.value == "")
			{
				alert("Please enter category");
				document.form_edit.etitle.focus();
				return false;
			}
		/*if(document.form_edit.photo.value == "")
			{
				alert("Please upload image");
				document.form_edit.photo.focus();
				return false;
			}*/
		 return true;
	}
</script>
'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $this->_tpl_vars['tpls']['heading']; ?>
 of <?php echo $this->_tpl_vars['cat_arr'][0]['title']; ?>
</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div style="float:right;">
            <form name="form_back" method="post" action="" enctype="multipart/form-data">
              <input class="btn btn-primary" type="submit" name="back" id="back" value=" Back To product" />
           </form>
          </div>
          <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
            <?php if ($_REQUEST['add'] != ""): ?>
            <form name="form_add" method="post" action="" enctype="multipart/form-data">
    
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title"><?php echo $this->_tpl_vars['tpls']['addcaption']; ?>
</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="odd pointer">
                            <td>
                                <div align="center">
                                    <table width="100%" id="addmoreimgs" border="0" cellpadding="20" cellspacing="0" class="MoreTable">
                                        <tr>
                                            <td>Title</td>
                                            <td><input name="title[]" type="text" id="title[]" maxlength="50" value="<?php echo $_REQUEST['title']; ?>
"/></td>
                                            <td>Image</td>
                                            <td><input type="file" name="photo_file[]" id="photo_file[]" /></td>
                                            <td><a href="javascript:;" onClick="return addMore();">Add more</a></td>
                                        </tr>                                          
                                      </table>
                                  </div>
                            </td>
                          </tr>
                          <tr>
                              <td class=" ">
                                    <div class="form-group">
                                      <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" style="padding-left:100px;">
                                          <input class="btn btn-success" type="submit" name="Submit" id="Submit" value="Submit" onClick="return addCheck();">
                                          <input class="btn btn-primary" type="submit" name="btn_cancel" id="btn_cancel" value=" Cancel " >
                                          </div>
                                    </div>
                              </td>
                            </tr>
                          </tbody>
                      </table>
                    </div>
                </form>
                <script language="javascript" type="text/javascript">
					document.form_add.category[0].focus();
				</script>
                <?php endif; ?>

				<?php if ($_REQUEST['edit'] != ""): ?>
        <form name="form_edit" method="post" action="" enctype="multipart/form-data">
    
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title"><?php echo $this->_tpl_vars['tpls']['edithead']; ?>
</th>
                            <th class="column-title">&nbsp;</th>
                            <th class="column-title">&nbsp;</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="odd pointer">
                            <td class=" ">Title</td>
                            <td class=" "><input name="etitle" type="text" id="etitle"  value="<?php echo $this->_tpl_vars['cls_site']->uwords($this->_tpl_vars['edit_arr'][0]['title']); ?>
"/></td>
                            <td class=" ">Image</td>
                            <td class=" "><input type="file" name="photo" id="photo" value="<?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['edit_arr'][0]['image']); ?>
"/></td>
                          </tr>
                          <tr>
                            <td class=" ">&nbsp;</td>
                          	<td class=" ">
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                      <input class="btn btn-success" type="submit" name="Update" id="Update" value=" Update " onClick="return editCheck();">
                                      <input class="btn btn-primary" type="submit" name="btn_ecancel" id="btn_ecancel" value=" Cancel " >
                                    </div>
                                  </div>
                            </td>
                            <td class=" ">&nbsp;</td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </form>
                <script language="javascript" type="text/javascript">
					document.form_edit.etitle.focus();
                </script>
                <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
