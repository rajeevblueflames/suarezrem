<?php /* Smarty version 2.6.19, created on 2021-09-14 03:38:10
         compiled from contact_address_manage.tpl.html */ ?>
<?php echo '

<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $this->_tpl_vars['tpls']['heading']; ?>
</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
				<?php if ($_REQUEST['add'] != ""): ?>
                <form name="form_add" method="post" action="" enctype="multipart/form-data">
    
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title"><?php echo $this->_tpl_vars['tpls']['addcaption']; ?>
</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="odd pointer">
                            <td class=" ">Address Title</td>
                            <td class=" "><input class="form-control" name="add_title" type="text" id="add_title" maxlength="50" value="<?php echo $_REQUEST['add_title']; ?>
"/></td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Address</td>
                            <td class=" "><textarea class="form-control" name="address" id="address" rows="4" cols="25"><?php echo $_REQUEST['address']; ?>
</textarea></td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">Phone</td>
                            <td class=" "><input class="form-control" name="phone" type="text" id="phone" maxlength="50" value="<?php echo $_REQUEST['phone']; ?>
"/></td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Fax</td>
                            <td class=" "><input class="form-control" name="fax" type="text" id="fax" maxlength="50" value="<?php echo $_REQUEST['fax']; ?>
"/></td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">Mobile No 1</td>
                            <td class=" "><input class="form-control" name="mobno1" type="text" id="mobno1" maxlength="50" value="<?php echo $_REQUEST['mobno1']; ?>
"/></td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Lan</td>
                            <td class=" "><input class="form-control" name="mobno2" type="text" id="mobno2" maxlength="50" value="<?php echo $_REQUEST['mobno2']; ?>
"/></td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">Email</td>
                            <td class=" "><input class="form-control" name="email" type="text" id="email" maxlength="50" value="<?php echo $_REQUEST['email']; ?>
"/></td>
                          </tr>
                          <tr>
                          	<td class=" ">
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" align="center" style="padding-left:100px;">
                                      <input class="btn btn-primary" type="submit" name="btn_cancel" id="btn_cancel" value=" Cancel " >
                                      <input class="btn btn-success" type="submit" name="Submit" id="Submit" value="Submit" onClick="return addCheck();"> 
                                    </div>
                                  </div>
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </form>
                <script language="javascript" type="text/javascript">
					document.form_add.category[0].focus();
				</script>
                <?php endif; ?>

				<?php if ($_REQUEST['edit'] != ""): ?>
                <form name="form_edit" method="post" action="" enctype="multipart/form-data">	
    
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title"><?php echo $this->_tpl_vars['tpls']['addcaption']; ?>
</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="odd pointer">
                            <td class=" ">Address Title</td>
                            <td class=" "><input class="form-control" name="add_title2" type="text" id="add_title2" maxlength="50" value="<?php echo $this->_tpl_vars['edit_arr'][0]['address_title']; ?>
"/></td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Address</td>
                            <td class=" "><textarea class="form-control" name="address" id="address" rows="4" cols="25"><?php echo $this->_tpl_vars['edit_arr'][0]['address']; ?>
</textarea></td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">Phone</td>
                            <td class=" "><input class="form-control" name="phone" type="text" id="phone" maxlength="50" value="<?php echo $this->_tpl_vars['edit_arr'][0]['phone']; ?>
"/></td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Fax</td>
                            <td class=" "><input class="form-control" name="fax" type="text" id="fax" maxlength="50" value="<?php echo $this->_tpl_vars['edit_arr'][0]['fax']; ?>
"/></td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">Mobile No 1</td>
                            <td class=" "><input class="form-control" name="mobno1" type="text" id="mobno1" maxlength="50" value="<?php echo $this->_tpl_vars['edit_arr'][0]['mobno1']; ?>
"/></td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Lan</td>
                            <td class=" "><input class="form-control" name="mobno2" type="text" id="mobno2" maxlength="50" value="<?php echo $this->_tpl_vars['edit_arr'][0]['mobno2']; ?>
"/></td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">Email</td>
                            <td class=" "><input class="form-control" name="email" type="text" id="email" maxlength="50" value="<?php echo $this->_tpl_vars['edit_arr'][0]['email']; ?>
"/></td>
                          </tr>
                          <tr>
                          	<td class=" ">
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" align="center">
                                      <input class="btn btn-primary" type="submit" name="btn_ecancel" id="btn_ecancel" value=" Cancel " >
                                      <input class="btn btn-success" type="submit" name="update" id="update" value="Update " onClick="return editCheck();"> 
                                    </div>
                                  </div>
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </form>
                <script language="javascript" type="text/javascript">
					document.form_edit.etitle.focus();
                </script>
                <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
