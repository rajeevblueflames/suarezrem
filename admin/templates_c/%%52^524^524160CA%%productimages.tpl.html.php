<?php /* Smarty version 2.6.19, created on 2021-09-16 01:16:25
         compiled from productimages.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'productimages.tpl.html', 211, false),)), $this); ?>
<?php echo '
<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<script language="javascript" type="text/javascript">
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))   return false;
         return true;
      }

var vdcnt	=	0;
function addMore()
	{
		var tbody = document.getElementById("addTable").getElementsByTagName("tbody")[0];
		vdcnt++;
		var row1 = document.createElement("TR");			
		trvdid		=	\'tr_vd\'+vdcnt;	
		row1.setAttribute(\'id\',trvdid);
		rmvvid	=	"<a href=\\"javascript:;\\" onclick=\\"removeVideos(\'" + trvdid + "\')\\">Remove</a>";
		var cell2= document.createElement("TD");
		var cell3= document.createElement("TD");
		cell2.innerHTML = "Title"; 	
		cell3.innerHTML = "<input name=\'title[]\' type=\'text\' id=\'title[]\' maxlength=\'50\'/>&nbsp;"; 	
		var cell4= document.createElement("TD");
		var cell5= document.createElement("TD");
		cell4.innerHTML = "Image"; 			
		cell5.innerHTML = "<input type=\'file\' name=\'photo_file[]\' id=\'photo_file[]\'/>&nbsp;"; 	
		var cell6= document.createElement("TD");
		cell6.innerHTML = rmvvid; 			
		row1.appendChild(cell2);	
		row1.appendChild(cell3);
		row1.appendChild(cell4);
		row1.appendChild(cell5);
		row1.appendChild(cell6);
		
		tbody.appendChild(row1);
	}
function removeVideos(id1)
	{
		document.getElementById("addTable").getElementsByTagName("tbody")[0].removeChild(document.getElementById(id1));		
	}

function checkAll()
	{
		if(document.form_view.checkone)
			{
				document.form_view.checkone.checked=document.form_view.checkall.checked;
				for(i=0;i<document.form_view.checkone.length;i++)
				document.form_view.checkone[i].checked=document.form_view.checkall.checked;
			}
		else
			{
				alert("Nothing to select");
				document.form_view.checkall.checked=false;
			
			}
	}
function Remove()
	{
		flag = 0;
		if(document.form_view.checkone.checked)	flag = 1;
		for(i=0;i<document.form_view.checkone.length;i++)
			{
				if(document.form_view.checkone[i].checked)
					{
						flag = 1;
						break;
					}
			}
		if(flag == 1)
			{
				if(confirm("You are going to change status, Do you want to continue?"))	  return true;
				else return false;
			}
		else
			{
				alert("No record(s) selected");
				return false;
			}	
	}
 function addCheck()
 	{
				
	var v_arr		=	document.getElementsByName("title[]");	
	var v_arr_file	=	document.getElementsByName("photo_file[]");
	for (var m = 0; m < v_arr.length; m++)
		{ 
			if (v_arr[m].value=="")
				{
					alert("Please enter title for image");
					v_arr[m].focus();
					return false;
				}
			if (v_arr_file[m].value=="")
				{
					alert("Please select an image");
					v_arr_file[m].focus();
					return false;
				}
		}
		 return true;	}
 function editCheck()
 	{
				
		if(document.form_edit.etitle.value == "")
			{
				alert("Please enter a title");
				document.form_edit.etitle.focus();
				return false;
			}
		/*if(document.form_edit.photo.value == "")
			{
				alert("Please upload image");
				document.form_edit.photo.focus();
				return false;
			}*/
		 return true;
	}
</script>
'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $this->_tpl_vars['tpls']['heading']; ?>
 of <?php echo $this->_tpl_vars['cat_arr'][0]['title']; ?>
</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
            <span class="label label-danger" style="color:#FFF;">Please Upload the images <?php if ($this->_tpl_vars['tpls']['width'] != ""): ?> <?php echo $this->_tpl_vars['tpls']['width']; ?>
 <?php else: ?> 800 <?php endif; ?> X <?php if ($this->_tpl_vars['tpls']['height'] != ""): ?> <?php echo $this->_tpl_vars['tpls']['height']; ?>
 <?php else: ?> 800 <?php endif; ?> (width = <?php if ($this->_tpl_vars['tpls']['width'] != ""): ?> <?php echo $this->_tpl_vars['tpls']['width']; ?>
 <?php else: ?> 800 <?php endif; ?> pixels, Height = <?php if ($this->_tpl_vars['tpls']['height'] != ""): ?> <?php echo $this->_tpl_vars['tpls']['height']; ?>
 <?php else: ?> 800 <?php endif; ?> pixels)</span>
          <div style="float:right;">
            <form name="form_back" method="post" action="" enctype="multipart/form-data">
              <input class="btn btn-primary" type="submit" name="back" id="back" value=" Back To product" />
           </form>
          </div>
          <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
            <form name="product" method="post" action="">
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th><input name="checkall" type="checkbox" id="checkall" onClick="return checkAll()" value="CHKALL"></th>
                  <th><?php echo $this->_tpl_vars['tpls']['listcaption']; ?>
</th>
                  <th>Category</th>
                  <th><?php echo $this->_tpl_vars['tpls']['pics']; ?>
</th>
                  <th><?php echo $this->_tpl_vars['tpls']['datedoj']; ?>
</th>
                  <th><?php echo $this->_tpl_vars['tpls']['listoptions']; ?>
</th>
                  <th>Order</th>
                </tr>
              </thead>

              <tbody>
              <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['data_arr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                <tr>
                  <td><input name="checkone[]" type="checkbox" id="checkone" value="<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
"> </td>
                  <td><?php echo $this->_tpl_vars['cls_site']->uwords($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['title']); ?>
</td>
                  <td>
                    <?php $this->assign('cat', $this->_tpl_vars['cls_db']->getdbcontents_id($this->_tpl_vars['tpls']['parenttable'],$this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['cid'])); ?>
                    <?php echo $this->_tpl_vars['cls_site']->uwords($this->_tpl_vars['cat'][0]['title']); ?>

                  <td>
                    <a href="<?php echo $this->_tpl_vars['tpls']['imagepath']; ?>
/<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['image']; ?>
" rel="lightbox">
                      <img src="<?php echo $this->_tpl_vars['tpls']['imagepath']; ?>
/thumb/<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['image']; ?>
" alt="" border="0" style="max-width:150px;">	
                    </a>
                  </td>
                  <td>
                    <?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['date_added']); ?>

                  </td>
                  <td>
                    <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?edit=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                      <img src="images/edit.gif" alt="down"  border="0" title="Click here to edit"/>	
                     </a>
                    <?php if ($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['status'] == '1'): ?>
                    &nbsp;&nbsp;
                    <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?statuschange=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">

                      <img src="images/active_1.gif" alt="down"  border="0" title="Click here to inactivate"/>
                    </a>
                    <?php else: ?>
                    &nbsp;&nbsp;
                    <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?statuschange=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                      <img src="./images/inactive_1.gif" alt="down"  border="0" title="Click here to activate"/>
                    </a>
                  <?php endif; ?>
                  </td>
                  <td>
                    <div align="left">
                      <?php if (count($this->_tpl_vars['data_arr']) > 1): ?>
                          <?php if ($this->_sections['i']['index'] == 0): ?>
                              <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?down=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                                <img src="images/down.gif" alt="down"  border="0" />
                               </a>
                          <?php elseif ($this->_sections['i']['index']+1 == count($this->_tpl_vars['data_arr'])): ?>
                              <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?up=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                                <img src="images/up.gif" alt="up"  border="0" />
                              </a>
                          <?php else: ?>
                              <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?down=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                                <img src="images/down.gif" alt="down"  border="0" />
                              </a>
                              &nbsp;
                              <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?up=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                                <img src="images/up.gif" alt="up"  border="0" />
                              </a>
                          <?php endif; ?>
                          
                      <?php endif; ?>			
                  </div>
                  </td>
                </tr>
                <?php endfor; endif; ?>
              </tbody>
            </table>
            <div>
            <input style="float:right;" type="submit" name="addpres" class="btn btn-primary" value="Add product Images" />   
            <input style="float:left;" name="btn_delete" type="submit" class="btn btn-primary" id="btn_delete" value="Delete" onClick="return Remove();">   
            </div>    
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
