<?php /* Smarty version 2.6.19, created on 2022-01-04 00:36:13
         compiled from advantage_meta.tpl.html */ ?>
<?php echo '

<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>

<script type="text/javascript" src="../tiny_mce/tinymce.js"></script>

<script language="javascript" type="text/javascript">

function validate()

	{

		if(document.form1.txt_content.value	==	"")

			{

				alert("Please enter your content");

				document.form1.txt_content.focus();

				return false;

			}	

		

	}

	

</script>

'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Manage <?php echo $this->_tpl_vars['title']; ?>
 Page</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
            <br />
            <form id="form1" name="form1" method="post" action="">
                <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Advantage Page Tags</th>
                            <th class="column-title">&nbsp;</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="odd pointer">
                            <td class=" ">Head Title</td>
                            <td class=" "><input class="form-control" type="text" name="meta_title" id="meta_title" value="<?php if ($this->_tpl_vars['meta_details']): ?><?php echo $this->_tpl_vars['cls_site']->hentity($this->_tpl_vars['meta_details'][0]['meta_title']); ?>
<?php endif; ?>" /></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Meta Keyword</td>
                            <td class=" "><input class="form-control" type="text" name="meta_keyword" id="meta_keyword" value="<?php if ($this->_tpl_vars['meta_details']): ?><?php echo $this->_tpl_vars['cls_site']->hentity($this->_tpl_vars['meta_details'][0]['meta_keyword']); ?>
<?php endif; ?>" /></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">Meta Description</td>
                            <td class=" "><textarea class="form-control" name="meta_description" id="meta_description" rows="3" cols="30" ><?php if ($this->_tpl_vars['meta_details']): ?><?php echo $this->_tpl_vars['cls_site']->hentity($this->_tpl_vars['meta_details'][0]['meta_description']); ?>
<?php endif; ?></textarea></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="3" style="padding-top:20px;">
                            <input name="change" type="submit" class="btn btn-primary" value="Change Content" onClick="return validate();"/>
                          </tr>
                        </tbody>
                      </table>
                </div>
            </form>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script language="javascript" type="text/javascript">

document.form1.txt_content.focus();

</script> 



<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

