<?php /* Smarty version 2.6.19, created on 2021-09-16 01:15:19
         compiled from product.tpl.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'product.tpl.html', 173, false),)), $this); ?>
<?php echo '
<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="../tiny_mce/tinymce.js"></script>

<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<script language="javascript" type="text/javascript">
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))   return false;
         return true;
      }
function checkAll()
	{
		if(document.form_view.checkone)
			{
				document.form_view.checkone.checked=document.form_view.checkall.checked;
				for(i=0;i<document.form_view.checkone.length;i++)
				document.form_view.checkone[i].checked=document.form_view.checkall.checked;
			}
		else
			{
				alert("Nothing to select");
				document.form_view.checkall.checked=false;
			
			}
	}
function Remove()
	{
		flag = 0;
		if(document.form_view.checkone.checked)	flag = 1;
		for(i=0;i<document.form_view.checkone.length;i++)
			{
				if(document.form_view.checkone[i].checked)
					{
						flag = 1;
						break;
					}
			}
		if(flag == 1)
			{
				if(confirm("You are going to change status, Do you want to continue?"))	  return true;
				else return false;
			}
		else
			{
				alert("No record(s) selected");
				return false;
			}	
	}
 function addCheck()
 	{
				
		if(document.form_add.title.value == "")
			{
				alert("Please enter a title");
				document.form_add.title.focus();
				return false;
			}
		 return true;
	}
 function editCheck()
 	{
				
		if(document.form_edit.etitle.value == "")
			{
				alert("Please enter a title");
				document.form_edit.etitle.focus();
				return false;
			}
		/*if(document.form_edit.photo.value == "")
			{
				alert("Please upload image");
				document.form_edit.photo.focus();
				return false;
			}*/
		 return true;
	}
</script>
'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $this->_tpl_vars['tpls']['heading']; ?>
</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
            <span class="label label-danger" style="color:#FFF;">Please Upload the images <?php if ($this->_tpl_vars['tpls']['width'] != ""): ?> <?php echo $this->_tpl_vars['tpls']['width']; ?>
 <?php else: ?> 800 <?php endif; ?> X <?php if ($this->_tpl_vars['tpls']['height'] != ""): ?> <?php echo $this->_tpl_vars['tpls']['height']; ?>
 <?php else: ?> 800 <?php endif; ?> (width = <?php if ($this->_tpl_vars['tpls']['width'] != ""): ?> <?php echo $this->_tpl_vars['tpls']['width']; ?>
 <?php else: ?> 800 <?php endif; ?> pixels, Height = <?php if ($this->_tpl_vars['tpls']['height'] != ""): ?> <?php echo $this->_tpl_vars['tpls']['height']; ?>
 <?php else: ?> 800 <?php endif; ?> pixels)</span>
          </div>
            <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
            <form name="product" method="post" action="">
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th><input name="checkall" type="checkbox" id="checkall" onClick="return checkAll()" value="CHKALL"></th>
                  <th>Category</th>
                  <th><?php echo $this->_tpl_vars['tpls']['listcaption']; ?>
</th>
                  <th><?php echo $this->_tpl_vars['tpls']['pics']; ?>
</th>
                  <th>Description</th>
                  <th><?php echo $this->_tpl_vars['tpls']['datedoj']; ?>
</th>
                  <th><?php echo $this->_tpl_vars['tpls']['listoptions']; ?>
</th>
                  <th>Order</th>
                </tr>
              </thead>

              <tbody>
              <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['data_arr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                <tr>
                  <td><input name="checkone[]" type="checkbox" id="checkone" value="<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
"> </td>
                  <td>
                    <?php $this->assign('cg', $this->_tpl_vars['cls_db']->getdbcontents_id('suarezrem_category',$this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['cid'])); ?>
                    <?php echo $this->_tpl_vars['cls_site']->uwords($this->_tpl_vars['cg'][0]['category']); ?>

                  </td>
                  <td><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['title']); ?>
</td>
                  <td>
                    <a href="<?php echo $this->_tpl_vars['tpls']['imagepath']; ?>
/<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['image']; ?>
" rel="lightbox">
                      <img src="<?php echo $this->_tpl_vars['tpls']['imagepath']; ?>
/thumb/<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['image']; ?>
" alt="" border="0" style="max-width:150px;">	
                    </a>
                  </td>
                  <td class="imageShortSize"><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['description']); ?>
</td>
                  <td>
                    <?php echo $this->_tpl_vars['cls_site']->displayTime($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['date_added']); ?>

                  </td>
                  <td>
                      <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?edit=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                        <img src="images/edit.gif" alt="down"  border="0" title="Click here to edit"/>	
                      </a>
                      <?php if ($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['status'] == '1'): ?>
                      &nbsp;&nbsp;
                      <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?statuschange=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                        <img src="images/active_1.gif" alt="down"  border="0" title="Click here to inactivate"/>
                      </a>
                      <?php else: ?>
                      &nbsp;&nbsp;
                      <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?statuschange=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                        <img src="images/inactive_1.gif" alt="down"  border="0" title="Click here to activate"/>
                      </a>
                      <?php endif; ?>	
                      <?php if ($this->_tpl_vars['tpls']['childpagename']): ?>
                      <a href="<?php echo $this->_tpl_vars['tpls']['childpagename']; ?>
?cid=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                        <img src="images/gallery.gif" alt="product"  border="0" title="Click here to manage product"/>	
                      </a>		
                      <?php endif; ?>
                  </td>
                  <td>
                    <div align="left">
                      <?php if (count($this->_tpl_vars['data_arr']) > 1): ?>
                          <?php if ($this->_sections['i']['index'] == 0): ?>
                              <a href="<?php echo $this->_tpl_vars['tpls']['pagename1']; ?>
?down=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                                <img src="images/down.gif" alt="down"  border="0" />
                              </a>
                          <?php elseif ($this->_sections['i']['index']+1 == count($this->_tpl_vars['data_arr'])): ?>
                              <a href="<?php echo $this->_tpl_vars['tpls']['pagename1']; ?>
?up=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                                <img src="images/up.gif" alt="up"  border="0" />
                              </a>
                          <?php else: ?>
                              <a href="<?php echo $this->_tpl_vars['tpls']['pagename1']; ?>
?down=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                                <img src="images/down.gif" alt="down"  border="0" />
                              </a>
                              &nbsp;
                              <a href="<?php echo $this->_tpl_vars['tpls']['pagename1']; ?>
?up=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                                <img src="images/up.gif" alt="up"  border="0" />
                              </a>
                          <?php endif; ?>
                          
                      <?php endif; ?>			
                  </div>
                  </td>
                </tr>
                <?php endfor; endif; ?>
              </tbody>
            </table>
            <div>
            <input style="float:right;" type="submit" name="addpres" class="btn btn-primary" value="Add product" />   
            <input style="float:left;" name="btn_delete" type="submit" class="btn btn-primary" id="btn_delete" value="Delete" onClick="return Remove();">   
            </div>    
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

