<?php /* Smarty version 2.6.19, created on 2021-09-16 01:20:01
         compiled from makeitwork.tpl.html */ ?>
<?php echo '

<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


<script language="javascript" type="text/javascript">

function checkAll()
{
	if(document.gallery.checkone)
		{
			document.gallery.checkone.checked=document.gallery.checkall.checked;
			for(i=0;i<document.gallery.checkone.length;i++)
			document.gallery.checkone[i].checked=document.gallery.checkall.checked;
		}
	else
		{
			alert("Nothing to select");
			document.gallery.checkall.checked=false;
		
		}
}
</script>
'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $this->_tpl_vars['msg1']; ?>
</h2>
		  	<!--<div style="display:inline-table;" class="col-md-offset-3" align="left">
				&emsp; <input id="fc_create" data-toggle="modal" data-target="#CalenderModalNew" type="button" name="update_meta_contents" value="Update Meta Contents" class="btn btn-primary"/>
			</div>-->
			<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
				<form id="form1" name="form1" class="form-horizontal" method="post" action="">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">New Meta Entry</h4>
				  </div>
				  <div class="modal-body">
					<div id="testmodal" style="padding: 5px 20px;">
						<div class="form-group">
						  <label class="col-sm-3 control-label">Head Title</label>
						  <div class="col-sm-9">
							<input class="form-control" type="text" name="meta_title" id="meta_title" value="<?php if ($this->_tpl_vars['meta_details']): ?><?php echo $this->_tpl_vars['cls_site']->hentity($this->_tpl_vars['meta_details'][0]['meta_title']); ?>
<?php endif; ?>" />
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-sm-3 control-label">Meta Keyword</label>
						  <div class="col-sm-9">
							<input class="form-control" type="text" name="meta_keyword" id="meta_keyword" value="<?php if ($this->_tpl_vars['meta_details']): ?><?php echo $this->_tpl_vars['cls_site']->hentity($this->_tpl_vars['meta_details'][0]['meta_keyword']); ?>
<?php endif; ?>" />
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-sm-3 control-label">Meta Description</label>
						  <div class="col-sm-9">
							<textarea class="form-control" name="meta_description" id="meta_description" rows="3" cols="30" ><?php if ($this->_tpl_vars['meta_details']): ?><?php echo $this->_tpl_vars['cls_site']->hentity($this->_tpl_vars['meta_details'][0]['meta_description']); ?>
<?php endif; ?></textarea>
						  </div>
						</div>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
					<input name="meta_change" type="submit" class="btn btn-primary" value="Save Chanes" onclick="return validate();">
				  </div>
        </div>
				</form>
				</div>
			  </div>
           <!-- <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>-->
            <div class="clearfix"></div>
            <span class="label label-danger" style="color:#FFF;">Please Upload the images 600 X 600 (width = 600 pixels, Height = 600 pixels)</span>
          </div>
            <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
            <form name="gallery" method="post" action="">
            <table id="datatable" class="table table-striped table-bordered">
              <thead>
                <tr>
                 

                <!--  <th><input name="checkall" type="checkbox" id="checkall" onClick="return checkAll()" value="CHKALL"></th>
         -->
                  <th>Title</th>
                  <th>Image</th>
                  <th>Description</th>
                </tr>
              </thead>

              <tbody>
              <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['array']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                <tr>
                 <!-- <td><input name="checkone[]" type="checkbox" id="checkone" value="<?php echo $this->_tpl_vars['array'][$this->_sections['i']['index']]['id']; ?>
"> </td>-->
                  <td><?php echo $this->_tpl_vars['cls_site']->ufirst($this->_tpl_vars['array'][$this->_sections['i']['index']]['title']); ?>
</td>
                  <td>
                        <a href="../makeitworkImages/<?php echo $this->_tpl_vars['array'][$this->_sections['i']['index']]['image']; ?>
" rel="lightbox">
                                <img src="../makeitworkImages/thumb/<?php echo $this->_tpl_vars['array'][$this->_sections['i']['index']]['image']; ?>
"  border="0" style="max-width:150px;" />
                        </a>
                  </td>
                  <td><?php echo $this->_tpl_vars['cls_site']->ufirst($this->_tpl_vars['array'][$this->_sections['i']['index']]['content']); ?>
</td>
                  </td>
                </tr>
                <?php endfor; endif; ?>
              </tbody>
            </table>
            <div>
              <th colspan="2">

                <?php if ($this->_tpl_vars['array']): ?>
                <input style="float:right;" type="submit" name="edit" class="btn btn-primary" value="Change The Make it Work"  onclick="return edit_val();" />   
                <?php else: ?>
                <input style="float:right;" type="submit" name="add" class="btn btn-primary" value="Add The Make it Work" />
                <?php endif; ?>

                </th>
         <!--   <input style="float:left;" name="btn_delete" type="submit" class="btn btn-primary" id="btn_delete" value="Delete" onClick="return Remove();"> -->  
            </div>    
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
