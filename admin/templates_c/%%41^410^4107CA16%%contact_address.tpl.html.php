<?php /* Smarty version 2.6.19, created on 2021-09-14 03:36:07
         compiled from contact_address.tpl.html */ ?>
<?php echo '

<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $this->_tpl_vars['tpls']['heading']; ?>
</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
          
            <?php if ($_REQUEST['add'] != ""): ?>
            <form name="form_add" method="post" action="" enctype="multipart/form-data">
    
                <div class="table-responsive">
                  <table class="table table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title"><?php echo $this->_tpl_vars['tpls']['addcaption']; ?>
</th>
                        <th class="column-title">&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="odd pointer">
                        <td class=" ">Address Title</td>
                        <td class=" "><input class="form-control" name="add_title" type="text" id="add_title"  value="<?php echo $_REQUEST['add_title']; ?>
"/></td>
                      </tr>
                      <tr class="even pointer">
                        <td class=" ">Address</td>
                        <td class=" "><textarea class="form-control" name="address" id="address" rows="4" cols="25"><?php echo $_REQUEST['address']; ?>
</textarea></td>
                      </tr>
                      <tr class="odd pointer">
                        <td class=" ">Phone</td>
                        <td class=" "><input class="form-control" name="phone" type="text" id="phone"  value="<?php echo $_REQUEST['phone']; ?>
"/></td>
                      </tr>
                      <tr class="odd pointer">
                        <td class=" ">Mobile No 1</td>
                        <td class=" "><input class="form-control" name="mobno1" type="text" id="mobno1"  value="<?php echo $_REQUEST['mobno1']; ?>
"/></td>
                      </tr>
                      <tr class="even pointer">
                        <td class=" ">Mobile No 2</td>
                        <td class=" "><input class="form-control" name="mobno2" type="text" id="mobno2"  value="<?php echo $_REQUEST['mobno2']; ?>
"/></td>
                      </tr>
                      <tr class="odd pointer">
                        <td class=" ">Email</td>
                        <td class=" "><input class="form-control" name="email" type="text" id="email"  value="<?php echo $_REQUEST['email']; ?>
"/></td>
                      </tr>
                      <tr class="even pointer">
                        <td class=" ">Email 2</td>
                        <td class=" "><input class="form-control" name="email2" type="text" id="email2"  value="<?php echo $_REQUEST['email']; ?>
"/></td>
                      </tr>
                      <tr>
                        <td class=" ">
                              <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" align="center" style="padding-left:100px;">
                                  <input class="btn btn-success" type="submit" name="Submit" id="Submit" value="Submit" onClick="return addCheck();"> 
                                  <input class="btn btn-primary" type="submit" name="btn_cancel" id="btn_cancel" value=" Cancel " >
                                </div>
                              </div>
                        </td>
                        <td class=" ">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </form>
            <script language="javascript" type="text/javascript">
                document.form_add.category[0].focus();
            </script>
            <?php endif; ?>
          
            <?php if ($_REQUEST['edit'] != ""): ?>
            <form name="form_edit" method="post" action="" enctype="multipart/form-data">	

                <div class="table-responsive">
                  <table class="table table-striped jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title"><?php echo $this->_tpl_vars['tpls']['addcaption']; ?>
</th>
                        <th class="column-title">&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="odd pointer">
                        <td class=" ">Address Title</td>
                        <td class=" "><input class="form-control" name="add_title2" type="text" id="add_title2"  value="<?php echo $this->_tpl_vars['edit_arr'][0]['address_title']; ?>
"/></td>
                      </tr>
                      <tr class="even pointer">
                        <td class=" ">Address</td>
                        <td class=" "><textarea class="form-control" name="address" id="address" rows="4" cols="25"><?php echo $this->_tpl_vars['edit_arr'][0]['address']; ?>
</textarea></td>
                      </tr>
                      <tr class="odd pointer">
                        <td class=" ">Phone</td>
                        <td class=" "><input class="form-control" name="phone" type="text" id="phone"  value="<?php echo $this->_tpl_vars['edit_arr'][0]['phone']; ?>
"/></td>
                      </tr>
                      <tr class="odd pointer">
                        <td class=" ">Mobile No 1</td>
                        <td class=" "><input class="form-control" name="mobno1" type="text" id="mobno1"  value="<?php echo $this->_tpl_vars['edit_arr'][0]['mobno1']; ?>
"/></td>
                      </tr>
                      <tr class="even pointer">
                        <td class=" ">Mobile No 2</td>
                        <td class=" "><input class="form-control" name="mobno2" type="text" id="mobno2"  value="<?php echo $this->_tpl_vars['edit_arr'][0]['mobno2']; ?>
"/></td>
                      </tr>
                      <tr class="odd pointer">
                        <td class=" ">Email1</td>
                        <td class=" "><input class="form-control" name="email" type="text" id="email"  value="<?php echo $this->_tpl_vars['edit_arr'][0]['email']; ?>
"/></td>
                      </tr>
                      <tr class="even pointer">
                        <td class=" ">Email2</td>
                        <td class=" "><input class="form-control" name="email2" type="text" id="email2"  value="<?php echo $this->_tpl_vars['edit_arr'][0]['email2']; ?>
"/></td>
                      </tr>
                      <tr>
                        <td class=" ">
                              <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" align="center">
                                  <input class="btn btn-success" type="submit" name="update" id="update" value="Update " onClick="return editCheck();"> 
                                  <input class="btn btn-primary" type="submit" name="btn_ecancel" id="btn_ecancel" value=" Cancel " >
                                </div>
                              </div>
                        </td>
                        <td class=" ">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </form>
            <script language="javascript" type="text/javascript">
                document.form_edit.etitle.focus();
            </script>
            <?php endif; ?>

            <?php if ($_REQUEST['add'] == "" && $_REQUEST['edit'] == ""): ?>
            <form name="form_view" method="post" action="">
            <table id="table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Address Title</th>
                  <th>Address</th>
                  <th>Mobile NO 1</th>
                  <th>Mobile No 2</th>
                  <th>Phone</th>
                  <th>Email ID</th>
                  <th>Email ID 2</th>
                  <th><?php echo $this->_tpl_vars['tpls']['datedoj']; ?>
</th>
                  <th><?php echo $this->_tpl_vars['tpls']['listoptions']; ?>
</th>
                </tr>
              </thead>

              <tbody>
              <?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['start'] = (int)0;
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['data_arr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
                <tr>
                  <td><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['address_title']); ?>
</td>
                  <td><?php echo $this->_tpl_vars['cls_site']->lbreak($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['address']); ?>
</td>
                  <td><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['mobno1']); ?>
</td>
                  <td><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['mobno2']); ?>
</td>
                  <td><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['phone']); ?>
</td>
                  <td><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['email']); ?>
 </td>
                  <td><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['email2']); ?>
 </td>

                  <td><?php echo $this->_tpl_vars['cls_site']->strip($this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['date_added']); ?>
 </td>
                  <td>
                        <a href="<?php echo $this->_tpl_vars['tpls']['pagename']; ?>
?edit=<?php echo $this->_tpl_vars['data_arr'][$this->_sections['i']['index']]['id']; ?>
">
                            <img src="images/edit.gif" alt="down"  border="0" title="Click here to edit"/>	
                        </a>
                  </td>
                </tr>
                <?php endfor; endif; ?>
                <tr>
                	<td colspan="8">
                        <?php if (! $this->_tpl_vars['data_arr']): ?>
        				<form name="frm_form" method="post" action="">
                        <input style="float:right;" type="submit" name="addpres" class="btn btn-primary" value="Add Contact Addres" /> 
        				</form>
                        <?php endif; ?>
                    </td>
                </tr>
              </tbody>
            </table>
            <?php endif; ?>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>

