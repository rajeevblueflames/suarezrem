<?php /* Smarty version 2.6.19, created on 2021-09-14 05:42:10
         compiled from emotion_manage.tpl.html */ ?>
<?php echo '

<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />

<script language="javascript" type="text/javascript">

function validate_add()
	{
			if(document.add_gallery.title.value=="")
	{
		alert("Please enter title");
			document.add_gallery.title.focus();
		return false;
	}
	if(document.add_gallery.description.value=="")
	{
		alert("Please enter description");
			document.add_gallery.description.focus();
		return false;
	}
	if(document.add_gallery.image.value=="")
	{
		alert("Please upload image");
			document.add_gallery.image.focus();
		return false;
	}
	
	/*var v_arr_file	=	document.getElementsByName("image[]");
	
	for (var m = 0; m < v_arr_file.length; m++)
		{ 
			
			if (v_arr_file[m].value=="")
				{
					alert("Please select an image ");
					v_arr_file[m].focus();
					return false;
				}
		}
		 return true;
		
	*/
}
function edit_val()
{
	if(document.edit_gallery.title.value=="")
	{
		alert("Please enter title");
			document.edit_gallery.title.focus();
		return false;
	}
		if(document.edit_gallery.description.value=="")
	{
		alert("Enter description");
			document.edit_gallery.description.focus();
		return false;
	}

}
</script>
'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><?php echo $this->_tpl_vars['tpls']['heading']; ?>
</h2>
			  

            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
				<?php if ($_REQUEST['add'] != ""): ?>
                <form name="add_gallery" method="post" action="" enctype="multipart/form-data">	
    
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Add Emotion</th>
                            <th class="column-title">&nbsp;</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="odd pointer">
                            <td class=" ">Title</td>
                            <td class=" "><input class="form-control" type="text" name="title" id="title" /></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr class="even pointer">

                            <td class=" ">Description</td>
                            <td class=" "><textarea class="form-control" name="description" id="description" rows="3" cols="30" ></textarea></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
		
                          <tr class="odd pointer">
                            <td class=" ">Image</td>
                            <td class=" "><input class="form-control" type="file" name="image" id="image" /></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr>
                          	<td class=" ">
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" align="center" style="padding-left:100px;">
                                        <input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="return validate_add();" />
                                        <input type="submit" name="cancel" value="Cancel" class="btn btn-primary" />
                                    </div>
                                  </div>
                            </td>
                            <td class=" ">&nbsp;</td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </form>
				<?php endif; ?>
				<?php if ($_REQUEST['edit'] != ""): ?>
                <form name="edit_gallery" method="post" action="" enctype="multipart/form-data">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Edit Emotion</th>
                            <th class="column-title">&nbsp;</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="odd pointer">
                            <td class=" ">Title</td>
                            <td class=" "><input class="form-control" type="text" name="title" id="title"  value="<?php echo $this->_tpl_vars['data_arr1'][0]['title']; ?>
"/></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Description</td>
                            <td class=" "><textarea class="form-control" name="description" id="description" rows="3" cols="30" ><?php echo $this->_tpl_vars['data_arr1'][0]['content']; ?>
</textarea></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
							
                          <tr class="odd pointer">
                            <td class=" ">Image</td>
                            <td class=" "><input class="form-control" type="file" name="image" id="image" /></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr>
                          	<td class=" ">
                                  <div class="form-group" style="padding-left:100px;">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                        <input type="submit" name="update" value="Update" class="btn btn-success" onclick="return edit_val();" />
                                        <input type="submit" name="cancel" value="Cancel" class="btn btn-primary"/>
                                    </div>
                                  </div>
                            </td>
                            <td class=" ">&nbsp;</td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
					
                </form>
                <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
