<?php /* Smarty version 2.6.19, created on 2021-09-14 03:20:36
         compiled from office_location.tpl.html */ ?>
<?php echo '

<!-- Datatables -->
<link href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
<link href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>

<script type="text/javascript" src="../tiny_mce/tinymce.js"></script>

<script language="javascript" type="text/javascript">

function validate()

	{

		if(document.form1.txt_latitude.value	==	"")

			{

				alert("Please enter your latitude");

				document.form1.txt_latitude.focus();

				return false;

			}	

		if(document.form1.txt_longitude.value	==	"")

			{

				alert("Please enter your longitude");

				document.form1.txt_longitude.focus();

				return false;

			}	

		

	}

	

</script>

'; ?>


<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Manage <?php echo $this->_tpl_vars['title']; ?>
 Page</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <?php if ($this->_tpl_vars['TPL_MESS']): ?><div align="center"><?php echo $this->_tpl_vars['TPL_MESS']; ?>
</div><?php endif; ?>
          <div class="x_content">
                <form id="form1" name="form1" method="post" action="">
    
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Manage <?php echo $this->_tpl_vars['title']; ?>
 Content</th>
                            <th class="column-title">&nbsp;</th>
                            <th class="column-title">&nbsp;</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="odd pointer">
                            <td class=" "><h4><strong>
                                <?php if ($this->_tpl_vars['data_arr'][0]['latitude']): ?>

                                Change Your latitude and longitude						

                                <?php else: ?>

                                Add Your latitude and longitude							

                                <?php endif; ?>
                            </strong></h4>
                            </td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr class="even pointer">
                            <td class=" ">Enter Your Latitude</td>
                            <td class=" "><input id="txt_latitude" name="txt_latitude" type="text" maxlength="50" value="<?php if ($this->_tpl_vars['edit_latitude']): ?><?php echo $this->_tpl_vars['cls_site']->hentity($this->_tpl_vars['edit_latitude']); ?>
<?php endif; ?>"/></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr class="odd pointer">
                            <td class=" ">Enter Your Longitude</td>
                            <td class=" "><input id="txt_longitude" name="txt_longitude" type="text" maxlength="50" value="<?php if ($this->_tpl_vars['edit_longitude']): ?><?php echo $this->_tpl_vars['cls_site']->hentity($this->_tpl_vars['edit_longitude']); ?>
<?php endif; ?>"/></td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                          <tr>
                          	<td class=" ">
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" align="center" style="padding-left:100px;">
                                    <?php if ($this->_tpl_vars['data_arr']): ?>
                                      <input name="change" type="submit" class="btn btn-success" value="Change Content" onClick="return validate();"/>
                                    <?php else: ?>
                                      <input name="add" type="submit" class="btn btn-success" value="Add Content" onClick="return validate();"/></td>
                                    <?php endif; ?>
                                    </div>
                                  </div>
                            </td>
                            <td class=" ">&nbsp;</td>
                            <td class=" ">&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </form>
                <script language="javascript" type="text/javascript">
					document.form_add.category[0].focus();
				</script>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.smarty_include_php.php');
smarty_core_smarty_include_php(array('smarty_file' => "footer.php", 'smarty_assign' => '', 'smarty_once' => false, 'smarty_include_vars' => array()), $this); ?>
