<?php

/**************************************************************************************

Created by 	:Anith M.S

Created on 	:16-049-2010

Name       	:contactus.php

Purpose		:List Contactus

**************************************************************************************/

require_once 'init.php';err_status("init.php included");

header_view($site." -List Contact");err_status("header included");



$tb_contact		=	"suarezrem_contactus";



if(isset($_POST["btn_delete"]))

	{

		err_status("inside of post of delete");		

		$cls_site->db_delete_combo_log($tb_contact,"id",$_POST['checkone'],"`name`,email,phone,details,ip");

		err_status("Deleted Successfully");	

		$_SESSION["sess_err"]	=	"Contact(s) deleted successfully";

		header("location:contactus.php");exit;

	}

	

$sql		=	"SELECT * FROM $tb_contact order by id desc";		

$cnt_data	=	$cls_db->getdbcount_sql($sql);

$spage		=	$cls_site->create_paging("n_page",$cnt_data,$global_pg_limit);

$links		=	$spage->s_get_links($_REQUEST["n_page"]);

$limit		=	$spage->s_get_limit($_REQUEST["n_page"]);	

$data_arr 	=	$cls_db->getdbcontents_sql($sql.$limit);

$smarty->assign("paging",$links);

$smarty->assign("data_arr",$data_arr);

if(!$data_arr)	$_SESSION["sess_err"]	=	"No Contacts available!";



if($_SESSION["sess_err"])

	{

		$smarty->assign("TPL_MESS",$_SESSION["sess_err"]);

		$_SESSION["sess_err"]="";

	}

$smarty->display('contactus.tpl.html');

?>

