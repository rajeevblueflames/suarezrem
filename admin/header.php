<?php
/****************************************************************************************
Created by 		:Rajeev.R
Module			:Admin area
PHP File		:header.php
Template File	:Nil
Created on 		:24-07-2009
Purpose			:Manage Admin
****************************************************************************************/
$cls_site			=	& $GLOBALS['cls_site'];
$cls_db				=	& $GLOBALS['cls_db'];
$cls_fun			=	& $GLOBALS['cls_fun'];

if(!$cls_site->check_session())
	{
		header("location:index.php");
		exit;
	}	
	
function checkpage($file)

	{

		$filename	=	$_SERVER["SCRIPT_NAME"];

		$filename	=	end(explode("/",$filename));

		if($filename	==	$file)	return " style='background-color:#ABBADE;'";

		else						return "";

	}
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Suarezrem- Admin</title>
    <link REL="SHORTCUT ICON" HREF="../images/favicon.ico">
    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container" style="background: #2A3F54;">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border:0; background-color: #eee;">
              <a href="index.php" class="site_title"><img style="max-width:148px;" src="../images/logo.png"></a>
            </div>
			      <br>
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix" style="height:10px;">
              <a href="index.php" class="site_title"><span>Suarezrem- Admin</span></a>
            </div>
            <!-- /menu profile quick info -->

            <br />
            <br />
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li <?php echo checkpage(""); ?> ><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="changepwd.php">Change Password</a></li>
                    </ul>
                  </li>
                  <li <?php echo checkpage(""); ?> ><a><i class="fa fa-info-circle"></i> Meta Tags <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="home_meta.php">Home Meta Tag</a></li>
                      <li><a href="gallery_meta.php">Gallery Tags</a></li>
                      <li><a href="newyork_meta.php">Newyork Tags</a></li>
                      <li><a href="advantage_meta.php">Advantage Tags</a></li>
                      <li><a href="service_meta.php">Service Tags</a></li>
                    </ul>
                  </li>
                  <li <?php echo checkpage(""); ?> ><a><i class="fa fa-info-circle"></i> About <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="webelive.php">WE BELIEVE</a></li>
                      <li><a href="weserve.php">WE SERVE</a></li>
                      <li><a href="wesee.php">WE SEE</a></li>
                    </ul>
                  </li>

                  <li <?php echo checkpage("");?>  ><a href="team.php"><i class="fa fa-film"></i> Team</a></li>
                  <li <?php echo checkpage(""); ?> ><a href="services.php"><i class="fa fa-book"></i> Services </a></li>
                  <li <?php echo checkpage(""); ?> ><a href="advantage.php"><i class="fa fa-briefcase"></i> Advantage Suarez </a></li>
				  <li <?php echo checkpage(""); ?> ><a href="newyork.php"><i class="fa fa-product-hunt"></i> Newyork</a></li>
                  <li <?php echo checkpage(""); ?> ><a href="development.php"><i class="fa fa-pencil"></i> Transaction Resume </a>
 				  <li <?php echo checkpage(""); ?> ><a href="gallery.php"><i class="fa fa-camera"></i> Gallery </a>
                  <li <?php echo checkpage(""); ?> ><a><i class="fa fa-phone-square"></i> Contact <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="contactus.php">View Contact </a> </li>
                      <li><a href="contact_address.php">Contact Address</a></li>
                      <li><a href="office_location.php">Office Location</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="logout.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Suarezrem- Admin
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="production/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="production/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="production/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="production/images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
