<?php
/**************************************************************************************
Created by :Sreejith vr
Created on :22-0-2012
Name       :development.php
Purpose    :Listing development Categories
**************************************************************************************/
require_once 'init.php';err_status("init.php included");
header_view("development");err_status("header included");
$adminid		=	$_SESSION[$cls_site->get_sessname()];
$tpls			=	array();
$def_data		=	array();
$edit			=	$_REQUEST["edit"];
$add			=	$_REQUEST["add"];
$ip 			=	$_SERVER['REMOTE_ADDR'];
$date_added		=	"escape now() escape";
//$category        =	$cls_db->getdbcontents_sql("select * from suarezrem_category where status=1");
//$smarty->assign("category",$category);

$tpls["norecords"]				=	"No Records Found!";//page details
$tpls["heading"]				=	"Manage development";
$tpls["pagename"]				=	"development.php";
$tpls["tplpagename"]			=	"development_manage.tpl.html";

$tpls["addcaption"]				=	"Development";//add area

$tpls["edithead"]				=	"Development";//edit area


$tpls["listcaption"]			=	"Development";//listing area
$tpls["pics"]					=	"Image";
$tpls["datedoj"]				=	"Date Added";
$tpls["ip"]						=	"IP";
$tpls["listoptions"]			=	"Options";

$tpls["width"]					=	"360";
$tpls["height"]					=	"191";

$tpls["subimagepath"]				=	"../developmentImages/developmentSubimages";	
$tpls["imagepath"]				=	"../developmentImages";
//*******DATA BASE************************************************************************************//
$def_data["table"]				=	"suarezrem_development";	

$def_data["subtable"]			=	"suarezrem_development_images";

//*******************************************************************************************//



if($_REQUEST['n_page']	!=	"")  $_SESSION["newprg"]	=  $_REQUEST['n_page'];
if(isset($_POST["addpres"]))	
	{
		header("location:".$tpls["pagename"]."?add=st");exit;
	}
if(isset($_POST["cancel"]) || ($_POST["btn_ecancel"]))	
	{
		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;		
	}
	
	
	
//..... Activate and inactivate status.................	
if($_REQUEST["statuschange"]!="")
	{
		 $cls_db->db_query("update ".$def_data["table"]." set status= !status  where id='".$_REQUEST["statuschange"]."'");
		header('location:'.$tpls["pagename"].'?n_page='.$_SESSION["newprg"]);exit;
	}
//*******************************************************************************************//
//..........EDIT VIEW..................	
if($edit !=	"" && !isset($_POST["btn_ecancel"]))
	{
		err_status("Got variable for edit");
		$edit_value		=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where id='$edit'");				
		$smarty->assign("edit_arr",$edit_value);
		$galpics		=	$edit_value[0]["image"];
		$ctgry			=	$edit_value[0]["cid"];
	}	
//*******************************************************************************************//
//..........PRESENTATION  ADDING..................	
if(isset($_POST["Submit"]))
	{
		err_status("inside of post of submit");
		foreach($_POST as $key	=>	$val) if(!is_array($val))	 $_POST[$key]	=	trim($val);extract($_POST);
		$ip 					=	$_SERVER['REMOTE_ADDR'];
		$date_added				=	"escape now() escape";
		if($title == "")	//if($title == "" || $photo_file == "")	
				$_SESSION["sess_err"]	=	"<span class='label label-warning'>Please enter all details correctly</span>";
		else
			{
				$add_arr				=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where title='$title'");
				if(!$add_arr)
					{
						$upload					=	$cls_site->create_upload(10,"jpeg,jpg,gif,png");
						$photos					=	$upload->copy("photo_file",$tpls["imagepath"],2);
						if($photos)					$upload->img_resize("100","120",$tpls["imagepath"]."/thumb");
						chmod($tpls["imagepath"]."/$photos",0755);	
						chmod($tpls["imagepath"]."/thumb/$photos",0755);	
						$pref_cnt	=	$cls_db->getdbcontents_sql("SELECT max(`preference`)+1 as maxpref FROM ".$def_data["table"]);
						$pref_cnt	=	$pref_cnt[0]["maxpref"];
						if($pref_cnt==0)
						{
							$pref_cnt=1;
						}		
						
						$fields					=	"image,title,description,ip,date_added,preference";
						$values					=	"photos,title,description,ip,date_added,pref_cnt";	
				// echo $ins="insert into suarezrem_development($fields) values('$photos','$title','$tag_name','$description','$ip','$date_added','$pref_cnt')";exit;	
						$add_id					=	$cls_db->db_insert($def_data["table"],$fields,$values);
						$cls_log->log_insert($def_data["table"],$add_id,$fields);
						$_SESSION["sess_err"]	=	"<span class='label label-success'>".$tpls["addcaption"]." added successfully</span>";
						header("location:".$tpls["pagename"]);exit;	
					}
				else
					$_SESSION["sess_err"]	=	"<span class='label label-warning'>This ".$tpls["listcaption"]." is already exist!</span>";
			}
			
	}	
//*******************************************************************************************//
//..........PRESENTATION  EDITING..................	
if(isset($_POST["Update"]))
	{
		err_status("inside of post of Update");
		foreach($_POST as $key	=>	$val) if(!is_array($val))	 $_POST[$key]	=	trim($val);extract($_POST);
		if($etitle == "")	$_SESSION["sess_err"]	=	"<span class='label label-warning'>Please enter all details correctly</span>"; // || $photo == "" 
		else
			{
				$add_arr		=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where title='$etitle' and id!='$edit'");
				if(!$add_arr)
					{
						$ip 					=	$_SERVER['REMOTE_ADDR'];
						$upload			=	$cls_site->create_upload(10,"jpeg,jpg,gif,png");
						$image			=	$upload->copy("photo",$tpls["imagepath"],2);
						if($image)		$upload->img_resize("100","120",$tpls["imagepath"]."/thumb");
						chmod($tpls["imagepath"]."/$image",0755);	
						chmod($tpls["imagepath"]."/thumb/$image",0755);	
						
						if(!$image)		$image		=	$galpics;
						else
							{
								unlink($tpls["imagepath"]."/".$galpics);	
								unlink($tpls["imagepath"]."/thumb/".$galpics);
							}
						$ecategory=$_POST['ecategory'];	
						$title	= mysql_real_escape_string($title);
						$edescription	= mysql_real_escape_string($edescription);
						$fields			=	"image,title,ip,date_added";			
						$args			=	"image='$image',title='$etitle',description='$edescription',ip='$ip' where id='$edit'";
						$cls_log->log_setupdate($def_data["table"],$edit,$fields);
						$add_id			=	$cls_db->db_update($def_data["table"],$args,1);
						$_SESSION["sess_err"]	=	"<span class='label label-success'>".$tpls["addcaption"]." updated successfully</span>";
						header('location:'.$tpls["pagename"].'?n_page='.$_SESSION["newprg"]);exit;
					}
				else
					$_SESSION["sess_err"]	=	"span class='label label-warning'>This ".$tpls["listcaption"]." is already exist!</span>";
			}
	}
//*******************************************************************************************//

$preference_arr	=	$cls_db->getdbcontents_sql("select max(preference) as max_pref,min(preference) as min_pref from ".$def_data["table"]);
$smarty->assign('maxs',$preference_arr[0]['max_pref']);
$smarty->assign('mins',$preference_arr[0]['min_pref']);

$sql			=	"Select * from ".$def_data["table"]." order by preference desc";
$cnt_dat		=	$cls_db->getdbcount_sql($sql);
$spag11			=	$cls_site->create_paging("n_page",$cnt_dat,$global_pg_limit);
$link11			=	$spag11->s_get_links($_REQUEST["n_page"]);
$limi11			=	$spag11->s_get_limit($_REQUEST["n_page"]);	
$smarty->assign("paging",$link11);
$smarty->assign("cnt_dat",$cnt_dat);
$data_arr		=	$cls_db->getdbcontents_sql($sql.$limi11);
$smarty->assign('data_arr',$data_arr);
if(!$data_arr)	$smarty->assign("TPL_MESS","No ".$tpls["listcaption"]." details available!");
if($_SESSION["sess_err"])
	{
		$smarty->assign("TPL_MESS",$_SESSION["sess_err"]);
		$_SESSION["sess_err"]="";
	}
$categorylist			=	$cls_site->get_combo_arr("category",$cls_db->getdbcontents_cond("suarezrem_category","status='1' "),"id","category","size='60'","");
$smarty->assign("TPL_CAT","$categorylist");

$ecategorylist			=	$cls_site->get_combo_arr("ecategory",$cls_db->getdbcontents_cond("suarezrem_category","status='1' "),"id","category",$ctgry,"");
$smarty->assign("TPL_ECAT","$ecategorylist");


$smarty->assign("tpls",$tpls);
$smarty->display($tpls["tplpagename"]);
?>	
