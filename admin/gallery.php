<?php
/**************************************************************************************
Created by :Sreejith vr
Created on :22-0-2012
Name       :productimages.php
Purpose    :Listing  Images
**************************************************************************************/
require_once 'init.php';err_status("init.php included");
header_view("Listing Gallery");err_status("header included");
$adminid		=	$_SESSION[$cls_site->get_sessname()];
$tpls			=	array();
$def_data		=	array();

$tpls["norecords"]				=	"No Records Found!";//page details
$tpls["heading"]				=	"Listing Gallery";
$tpls["pagename"]				=	"gallery_manage.php";
$tpls["tplpagename"]			=	"gallery.tpl.html";
//$tpls["parentpagename"]			=	"kids_care.php";
$tpls["pagename1"]				=	"gallery.php";
$tpls["addcaption"]				=	"Add Gallery";//add area
$tpls["edithead"]				=	"Edit Gallery";//edit area
$tpls["listcaption"]			=	"Title";//listing area
$tpls["pics"]					=	"Image";
$tpls["datedoj"]				=	"Date Added";
$tpls["ip"]						=	"IP";
$tpls["listoptions"]			=	"Options";
$tpls["parenttable"]			=	"suarezrem_gallery";	
$def_data["parenttable"]		=	"suarezrem_gallery";	
$def_data["table"]				=	"suarezrem_gallery";	
$tpls["imagepath"]				=	"../galleryImages";	



//*******************************************************************************************//

$edit			=	$_REQUEST["edit"];
$add			=	$_REQUEST["add"];
$cid			=	$_REQUEST["cid"];

if($cid	!=	"")
	$_SESSION["cid"] 	=	"";
if($_SESSION["cid"] 	==	"")
	$_SESSION["cid"]	=	$cid;
$cid			=	$_SESSION["cid"];

//$cat_id			=	$cls_db->getdbcontents_sql("SELECT * from ".$def_data["parenttable"]." where id='$cid'");
//$smarty->assign("cat_arr",$cat_id);
//if(!$cat_id)	
//	{
//		$_SESSION["sess_err"]	=	"<span class='label label-warning'>This category is not exist.</span>";
//		header("location:".$tpls["parentpagename"]);exit;		
//	}
$ip 			=	$_SERVER['REMOTE_ADDR'];
$date_added		=	"escape now() escape";
if(isset($_POST["addpres"]))	
	{
		header("location:".$tpls["pagename"]."?add=st");exit;
	}

if(isset($_POST["btn_cancel"]) || ($_POST["btn_ecancel"]))	
	{
		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;		
	}
	
if(isset($_POST["back"]))	
	{
		header("location:".$tpls["parentpagename"]);exit;
	}




if(isset($_POST['btn_delete']))
	{
		err_status("perform deletion");
		extract($_POST); err_status("deletion successfull");
		$del_arr	=	$_POST['checkone'];
		foreach($del_arr as $key=>$val)
			{
				$val_arr	=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]." where id='$val'");
				$img_val	=	$val_arr[0]['image'];
				unlink($tpls["imagepath"]."/".$img_val);	
				unlink($tpls["imagepath"]."/thumb/".$img_val);
			}
		$cls_site->db_delete_combo($def_data["table"],"id",$_POST['checkone']);
			$val_subarr	=	$cls_db->getdbcontents_sql("Select * from ".$def_data["subtable"]." where cid='$del_arr'");
		$cls_site->db_delete_combo($def_data["subtable"],"cid",$_POST['checkone']);
		header("location:".$tpls["pagename1"]);
		
	}	
		




	
//..... Activate and inactivate status.................	
if($_REQUEST["statuschange"]!="")
	{
		 $cls_db->db_query("update ".$def_data["table"]." set status= !status  where id='".$_REQUEST["statuschange"]."'");
		header('location:'.$tpls["pagename"].'?n_page='.$_SESSION["newprg"]);exit;
	}
//*******************************************************************************************//
//..........EDIT VIEW..................	
if($edit !=	"" && !isset($_POST["btn_ecancel"]))
	{
		err_status("Got variable for edit");
		$edit_value		=	$cls_db->getdbcontents_sql("Select * from ".$def_data["table"]."  where id='$edit'");				
		$smarty->assign("edit_arr",$edit_value);
		$galpics		=	$edit_value[0]["image"];
	}	
//*******************************************************************************************//
//*******************************************************************************************//

if(isset($_REQUEST['down']))
	{		
		$down_id	=	$_REQUEST['down']; 
		$arr		=	$cls_db->getdbcontents_sql("select * from ".$def_data["table"]." where id='$down_id'");
		$down_pref	=	$arr[0][preference];  
		$result3	=	$cls_db->getdbcontents_sql("select max(preference) from ".$def_data["table"]." where preference<'$down_pref'");
		$new_pref 	=	$result3[0]['max(preference)'];
		$result4	=	$cls_db->getdbcontents_sql("select * from  ".$def_data["table"]." where preference='$new_pref'");
		$new_id		= 	$result4[0]['id'];
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$new_pref' where id='$down_id'");
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$down_pref' where id='$new_id'");
		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;	
	}
	
//priority down	
if(isset($_REQUEST['up']))
	{		
		$up_id		=	$_REQUEST['up']; 
		$arr=$cls_db->getdbcontents_sql("select * from ".$def_data["table"]." where id='$up_id'");
		$up_pref		=	$arr[0][preference];  
		$result3		=	$cls_db->getdbcontents_sql("select min(preference) from ".$def_data["table"]." where preference>'$up_pref'   ");
		$new_pref 		=	$result3[0]['min(preference)'];
		$result4		=	$cls_db->getdbcontents_sql("select * from  ".$def_data["table"]." where preference='$new_pref'");
		$new_id			= 	$result4[0]['id'];
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$new_pref' where id='$up_id'");
		$cls_db->getdbcontents_sql("update ".$def_data["table"]." set preference='$up_pref' where id='$new_id'");		
		header("location:".$tpls["pagename"]."?n_page=".$_SESSION["newprg"]);exit;	
	}	
$preference_arr	=	$cls_db->getdbcontents_sql("select max(preference) as max_pref,min(preference) as min_pref from ".$def_data["table"]."");
$smarty->assign('maxs',$preference_arr[0]['max_pref']);
$smarty->assign('mins',$preference_arr[0]['min_pref']);



$sql			=	"Select * from ".$def_data["table"]."  order by preference desc";
$cnt_dat		=	$cls_db->getdbcount_sql($sql);
$spag11			=	$cls_site->create_paging("n_page",$cnt_dat,$global_pg_limit);
$link11			=	$spag11->s_get_links($_REQUEST["n_page"]);
$limi11			=	$spag11->s_get_limit($_REQUEST["n_page"]);	
$smarty->assign("paging",$link11);
$smarty->assign("cnt_dat",$cnt_dat);
$data_arr		=	$cls_db->getdbcontents_sql($sql.$limi11);
$smarty->assign('data_arr',$data_arr);
if(!$data_arr)	$smarty->assign("TPL_MESS","No Gallery image(s) available");
if($_SESSION["sess_err"])
	{
		$smarty->assign("TPL_MESS",$_SESSION["sess_err"]);
		$_SESSION["sess_err"]="";
	}
$smarty->assign("tpls",$tpls);
$smarty->display($tpls["tplpagename"]);
?>	
