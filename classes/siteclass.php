<?php
/**************************************************************************************
Created by 	:Sreeraj
Created on 	:from 20:7:2008
Name       	:siteclass.php	
Purpose		:Initial functions
**************************************************************************************/
class siteclass
	{
		//variables
		private	$sitename			=	"";
		private	$user_session		=	"";
		private	$dbclass			=	"";
		private	$usertable			=	"";
		public	$cls_spaging		=	"";
		private $logclass			=	"";
		//constructor		
		function __construct($sitename="",$user_session="",$dbclassname="",$usertable="")
			{
				if(!$sitename)			$sitename		=	"www.website.com";
				if(!$user_session)		$user_session	=	"sess_user";
				if(!$dbclassname)		$dbclassname	=	"cls_db";
				if(!$usertable)			$usertable		=	"users";
				$this->sitename			=	$sitename;
				$this->user_session		=	$user_session;
				$this->dbclass			=	& $GLOBALS[$dbclassname];
				$this->usertable		=	$usertable;
			}
		function setlog($logclass="")
			{
				if($logclass)	$this->logclass		=	& $GLOBALS[$logclass];
			} 

		//method for paging
		function create_paging($varname,$tot_cnt,$per_page=10,$tot_links=10,$stable_links=3)
			{
				return 	new spaging($varname,$tot_cnt,$per_page,$tot_links,$stable_links);
			}
		//method for uploading
		function create_upload($max_size=10,$files="")
			{
				return 	new upload($max_size,$files);
			}
		//method for paypal implementation
		function create_paypal_sd($email,$live=1)
			{
				return 	new paypal_sd($email,$live);
			}
		//method for sms api implementation
		function create_zni_sms($gb_userid,$gb_apikey,$gb_cdma,$gb_gsm,$gb_url)
			{
				return 	new znisms($gb_userid,$gb_apikey,$gb_cdma,$gb_gsm,$gb_url);
			}
		//methods for sitename
		function set_sitename($sitename)
			{
				$this->sitename	=	$sitename;
			}
		function get_sitename()
			{
				return $this->sitename;
			}
		//methods for sessname
		function set_sessname($sessname)
			{
				$this->user_session	=	$sessname;
			}
		function get_sessname()
			{
				return $this->user_session;
			}
		//getting the members data(logged in user)
		function get_user_data()
			{
				if($_SESSION[$this->get_sessname()]	<>	"")
					{
						return $this->dbclass->getdbcontents_id($this->usertable,$_SESSION[$this->get_sessname()]);
					}
			}
		//to get trhe current page name
		function getPageName()
			{
				return end(explode("/",$_SERVER["SCRIPT_FILENAME"]));
			}
		//to check whether a user logged in or not
		function check_session()
			{
				if($_SESSION[$this->get_sessname()]	<>	"")
					{
						return $_SESSION[$this->get_sessname()];
					}
			}
		function displayTime($date_value)//date format in dd mon year hr:min
			{
				$arr	=	explode(" ",$date_value);
				$dt		=	explode("-",$arr[0]);
				$tm		=	explode(":",$arr[1]);
				return date("d M Y H:i",mktime($tm[0],$tm[1],0,$dt[1],$dt[2],$dt[0]));
			}
		function dateformat($date_value)	//date format in dd mon year
			{
				$arr=explode("-",$date_value);
				return date("d M Y",mktime(0,0,0,$arr[1],$arr[2],$arr[0]));
			}
		//stripslashes
		function strip($str)
			{
				return stripslashes($str);
			}
		//nl2br
		function lbreak($str)
			{
				$lbstr	=	ucfirst(stripslashes($str));
				return nl2br($lbstr);
			}
		//htmlentities
		function hentity($str)
			{
				$entity	=	ucfirst(stripslashes($str));
				return htmlentities($entity);
			}
		//Ucfirst
		function ufirst($str)
			{
				$ufstr	=	stripslashes($str);
				return ucfirst($ufstr);
			}
		//ucwords
		function uwords($str)
			{
				$uallstr	=stripslashes($str);
				return ucwords($uallstr);
			}
		//print_r
		function prints($str)
			{
			  print_r($str);
			}	
		//eval
		function evl($str)
			{
				return eval($str);
			}
		//floor
		function floor($val)
			{
				return floor($val);
			}
		//ceil
		function ceil($val)
			{
				return ceil($val);
			}
		function reduceLength($str,$len="30")
			{
				$str_len	=	strlen(stripslashes($str));
				if($str_len	>$len)	$strs	=	substr($str,0,$len)."..";
				else                $strs	=	$str;
				return $strs;
					
			}
		function get_fck($name,$val="")
			{
				$str	=	'<input type="hidden" id="'.$name.'" name="'.$name.'"
							 value="'.$val.'" style="display:none" />
							 <input type="hidden" id="'.$name.'___Config" value="" 
							 style="display:none" />
							 <iframe id="'.$name.'___Frame" 
							 src="fckeditor/editor/fckeditor.html?InstanceName='.$name.'&amp;
							 Toolbar=Default" width="100%" height="400" 
							 frameborder="0" scrolling="no"></iframe>';
				return	$str;
			}	
		//Special case 
		function get_fckArr($name,$ids,$val="")
			{
				$str	=	'<input type="hidden" id="'.$ids.'" name="'.$name.'"
							 value="'.$val.'" style="display:none" />
							 <input type="hidden" id="'.$ids.'___Config" value="" 
							 style="display:none" />
							 <iframe id="'.$ids.'___Frame" 
							 src="fckeditor/editor/fckeditor.html?InstanceName='.$ids.'&amp;
							 Toolbar=Default" width="80%" height="250" 
							 frameborder="0" scrolling="yes"></iframe>';
				return	$str;
			}								
		//delete all for checkboxes
		function db_delete_combo($tbnames,$fieldname,$arr_checkone,$ret_field="")
			{
				$comma_separated = implode(",", $arr_checkone);
				if($ret_field	<>	"")
					{
						$gets		= 	$this->dbclass->getdbcontents_cond($tbnames,"$fieldname in($comma_separated)");
						$arr_cnt	=	-1;
						$ret_arr	=	array();	
						for($i=0;$i<count($gets);$i++)
							{
								$arr_cnt++;
								$ret_arr[$arr_cnt]	=	$gets[$i][$ret_field];
							}
					}
				$deletequery	= "delete from $tbnames where $fieldname in($comma_separated)";
				if(!$queries    = mysql_query($deletequery));				
				if($ret_field	<>	"")return $ret_arr;
			}
		function db_delete_combo_log($tbnames,$fieldname,$arr_checkone,$fields,$ret_field="")
			{
				$comma_separated = implode(",", $arr_checkone);
				if($ret_field	<>	"")
					{
						$gets		= 	$this->dbclass->getdbcontents_cond($tbnames,"$fieldname in($comma_separated)");
						$arr_cnt	=	-1;
						$ret_arr	=	array();	
						for($i=0;$i<count($gets);$i++)
							{
								$arr_cnt++;
								$ret_arr[$arr_cnt]	=	$gets[$i][$ret_field];
							}
					}
				foreach($arr_checkone as $key=>$val)
					{
						$this->logclass->log_setdelete($tbnames,$val,$fields);
						$deletequery	= "delete from $tbnames where $fieldname='".$val."'";
						if(!$queries    = mysql_query($deletequery));
						if($queries)	
							{
								$this->logclass->log_delete();
								$this->logclass->log_unsetdelete();
							}
						else	$this->logclass->log_unsetdelete();
					}
				if($ret_field	<>	"")return $ret_arr;
			}

		//for checking the data exist in the table
		function checkfields($tablename,$fields,$values,$returnfield)
				{
					$values		=	str_replace(",","Ace_2255_escape_comma",$values);//escape from coma
					$fieldsarr	=	split(",",$fields);//accept the values to array,seperated by commas
					$valuesarr	=	split(",",$values);
					$flag		=	"";//to give the comdition and
					for($i=0;$i<count($fieldsarr);$i++)
						{	
							if($i!=0)
								$flag		=	" and ";
							$valuesarr[$i]	=	str_replace("Ace_2255_escape_comma",",",$valuesarr[$i]);
							$condition		.=	"$flag ltrim(rtrim($fieldsarr[$i])) = '$valuesarr[$i]'";//condition of the query
						}
					$sql	=	"select * from $tablename where $condition";//Got the exact query
					$res	=	mysql_query($sql);// or die(mysql_error());
					$row	=	mysql_num_rows($res);// or die(mysql_error());
					$count	=	mysql_fetch_object($res);
					if($row)//if it has same value
						return	$count->$returnfield;
					else
						return "0";
				}
		//this function will resize the image(jpg,gif,png,bmp,wbmp)
		function img_resize($widthtmp,$heighttemp,$src,$des)
			{	
				$filename 	= 	$src;
				$dest_file 	= 	$des;
				$fnamerev	=	strrev($filename);
				$expfname	=	explode(".",$fnamerev);///for extension 
				$expfname1	=	$expfname[0];
				$filetype	=	strrev($expfname1);
				$ftype 		=	$filetype;
				$percent 	= 	0.5;
				
				$a			=	getimagesize($filename);// Get new sizes
				$old_w		=	$a[0];
				$old_h		=	$a[1];
				$new_w		=	$a[0];
				$new_h		=	$a[1];
			
				if($old_w	>	$widthtmp)
					{
						$new_w	=	$widthtmp; 
						$x		=	round(($new_w*100)/$old_w);
						$new_h	=	round($old_h*($x/100));
						$old_h	=	$new_h;
					}	
				if($old_h	>	$heighttemp)
					{		
						$new_h	=	$heighttemp;
						$x		=	round(($new_h*100)/$old_h);
						$new_w	=	round($new_w*($x/100)); 
					}
				list($width, $height) = getimagesize($filename);
							
				if(($a[0]>$widthtmp) or ($a[1]>$heighttemp))	$thumb 		= imagecreatetruecolor($new_w, $new_h);
				else											$thumb 		= imagecreatetruecolor($old_w, $old_h);
							
				if(strtolower($ftype)	==	'jpeg') $ftype="jpg";
				if(strtolower($ftype)	==	'bmp' )	$ftype="wbmp";
				
				if(strtolower($ftype)	==	'jpg' )	$source 	= imagecreatefromjpeg($filename);
				if(strtolower($ftype)	==	'gif' )	$source 	= imagecreatefromgif($filename);
				if(strtolower($ftype)	==	'png' )	$source 	= imagecreatefrompng($filename);
				if(strtolower($ftype)	==	'wbmp')	$source 	= imagecreatefromwbmp($filename);
		   
				imagecopyresized($thumb, $source, 0, 0, 0, 0, $new_w, $new_h, $width, $height);// Resize
						
				if(strtolower($ftype)=='jpg')	imagejpeg($thumb, $dest_file); 
				if(strtolower($ftype)=='gif')	imagegif($thumb, $dest_file); 
				if(strtolower($ftype)=='png')	imagepng($thumb, $dest_file); 
				if(strtolower($ftype)=='wbmp')	imagewbmp($thumb, $dest_file); 
			}
		
			
		// name=> select box name,$arr=>data array,$valname=>value index name of arry,$dataname=> data index name of array, $selected => value to be selected,$params=> Extra params of select box(like class etc) 
		function get_combo_arr($name,$arr,$valname,$dataname,$selectd="",$params="")
			{
				$str		=	"<select name='$name', class='form-control' $params>";
				if($selectd	==	"")		$str		.=	"<option selected value=''>Select</option>";
				else					$str		.=	"<option value=''>Select</option>";
				if(is_array($arr))
					{
						foreach ($arr as $key	=>	$val)
							{
								$val[$dataname]		=	stripslashes($val[$dataname]);
								
								if($selectd)
									{
										if($selectd	==	$val[$valname])
											$str		.=	"<option selected value='".$val[$valname]."'>".stripslashes($val[$dataname])."</option>";
										else
											$str		.=	"<option value='".$val[$valname]."'>".stripslashes($val[$dataname])."</option>";
									}
								else
									$str		.=	"<option value='".$val[$valname]."'>".$val[$dataname]."</option>";
							}
					}
				$str		.=	"</select>";
				return $str;
			}
		function get_combo_name_change($name,$arr,$valname,$dataname,$selectd="",$params="",$name_cha)
			{
				$str		=	"<select name='$name' $params>";
				if($selectd	==	"")		$str		.=	"<option selected value=''>".$name_cha."</option>";
				else 					$str		.=	"<option value=''>".$name_cha."</option>";
				if(is_array($arr))
					{
						
						foreach ($arr as $key	=>	$val)
							{
								$val[$dataname]		=	stripslashes($val[$dataname]);
								
								if($selectd)
									{
										if($selectd	==	$val[$valname])
											$str		.=	"<option selected value='".$val[$valname]."'>".stripslashes($val[$dataname])."</option>";
										else
											$str		.=	"<option value='".$val[$valname]."'>".stripslashes($val[$dataname])."</option>";
									}
								else
									$str		.=	"<option value='".$val[$valname]."'>".$val[$dataname]."</option>";
							}
					}
				$str		.=	"</select>";
				return $str;
			}	
		//name=> select box name,$start=>starting number of series,$end=>ending number of series,$selected => value to be selected,$params=> Extra params of select box	
		function get_combo($name,$start,$end,$selectd="",$valname="",$params="",$options="")
			{
				$str		=	"<select name='$name' $params>";
				$str		.=	"<option selected value=''>".$selectd."</option>";//displaying first value as select
				$str		.=	$options;
				for($i=$start;$i<=$end;$i++)
					{
						if($valname != "")
							{
								if($valname	==	$i)
									$str		.=	"<option selected value='".$i."'>".$i."</option>";
								else
									$str		.=	"<option value='".$i."'>".$i."</option>";
							}
						else
							$str		.=	"<option value='".$i."'>".$i."</option>";
					}
				$str		.=	"</select>";
				return  $str;
			}
		////Class for getting dob year
		function get_dobyear($name,$params="",$valname="")
			{
				
				$endyr		=	date('Y');
				$startyr	=	$endyr-'100';
				$str		=	"<select name='$name' $params class='selectbx'>";
				$str		.=	"<option value='' selected='selected'> Year &nbsp;&nbsp;</option>";
				for ($i = $endyr; $i >=$startyr ; $i-- )
					{
						if($valname != "")
							{
								if($valname	==	$i)
									$str		.=	"<option selected value='".$i."'>".$i."</option>";
								else
									$str		.=	"<option value='".$i."'>".$i."</option>";
							}
						else
							$str		.=	"<option value='".$i."'>".$i."</option>";
					}
				$str		.=	"</select>";
				return  $str;
			}
		//class for dobyr selected
		function get_dobyrselect($name,$selectd="")
			{
				$endyr		=	date('Y')-'5';
				$startyr	=	$endyr-'95';
				$str		 =	"<select name='$name' class='selectbx'>";		
				for ($i = $startyr; $i <= $endyr; $i++ )
					{
						if($i	==	$selectd)
							$str	.="<option value='$i' selected>$i</option>";
						else
							$str	.="<option value='$i'>$i</option>";
					}
				$str		.=	"</select>";
				return  $str;
			}
		//class for year selected , function created by SUNITHA on 8/04/09
		function get_yearselect($name,$selectd="")
			{
				$endyr		=	date('Y');
				$startyr	=	'2008';
				$str		 =	"<select name='$name' class='selectbx'>";		
				for ($i = $startyr; $i <= $endyr; $i++ )
					{
						if($i	==	$selectd)
							$str	.="<option value='$i' selected>$i</option>";
						else
							$str	.="<option value='$i'>$i</option>";
					}
				$str		.=	"</select>";
				return  $str;
			}		
		function get_years($name,$params="")
			{
				$startyr	=	date('Y') ;
				$endyr		=	date('Y')+8;
				$str		=	"<select name='$name' $params class='selectbx'>";
				$str		.=	"<option value='0' selected='selected'> Year &nbsp;</option>";
				for ($i = $startyr; $i <= $endyr; $i++ )
					{
						$str	.="<option value='$i'>$i</option>";
					}
				$str		.=	"</select>";
				return  $str;
			}
		function get_dates($name)
			{
				$start		=	'1';
				$end		=	'31';
				$str		=	"<select name='$name' class='selectbx'>";
				$str		.=	"<option value='0' selected='selected'> Select </option>";
				for ($i = $start; $i <= $end; $i++ )
					{
						$str	.="<option value='$i'>$i</option>";
					}
				$str		.=	"</select>";
				return  $str;
			}
		
		function get_dateselect($name,$selectd="")
			{
				$start		=	'1';
				$end		=	'31';
				$str		=	"<select name='$name' class='selectbx'>";
				$str		.=	"<option value='0' selected='selected'> Select </option>";
				for ($i = $start; $i <= $end; $i++ )
					{
						if($i	==	$selectd)
							$str	.="<option value='$i' selected>$i</option>";
						else
							$str	.="<option value='$i'>$i</option>";
					}
				$str		.=	"</select>";
				return  $str;
			}
		function get_months($name,$params="")
			{
				$str		=	"<select name='$name' $params class='selectbx'>";
				$str		.=	"<option value='' selected='selected'>Month</option>";
				$str		.=	"<option value='January'>January</option>";
				$str		.=	"<option value='February'>February</option>";
				$str		.=	"<option value='March'>March</option>";
				$str		.=	"<option value='April'>April</option>";
				$str		.=	"<option value='May'>May</option>";
				$str		.=	"<option value='June'>June</option>";	
				$str		.=	"<option value='July'>July</option>";	
				$str		.=	"<option value='August'>August</option>";	
				$str		.=	"<option value='September'>September</option>";
				$str		.=	"<option value='October'>October</option>";	
				$str		.=	"<option value='November'>November</option>";
				$str		.=	"<option value='December'>December</option>";
				$str		.=	"</select>";
				return  $str;
			}
		function get_selected($name,$selectd="")
			{
				$str		 =	"<select name='$name' class='selectbx'>";
				for($i=1;$i<=12;$i++)
					{
						if($i	==	$selectd)
							$str		.=	"<option value='".$i."' selected>".$this->getmonth($i)."</option>";
						else
							$str		.=	"<option value='".$i."'>".$this->getmonth($i)."</option>";
					}
				$str		.=	"</select>";
				return  $str;
			}	
		function createRandom($no=6)
			 {
				$chars = "abcdefghijkmnopqrstuvwxyz023456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
				srand((double)microtime()*1000000);
				$i = 1;
				$pass = '' ;
				while ($i <= $no)
					{
						$num = rand() % 33;
						$tmp = substr($chars, $num, 1);
						$pass = $pass . $tmp;
						$i++;
					}			
				return $pass;
			 }
		function createRandomNum($no=6)
			 {
				$chars = "1234567890";
				srand((double)microtime()*1000000);
				$i = 1;
				$pass = '' ;
				while ($i <= $no)
					{
						$num = rand() % 9;
						$tmp = substr($chars, $num, 1);
						$pass = $pass . $tmp;
						$i++;
					}			
				return $pass;
			 }
		function getmonth($m=0) 
			{	
				return (($m==0 ) ? date("F") : date("F", mktime(0,0,0,$m)));
			}
		function get_multiple($name,$arr,$valname,$dataname,$selectd="",$params="")
				{
					$sel_arr	=	explode(",",$selectd);
					$co_arr		=	count($sel_arr);
					$str		=	"<select name='$name' $params>";
					if($selectd	==	"")		$str		.=	"<option selected value='0'>Any</option>";
					else					$str		.=	"<option value='0'>Any</option>";	
					if(is_array($arr))
						{
							
							foreach ($arr as $key	=>	$val)
								{
									$val[$dataname]		=	stripslashes($val[$dataname]);
									if($selectd)
										{
											$flag	=	0;
											for($a=0;$a<$co_arr;$a++)
											{
												if($val[$valname]==$sel_arr[$a])
												{
												$str		.=	"<option selected value='".$val[$valname]."'>".stripslashes($val[$dataname])."</option>";
												$flag		=	1;
												}	
											}
											if($flag!=1)
												$str		.=	"<option value='".$val[$valname]."'>".stripslashes($val[$dataname])."</option>";
											
										}
									else
										$str		.=	"<option value='".$val[$valname]."'>".$val[$dataname]."</option>";
								}
						}
					$str		.=	"</select>";
					return $str;
				}
		function sendmail($to,$from,$subject,$message,$files="")
			{
				$headers 		= 	"From: $from";
				$semi_rand 		= 	md5(time());// boundary
				$mime_boundary 	= 	"==Multipart_Boundary_x{$semi_rand}x";

				// headers for attachment
				$headers 		.= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\"";

				// multipart boundary
				$message = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n";
				$message .= "--{$mime_boundary}\n";
					
				if(is_array($files))// preparing attachments
					{
						for($x=0;$x<count($files);$x++)
							{
								$file 	=	fopen($files[$x],"rb");
								$data 	=	fread($file,filesize($files[$x]));
								$data 	=	chunk_split(base64_encode($data));
								$fname	=	end(explode("/",$files[$x]));
								fclose($file);
								$message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$fname\"\n" .
											 "Content-Disposition: attachment;\n" . " filename=\"$files[$x]\"\n" .
											 "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
								$message .= "--{$mime_boundary}\n";
							}
					}
				$ok = @mail($to, $subject, $message, $headers);// send
				return $ok;
			}
		function sendEmail($to,$subject,$message)
			{
				$headers 			.= 'MIME-Version: 1.0' . "\n";
				$headers 			.= 'Content-type: text/html; charset=iso-8859-1' . "\n";
				$headers    		.= "From: mb.blueflamesonline.com  <admin@blueflamesonline.com>\n";
				$ok = @mail($to,$subject,$message,$headers);// send
				return $ok;
			}
		//zipFiles("xyz.zip",array("dir1","dir2"),array("file1","file2"));
		function zipFiles($zipname,$dir_arr,$file_arr)
			{
				$zip		= 	  new ZipArchive();   
				if($zip->open($zipname, ZIPARCHIVE::CREATE)	!=		TRUE)	exit("cannot open <$zipname>\n");
				
				$num_file	=	count($file_arr);
				for($i=0;$i<$num_file;$i++)		$zip->addFile($file_arr[$i]);
				$num_dir	=	count($dir_arr);	
				for($i=0;$i<$num_dir;$i++)
					{
						if(!is_dir($dir_arr[$i]))	return false;
						$files 		= 	  array();
						$dirs 		= 	  array($dir_arr[$i]);
						while(NULL	!==	($dir = array_pop( $dirs)))
							{
								if($dh = opendir($dir))
									{
										while(false	!==		($file	=	readdir($dh)))
											{
												if($file	==	'.' || $file	==	'..')	continue;
												$path = $dir . '/' . $file;
												if(is_dir($path))	$dirs[]		= 	$path;	
												else
													{
														$files[]	=	$path;
														$zip->addFile($path);
													}
											}	
										closedir($dh);
									}		
							}
					}
				$zip->close();
			}
		function isie()
			{
				if(strstr($_SERVER["HTTP_USER_AGENT"],"MSIE"))	return 1;
				else											return 0;
			}
		function array_remove($arr,$value) 
			{
				return array_values(array_diff($arr,array($value)));
			}
		function validateEmail($email)
			{
				if (eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) return true;
				else return false;				
			}
	}	
//class for paging
class spaging
	{
		private  $perpage			=	"";//nu																														mber of records per page
		private  $totcnt			=	"";//total number of records
		private  $s_current_page	=	"";//current page
		private  $s_stable_links	=	"";//stable links on the links
		private  $s_total_links		=	"";//total links
		private  $varname			=	"";//htnl variabe for paging
		private  $s_param			=	"";//additional parameters 
		
		function geturl($param,$sql)
			{
				if($param	==	"")	$param	=	"1-".$this->perpage;
				$params						=	explode("-",$param);
				$s_current_page_tmp			= 	$params["0"];
				$s_per_page					= 	$params["1"];
				$sql_limit					=	($s_per_page	*	$s_current_page_tmp)	-	$s_per_page;
				$sql_q						=	"$sql limit $sql_limit,$s_per_page";
				$res						=	mysql_query($sql_q);
				return $res;
			}
		private function s_get_nos($start,$end,$totnos)
			{
				$arraycount	=	-1;
				$arraycount++;
				$arr_data[$arraycount]	=	$start;
				$diff	=	($end-$start)/($totnos-1);
				for($i=1;$i<=$totnos-2;$i++)
					{
						$start	=	$start + $diff;
						if($start >=$end )$start	=	$end-1;
						if($arr_data[$arraycount]	>=	$end-1) $start	=	"";
						$arraycount++;
						$arr_data[$arraycount]	=	ceil($start);
					}
				$arraycount++;
				$arr_data[$arraycount]	=	$end;
				return($arr_data);
			}
		function  __construct($varname,$tot_cnt,$per_page=10,$tot_links=10,$stable_links=3)
			{
				$this->set_per_page($per_page);
				$this->set_tot_count($tot_cnt);
				$this->set_varname($varname);
				$this->set_total_links($tot_links);
				$this->set_stable_links($stable_links);
			}
		function set_varname($name)
			{
				$this->varname	=	$name;				
			}
		function set_total_links($num)
			{
				$this->s_total_links	=	$num;
			}		
		function set_stable_links($num)
			{
				$this->s_stable_links	=	$num;
			}
		function set_per_page($num)
			{
				$this->perpage	=	$num;
			}
		function set_tot_count($num)
			{
				$this->totcnt	=	$num;
			}
		function s_get_limit($param)
			{
				if($param	==	"")	$param	=	"1-".$this->perpage;
				$params						=	explode("-",$param);
				$s_current_page_tmp			= 	$params["0"];
				$s_per_page					= 	$params["1"];
				$sql_limit					=	($s_per_page	*	$s_current_page_tmp)	-	$s_per_page;
				$sql_q						=	" limit $sql_limit,$s_per_page";
				return $sql_q; 
			}
		function get_cur_page($param)
			{
				if($param	==	"")	$param	=	"1-0";
				$params		=	explode("-",$param);
				$s_current_page_tmp	= 	$params["0"];
				$s_per_page			= 	$params["1"];
				return $s_current_page_tmp;
			}
		function get_per_page($param)
			{
				if($param	==	"")	$param	=	"1-0";
				$params		=	explode("-",$param);
				$s_current_page_tmp	= 	$params["0"];
				$s_per_page			= 	$params["1"];
				return $s_per_page;
			}
		function s_get_links($params,$s_param="")
			{
				$s_current_page		=	$this->get_cur_page($params);
				$totcnt				=	$this->totcnt;
				$s_per_page			=	$this->perpage;
				$s_stable_links		=	$this->s_stable_links;
				$s_total_links		=	$this->s_total_links;
				$varname			=	$this->varname;
				$s_total_rows		=	$totcnt;//getting total no of rows
				
				if($s_total_rows	==	0)	return "";//if no of total row is zero then the function will stop
				$s_arraycount	=	-1;//to increment array index
				
				//setting the start point of stavle link to sree_numbertostart_stable
				if(($s_numbertostart_stable	=	$s_current_page - intval($s_stable_links/2)) <= 0 ) $s_numbertostart_stable=1;

				//generating links order 	start:starting no of links, end ending no of links, totnos:total no of links
				if(ceil($s_total_rows/$s_per_page)<=$s_total_links)
					{
						for($i=1;$i<=ceil($s_total_rows/$s_per_page);$i++)
							{
								$s_arraycount++;
								$arr_data[$s_arraycount]	=	$i;
							}
						$varwait	=	$arr_data[(count($arr_data))-1];
					}
				else
					{
						$arr_data	=	$this->s_get_nos(1,ceil($s_total_rows/$s_per_page),$s_total_links);
						$varwait	=	$arr_data[(count($arr_data))-1];
						$s_flag	=	0;
						for($i=0;$i<count($arr_data);$i++)
							{
								if($s_flag	==	0)
									{
										if($varwait	==	$s_current_page)
											{
												for($j=1;$j<=$s_stable_links;$j++)
													{
														if($arr_data[(count($arr_data))-$j] <> "")
															{
																$arr_data[(count($arr_data))-$j] = $varwait-($j-1);
															}
														$s_flag				=	1;
													}	
												break;
											}
										if($arr_data[$i]	>=	$s_numbertostart_stable)
											{
												$s_assign_stabele	=	$i;
												$s_flag				=	1;
												for($j=1;$j<=$s_stable_links;$j++)
													{
														if($arr_data[$s_assign_stabele] <> "")
															{
																$arr_data[$s_assign_stabele] = $s_numbertostart_stable;
															}
														$s_numbertostart_stable++;
														$s_assign_stabele++;
													}
											}
									}
							}
					}
				//getting $_GET variables
				$tempstr	=	"";
				foreach($_GET as $key=>$val)
					if ($key	<>	$varname)	$tempstr	.=	$key."=".$val."&";
				if($tempstr)	$ex_params	=	"&".substr($tempstr,0,strlen($tempstr)-1);

				$arr_data[(count($arr_data))-1]	=	$varwait;	
				$flag	=	0;
				for($i=0;$i<count($arr_data);$i++)
					if($arr_data[$i]	==	$s_current_page)	$flag	=	1;
				
				if($flag	==	0)	$arr_data[(count($arr_data))-2]	=	$s_current_page;
				$s_return_stringT	=	'<table cellpadding="0" cellspacing="0" border="0" ><tr>';
				for($i=0;$i<count($arr_data);$i++)
					{
						if($arr_data[$i] == $s_current_page)
							{
								$s_return_string	.=	"<td align='left' style='background-color:#FFFFFF;width:19px;height:19px;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;text-align:center;color:#000000'>$arr_data[$i]</td><td align='left' style='width:5px;'>&nbsp;</td>";
								continue;
							}
						if($arr_data[$i] <> "")
							$s_return_string	.=	"<td align='left' style='background-color:#FFFFFF;width:19px;height:19px;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;text-align:center;color:#000000'><a style='color:#000000;text-decoration:none' href = '$url?$varname=$arr_data[$i]-$s_per_page";
						if($s_param	<> "")
							$s_return_string	.= "&$s_param$ex_params'>$arr_data[$i]</a>";
						else
							$s_return_string	.= "$ex_params'>$arr_data[$i]</a></td><td align='left' style='width:5px;'>&nbsp;</td>";
					}
				if(end($arr_data)	<>	$s_current_page)
					{
						$s_current_page_tmp	=	$s_current_page+1;
						$s_return_string	.=	"<td align='left'> <a href = '$url?$varname=$s_current_page_tmp-$s_per_page";
						if($s_param	<> "")
							$s_return_string	.= "&$s_param$ex_params'><img src='images/next.jpg' border='0' alt='Next'></a></td>";
						else
							$s_return_string	.= "$ex_params'><img src='images/next.jpg' border='0' alt='Next'></a></td>";
					}
				if($arr_data[0]		<>	$s_current_page)
					{
						if($s_param	<> "")
							$s_return_stringN	= "&$s_param$ex_params'><img src='images/previous.jpg' border='0' alt='Previous'></a></td><td align='left' style='width:5px;'>&nbsp;</td>";
						else
							$s_return_stringN	= "$ex_params'><img src='images/previous.jpg' border='0' alt='Previous'></a></td><td align='left' style='width:5px;'>&nbsp;</td>";
						$s_current_page_tmp	=	$s_current_page-1;
						$s_return_string	=	$s_return_stringT."<td align='left'><a href = '$url?$varname=$s_current_page_tmp-$s_per_page".$s_return_stringN.$s_return_string;
					}
				else
					{
						$s_return_string	=	$s_return_stringT.$s_return_string;
					}
				
				$s_return_string	.=	'</tr></table>';
				if(count($arr_data)	==	"1") return "";
				return($s_return_string);
			}
			
			
			
			
			
			
			
			
			function s_get_links1($params,$s_param="")
			{
				$s_current_page		=	$this->get_cur_page($params);
				$totcnt				=	$this->totcnt;
				$s_per_page			=	$this->perpage;
				$s_stable_links		=	$this->s_stable_links;
				$s_total_links		=	$this->s_total_links;
				$varname			=	$this->varname;
				$s_total_rows		=	$totcnt;//getting total no of rows
				
				if($s_total_rows	==	0)	return "";//if no of total row is zero then the function will stop
				$s_arraycount	=	-1;//to increment array index
				
				//setting the start point of stavle link to sree_numbertostart_stable
				if(($s_numbertostart_stable	=	$s_current_page - intval($s_stable_links/2)) <= 0 ) $s_numbertostart_stable=1;

				//generating links order 	start:starting no of links, end ending no of links, totnos:total no of links
				if(ceil($s_total_rows/$s_per_page)<=$s_total_links)
					{
						for($i=1;$i<=ceil($s_total_rows/$s_per_page);$i++)
							{
								$s_arraycount++;
								$arr_data[$s_arraycount]	=	$i;
							}
						$varwait	=	$arr_data[(count($arr_data))-1];
					}
				else
					{
						$arr_data	=	$this->s_get_nos(1,ceil($s_total_rows/$s_per_page),$s_total_links);
						$varwait	=	$arr_data[(count($arr_data))-1];
						$s_flag	=	0;
						for($i=0;$i<count($arr_data);$i++)
							{
								if($s_flag	==	0)
									{
										if($varwait	==	$s_current_page)
											{
												for($j=1;$j<=$s_stable_links;$j++)
													{
														if($arr_data[(count($arr_data))-$j] <> "")
															{
																$arr_data[(count($arr_data))-$j] = $varwait-($j-1);
															}
														$s_flag				=	1;
													}	
												break;
											}
										if($arr_data[$i]	>=	$s_numbertostart_stable)
											{
												$s_assign_stabele	=	$i;
												$s_flag				=	1;
												for($j=1;$j<=$s_stable_links;$j++)
													{
														if($arr_data[$s_assign_stabele] <> "")
															{
																$arr_data[$s_assign_stabele] = $s_numbertostart_stable;
															}
														$s_numbertostart_stable++;
														$s_assign_stabele++;
													}
											}
									}
							}
					}
				//getting $_GET variables
				$tempstr	=	"";
				foreach($_GET as $key=>$val)
					if ($key	<>	$varname)	$tempstr	.=	$key."=".$val."&";
				if($tempstr)	$ex_params	=	"&".substr($tempstr,0,strlen($tempstr)-1);

				$arr_data[(count($arr_data))-1]	=	$varwait;	
				$flag	=	0;
				for($i=0;$i<count($arr_data);$i++)
					if($arr_data[$i]	==	$s_current_page)	$flag	=	1;
				
				if($flag	==	0)	$arr_data[(count($arr_data))-2]	=	$s_current_page;
				$s_return_stringT	=	'<table cellpadding="0" cellspacing="0" border="0" ><tr>';
				for($i=0;$i<count($arr_data);$i++)
					{
						if($arr_data[$i] == $s_current_page)
							{
								$s_return_string	.=	"<td align='left' style='width:19px;height:19px;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;text-align:center;color:#FFFFFF'>";
								if($arr_data[$i] == '1')
									$s_return_string	.=	"$arr_data[$i]</td><td align='left' style='width:5px;'>&nbsp;</td>";
								else
									$s_return_string	.=	"| &nbsp; $arr_data[$i] </td><td align='left' style='width:5px;'>&nbsp;</td>";
								continue;
							}
						if($arr_data[$i] <> "")
							$s_return_string	.=	"<td align='left' style='width:19px;height:19px;font-family:Arial, Helvetica, sans-serif;font-size:12px;font-weight:bold;text-align:center;color:#FFFFFF'><a style='color:#FFFFFF;text-decoration:none' href = '$url?$varname=$arr_data[$i]-$s_per_page";
						if($s_param	<> "")
							$s_return_string	.= "&$s_param$ex_params'>$arr_data[$i]</a>";
						else
							$s_return_string	.= "$ex_params'>";
						if($arr_data[$i] == '1')
							$s_return_string	.=  "$arr_data[$i] </a></td><td align='left' style='width:5px;'>&nbsp;</td>";
						else
							$s_return_string	.=  "| &nbsp; $arr_data[$i] </a></td><td align='left' style='width:5px;'>&nbsp;</td>";
					}
				$s_return_string	=	$s_return_stringT.$s_return_string;
				
				$s_return_string	.=	'</tr></table>';
				if(count($arr_data)	==	"1") return "";
				return($s_return_string);
			}
			
	}
	
//class for uploading files
class upload
	{
		private $name		=	"";
		private $max_size	=	"";
		private $files		=	"";
		private $tmp_name	=	"";
		private $size		=	"";
		private $filename	=	"";
		private $error		=	"";
		private $des		=	"";
		private $upfile		=	"";
		
		function  __construct($max_size=10,$files="")
			{
				$this->set_max_size($max_size);
				$this->set_files($files);
			}
		public function  set_name($name)
			{
				$this->name	=	$name;
			}
		public function set_max_size($size_mb=10)
			{
				if($size	<=	0)	$size	=	10;//size in MB
				$this->max_size	=	$size * (1024 * 1024);
			}
		public function set_files($files)
			{
				$this->files	=	$files;
			}
		private function  set_error($err)
			{
				$this->error	=	$err;
			}
		public function  get_status()
			{
				if(!$this->error) return false;
				else return $this->error;
			}
		public function copy($filename,$des,$filetype="1",$name="")
			{
				//fileteype=1 for same file name,fileteype=2 for random file name
				$opath	=	$des;
				$path	=	$des;
				$this->set_error("");
				if(!$filename)	$this->set_error("Not a valid Filename");
				$this->name		=	$filename;
				$this->tmp_name	=	$_FILES[$filename]["tmp_name"];
				$this->size		=	$_FILES[$filename]["size"];
				$this->filename	=	$_FILES[$filename]["name"];
				$ext			=	pathinfo($this->filename);
				$ext			=	strtolower($ext["extension"]);

				if($this->size>$this->max_size)		$this->set_error("File size is too large!");
				$flag	=	1;
				if($this->files)
					{
						$arr_ext	=	array();
						$arr_ext	=	explode(",",$this->files);
						$flag		=	0;
						foreach ($arr_ext	as $val)	if(strtolower($val)	==	$ext)	$flag	=	1;
					}
				if($flag	!=	1)	$this->set_error("File Format is Invalid");
				if(!$this->error)
					{
						if($name)
							{
								$des	.=	"/".$name;
							}
						else
							{
								if($filetype	==	"1") $des	.=	"/".$this->name;
								if($filetype	==	"2")
									{
										$tmpfname = tempnam("$opath/","");
										rename($tmpfname,$tmpfname.".".$ext);
										$temp	=	explode("/",$tmpfname);
										$temp	=	end($temp);
										$des	.=	"/".$temp.".".$ext;
									}
							}
						if(copy($this->tmp_name,$des))
							{
								$this->set_error("Copied Successfully");
								$temp		=	explode("/",$des);
								$this->des		=	$des;	
								$this->upfile	=	end($temp);
								return end($temp);				
							}
						else 
							{
								$this->set_error("Cannot Copy the file");
								return "0";	
							}
					}					
			}
		public function copy_spec($filename,$des,$index=0,$filetype="1",$name="")
			{
				//fileteype=1 for same file name,fileteype=2 for random file name
				$path	=	$des;
				$this->set_error("");
				if(!$filename)	$this->set_error("Not a valid Filename");
				$this->name		=	$filename;
				$this->tmp_name	=	$_FILES[$filename]["tmp_name"][$index];
				$this->size		=	$_FILES[$filename]["size"][$index];
				$this->filename	=	$_FILES[$filename]["name"][$index];
				$ext			=	pathinfo($this->filename);
				$ext			=	strtolower($ext["extension"]);

				if($this->size>$this->max_size)		$this->set_error("File size is too large!");
				$flag	=	1;
				if($this->files)
					{
						$arr_ext	=	array();
						$arr_ext	=	explode(",",$this->files);
						$flag		=	0;
						foreach ($arr_ext	as $val)	if(strtolower($val)	==	$ext)	$flag	=	1;
					}
				if($flag	!=	1)	$this->set_error("File Format is Invalid");
				if(!$this->error)
					{
						
						if($name)
							{
								$des	.=	"/".$name;
							}
						else
							{
								if($filetype	==	"1") $des	.=	"/".$this->name;
								if($filetype	==	"2")
									{
										$tmpfname = tempnam("$path/","");
										rename($tmpfname,$tmpfname.".".$ext);
										$temp	=	explode("/",$tmpfname);
										$temp	=	end($temp);
										$des	.=	"/".$temp.".".$ext;
									}
							}
						if(copy($this->tmp_name,$des))
							{
								$this->set_error("Copied Successfully");
								$temp		=	explode("/",$des);
								if(end($temp)	==	"")	$temp		=	explode("\\",$des);
								$this->des		=	$des;	
								$this->upfile	=	end($temp);
								return end($temp);				
							}
						else 
							{
								$this->set_error("Cannot Copy the file");
								return "0";	
							}
					}					
			}
		public function img_resize($widthtmp,$heighttemp,$des="",$del="0")
			{
				if($src	==	"") $src	=	$this->des;
				if($des	==	"")	$des	=	$this->des;
				else			$des	.=	"/".$this->upfile;	
					
				$filename 	= 	$src;
				$dest_file 	= 	$des;
				$fnamerev	=	strrev($filename);
				$expfname	=	explode(".",$fnamerev);///for extension 
				$expfname1	=	$expfname[0];
				$filetype	=	strrev($expfname1);
				$ftype 		=	$filetype;
				$percent 	= 	0.5;
				
				$a			=	getimagesize($filename);// Get new sizes
				$old_w		=	$a[0];
				$old_h		=	$a[1];
				$new_w		=	$a[0];
				$new_h		=	$a[1];
			
				if($old_w	>	$widthtmp)
					{
						$new_w	=	$widthtmp; 
						$x		=	round(($new_w*100)/$old_w);
						$new_h	=	round($old_h*($x/100));
						$old_h	=	$new_h;
					}	
				if($old_h	>	$heighttemp)
					{		
						$new_h	=	$heighttemp;
						$x		=	round(($new_h*100)/$old_h);
						$new_w	=	round($new_w*($x/100)); 
					}
				list($width, $height) = getimagesize($filename);
							
				if(($a[0]>$widthtmp) or ($a[1]>$heighttemp))	$thumb 		= imagecreatetruecolor($new_w, $new_h);
				else											$thumb 		= imagecreatetruecolor($old_w, $old_h);
							
				if(strtolower($ftype)	==	'jpeg') $ftype="jpg";
				if(strtolower($ftype)	==	'bmp' )	$ftype="wbmp";
				
				if(strtolower($ftype)	==	'jpg' )	$source 	= imagecreatefromjpeg($filename);
		   		if(strtolower($ftype)	==	'gif' )	$source 	= imagecreatefromgif($filename);
		   		if(strtolower($ftype)	==	'png' )	$source 	= imagecreatefrompng($filename);
		   		if(strtolower($ftype)	==	'wbmp')	$source 	= imagecreatefromwbmp($filename);
		   
				imagecopyresized($thumb, $source, 0, 0, 0, 0, $new_w, $new_h, $width, $height);// Resize
						
				if(strtolower($ftype)=='jpg')	imagejpeg($thumb, $dest_file); 
				if(strtolower($ftype)=='gif')	imagegif($thumb, $dest_file); 
				if(strtolower($ftype)=='png')	imagepng($thumb, $dest_file); 
				if(strtolower($ftype)=='wbmp')	imagewbmp($thumb, $dest_file); 
				
				if ($del	<>	"0")	unlink($this->des);
			}
		}
//class for paypal
class paypal_sd
	{
		private $email		=	"";
		private $live		=	"";
		private	$params		=	array();
		private $path		=	"";
			
		function  __construct($email,$live=1)
			{
				$this->email	=	$email;
				if($live)			$this->live		=	1;
				$this->path		=	$_SERVER["REQUEST_URI"];
				$this->add_arg("business",$this->email);
				$this->add_arg("return",$this->path);
				$this->add_arg("cancel_return",$this->path);
				$this->add_arg("no_note","1");
				$this->add_arg("currency_code","USD");
				$this->add_arg("lc","US");			
			}
		function add_arg($name,$value,$type="hidden")
			{
				$var_key	=	$this->isexist($name);
				if($var_key	==	"NA")
					$this->params[]	=	array("name"=>$name,"value"=>$value,"type"=>$type);
				else
					{
						$this->params[$var_key]["name"]		=	$name;
						$this->params[$var_key]["value"]	=	$value;
						$this->params[$var_key]["type"]		=	$type;
					}
			}
		private function isexist($name)
			{
				$flag	=	0;
				$flag2	=	0;
				foreach($this->params	as	$key	=>	$val)
					{
						if($val["name"]	==	$name) 
							{
								$flag	=	1;
								$flag2	=	$key;
								break;							
							}
					}
				if($flag)	return $flag2;
				else		return "NA";
			}
		function go($amt)
			{
				if($this->live)
					$action	=	"https://www.paypal.com/cgi-bin/webscr";
				else
					$action	=	"https://www.sandbox.paypal.com/webscr";
					
				$str	=	"<html><head><body><form name='paypal' action='$action' method='post'>";
				foreach($this->params	as	$key	=>	$val)
					{
						$str	.=	"<input type='".$val['type']."' name='".$val['name']."' value='".$val['value']."'>";
					}
				$str	.=	"</form><script>document.paypal.submit();</script></body></head></html>";
				ob_clean();
				echo $str;
			}
	}
	
//class for Sending SMS 
class znisms
	{
		private $gb_userid		=	"";
		private $gb_apikey		=	"";
		private $gb_cdma		=	"";
		private $gb_gsm			=	"";
		private $gb_url			=	"";	
		function  __construct($gb_userid,$gb_apikey,$gb_cdma,$gb_gsm,$gb_url)
			{
				$this->gb_userid	=	$gb_userid;
				$this->gb_apikey	=	$gb_apikey;
				$this->gb_cdma		=	$gb_cdma;
				$this->gb_gsm		=	$gb_gsm;
				$this->gb_url		=	$gb_url;
				
			}
		private function createsmsxml($rcvnos,$message)
			{
				$stringData =	'<?xml version="1.0" encoding="ISO-8859-1" ?>';
				$message	=	str_replace("�","x",$message);
				$message	=	str_replace("�","",$message);
				$message	=	str_replace("?","",$message);
				$message	=	str_replace("?","'",$message);
				$message	=	str_replace("?","'",$message);
				$message	=	str_replace("?","-",$message);
				$message	=	str_replace("?","...",$message);
				$message	=	str_replace("?","*",$message);
				$message	=	str_replace("?","'",$message);
				$message	=	str_replace("?","'",$message);
				$message	=	str_replace("?","",$message);
				$message	=	str_replace("�","",$message);
				$message	=	str_replace("�","",$message);
				$message	=	str_replace("�","",$message);
				$message	=	str_replace("?","-",$message);
				$message	=	str_replace("�","",$message);
				$message	=	str_replace("?","",$message);
				$message	=	str_replace("?","",$message);
				// Assigning to attributes 
				$stringData	.=	"
								<push>
								<from>".$this->gb_userid."\$".$this->gb_apikey."</from>
								<gsm>".$this->gb_gsm."</gsm>
								<cdma>".$this->gb_cdma."</cdma>
								<message><![CDATA[$message]]></message>
								<mobileno>";
								
				foreach($rcvnos as $key	=>$val)	$stringData	.=	"<nos>$val</nos>";	
				$stringData	.=	"</mobileno></push>";	
				return 	$stringData;
				
				
			}
		public function sendsms($rcvnos,$message)
			{
				
				$xmlData	=	$this->createsmsxml($rcvnos,$message);
				$ch = curl_init();// create a new cURL resource
				curl_setopt($ch, CURLOPT_URL, $this->gb_url);// set URL and other appropriate options
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);
				curl_exec($ch);// grab URL and pass it to the browser
				curl_close($ch);// close cURL resource, and free up system resources	
				return $ch;
			}	
			
	}
class logclass
	{
		private	$dbclass		=	"";
		private	$siteclass		=	"";
		private $tablename		=	"";
		private $updatefields	=	array();
		private $deletefields	=	array();
				
		function  __construct($dbclass,$siteclass,$tblname)
			{
				$this->dbclass		=	& $GLOBALS[$dbclass];
				$this->siteclass	=	& $GLOBALS[$siteclass];
				$this->tablename	=	$tblname;
				$this->create_table($tblname);
			}
		private function create_table($tblname)
			{
				$str	=	"	CREATE TABLE IF NOT EXISTS `$tblname` (
								`id` bigint(11) NOT NULL auto_increment,
								`actionid` bigint(11) NOT NULL COMMENT 'action performed by',
								`ip` varchar(225) NOT NULL COMMENT 'ip address',
								`actiontype` varchar(225) NOT NULL COMMENT 'insert,delete,update,select',
								`phpself`  varchar(225) NOT NULL,
								`tablename` varchar(225) NOT NULL,
								`tableid` bigint(11) NOT NULL COMMENT 'primary key of table_name',
								`date` datetime NOT NULL,
								`notes` longtext NOT NULL,
								PRIMARY KEY  (`id`)
								) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for tracking the actions of admin';
							";
				$this->dbclass->db_query($str);
			}
		public function log_insert($tblname,$new_id,$fields)
			{
				$comma_separated 	=	explode(",", $fields);
				$gets				= 	$this->dbclass->getdbcontents_id($tblname,$new_id);
				$field_count		=	count($comma_separated);
				for($i=0;$i<$field_count;$i++)
					{
						$field_name	=	$comma_separated[$i];
						$field_val	=	$gets[0][$field_name];
						
						$tempnote	=	"$field_name - $field_val";
						$notes		.=	$tempnote	;
						$notes		.=	"<br>";
					}
				$ip				=	$_SERVER['REMOTE_ADDR'];
				$type			=	"INSERT";
				$adminid		=	$this->siteclass->check_session();
				$adminid		=	mysql_real_escape_string($adminid);
				$ip				=	mysql_real_escape_string($ip);
				$type			=	mysql_real_escape_string($type);
				$phpself		=	mysql_real_escape_string($_SERVER['PHP_SELF']);
				$table_name		=	mysql_real_escape_string($tblname);
				$id_used		=	mysql_real_escape_string($new_id);
				$note			=	mysql_real_escape_string($notes);
				$fields			=	"actionid,ip,actiontype,phpself,tablename,tableid,date,notes";
				$vals			=	"'$adminid','$ip','$type','$phpself','$table_name','$id_used',now(),'$note'";
				$this->dbclass->db_query("insert into ".$this->tablename." ($fields) values($vals)");					
			}
		public function log_setupdate($tblname,$new_id,$fields)
			{
				$comma_separated 	=	explode(",", $fields);
				$gets				= 	$this->dbclass->getdbcontents_id($tblname,$new_id);
				$field_count		=	count($comma_separated);
				for($i=0;$i<$field_count;$i++)
					{
						$field_name	=	$comma_separated[$i];
						$field_val	=	$gets[0][$field_name];
						$tempnote	=	"$field_name - $field_val";
						$notes		.=	$tempnote	;
						$notes		.=	"<br>";
					}
				$this->updatefields["table"]	=	$tblname;
				$this->updatefields["id"]		=	$new_id;
				$this->updatefields["fields"]	=	$fields;
				$this->updatefields["oldnotes"]	=	"Before<br>".$notes;
			}
		public function log_update()
			{
				if(!$this->updatefields)	return;

				$tblname	=	$this->updatefields["table"];
				$new_id		=	$this->updatefields["id"];	
				$fields		=	$this->updatefields["fields"];
				$oldnotes	=	$this->updatefields["oldnotes"];
				
				$comma_separated 	=	explode(",", $fields);
				$gets				= 	$this->dbclass->getdbcontents_id($tblname,$new_id);
				$field_count		=	count($comma_separated);
				for($i=0;$i<$field_count;$i++)
					{
						$field_name	=	$comma_separated[$i];
						$field_val	=	$gets[0][$field_name];
						$tempnote	=	"$field_name - $field_val";
						$notes		.=	$tempnote	;
						$notes		.=	"<br>";
					}
				$notes			=	$oldnotes."After<br>".$notes;
				$ip				=	$_SERVER['REMOTE_ADDR'];
				$type			=	"UPDATE";
				$adminid		=	$this->siteclass->check_session();
				$adminid		=	mysql_real_escape_string($adminid);
				$ip				=	mysql_real_escape_string($ip);
				$type			=	mysql_real_escape_string($type);
				$phpself		=	mysql_real_escape_string($_SERVER['PHP_SELF']);
				$table_name		=	mysql_real_escape_string($tblname);
				$id_used		=	mysql_real_escape_string($new_id);
				$note			=	mysql_real_escape_string($notes);
				$fields			=	"actionid,ip,actiontype,phpself,tablename,tableid,date,notes";
				$vals			=	"'$adminid','$ip','$type','$phpself','$table_name','$id_used',now(),'$note'";
				$this->dbclass->db_query("insert into ".$this->tablename." ($fields) values($vals)");
				$this->updatefields	=	array();					
			}
		public function log_setdelete($tblname,$new_id,$fields)
			{
				$comma_separated 	=	explode(",", $fields);
				$gets				= 	$this->dbclass->getdbcontents_id($tblname,$new_id);
				$field_count		=	count($comma_separated);
				for($i=0;$i<$field_count;$i++)
					{
						$field_name	=	$comma_separated[$i];
						$field_val	=	$gets[0][$field_name];
						$tempnote	=	"$field_name - $field_val";
						$notes		.=	$tempnote	;
						$notes		.=	"<br>";
					}
				$this->deletefields["table"]	=	$tblname;
				$this->deletefields["id"]		=	$new_id;
				$this->deletefields["fields"]	=	$fields;
				$this->deletefields["oldnotes"]	=	$notes;
			}
		public function log_delete()
			{
				if(!$this->deletefields)	return;

				$tblname	=	$this->deletefields["table"];
				$new_id		=	$this->deletefields["id"];	
				$fields		=	$this->deletefields["fields"];
				$oldnotes	=	$this->deletefields["oldnotes"];
				
				$notes			=	$oldnotes;
				$ip				=	$_SERVER['REMOTE_ADDR'];
				$type			=	"DELETE";
				$adminid		=	$this->siteclass->check_session();
				$adminid		=	mysql_real_escape_string($adminid);
				$ip				=	mysql_real_escape_string($ip);
				$type			=	mysql_real_escape_string($type);
				$phpself		=	mysql_real_escape_string($_SERVER['PHP_SELF']);
				$table_name		=	mysql_real_escape_string($tblname);
				$id_used		=	mysql_real_escape_string($new_id);
				$note			=	mysql_real_escape_string($notes);
				$fields			=	"actionid,ip,actiontype,phpself,tablename,tableid,date,notes";
				$vals			=	"'$adminid','$ip','$type','$phpself','$table_name','$id_used',now(),'$note'";
				$this->dbclass->db_query("insert into ".$this->tablename." ($fields) values($vals)");
				$this->deletefields	=	array();					
			}
		public function log_select($tblname="",$new_id="",$notes="")
			{
				$this->log_create("s",$tblname,$new_id,$notes);
			}
		//$type	=	i,s,d,u(insert,select,delete,update)
		public function log_create($type,$tblname="",$new_id="",$notes="")
			{
				if(strtolower(trim($type))	==	"i")		$types	=	"INSERT";
				elseif(strtolower(trim($type))	==	"s")	$types	=	"SELECT";
				elseif(strtolower(trim($type))	==	"d")	$types	=	"DELETE";
				elseif(strtolower(trim($type))	==	"u")	$types	=	"UPDATE";
				else return;
				$type			=	$types;						
				$ip				=	$_SERVER['REMOTE_ADDR'];
				$adminid		=	$this->siteclass->check_session();
				$adminid		=	mysql_real_escape_string($adminid);
				$ip				=	mysql_real_escape_string($ip);
				$type			=	mysql_real_escape_string($type);
				$phpself		=	mysql_real_escape_string($_SERVER['PHP_SELF']);
				$table_name		=	mysql_real_escape_string($tblname);
				$id_used		=	mysql_real_escape_string($new_id);
				$note			=	mysql_real_escape_string($notes);
				$fields			=	"actionid,ip,actiontype,phpself,tablename,tableid,date,notes";
				$vals			=	"'$adminid','$ip','$type','$phpself','$table_name','$id_used',now(),'$note'";
				$this->dbclass->db_query("insert into ".$this->tablename." ($fields) values($vals)");
				$this->deletefields	=	array();					
			}
		public function log_unsetdelete()
			{
				$this->deletefields	=	array();					
			}
		public function log_unsetupdate()
			{
				$this->updatefields	=	array();					
			}
	}
?>
