<?php
/**************************************************************************************
Created by	:	Sreeraj
Created on	:	04-11-2008
Name		:	functions.php
Purpose		:	Internal functions for the site
**************************************************************************************/
class funclass
	{
		private	$dbclass	=	"";
		private	$siteclass	=	"";
				
		function  __construct($dbclass,$siteclass)
			{
				$this->dbclass		=	& $GLOBALS[$dbclass];
				$this->siteclass	=	& $GLOBALS[$siteclass];
			}
		function getIdArray($arrSource,$arrDest,$keys)
			{
				foreach($arrSource as $key=>$val)	$arrDest[]	=	$val[$keys];
				return $arrDest;
			}
		function incrViewCount($type,$id)
			{
				if($type	==	"img")	$this->dbclass->db_update("wu_adminimage","viewcount=viewcount+1 where id = '$id'");
				else					$this->dbclass->db_update("wu_adminvideo","viewcount=viewcount+1 where id = '$id'");
			}
			
				//Youtube embed code
		function youtubeEmbedCode($code,$width="560",$height="340",$autoplay="",$color="ffffff")
			{
				if($code)
					{
						if($autoplay)	$autoplayTxt	=	"&autoplay=1";
						$code	=	'
									<object width="'.$width.'" height="'.$height.'">
									<param name="movie" value="http://www.youtube.com/v/'.$code.'&hl=en_US&rel=0&color1=0x'.$color.'&color2=0x'.$color.$autoplayTxt.'&iv_load_policy=3&&showinfo=0"></param>
									<param name="allowFullScreen" value="true"></param>
									<param name="allowscriptaccess" value="always"></param>
									<embed src="http://www.youtube.com/v/'.$code.'&hl=en_US&rel=0&color1=0x'.$color.'&color2=0x'.$color.$autoplayTxt.'&iv_load_policy=3&&showinfo=0"
									type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="'.$width.'" height="'.$height.'"></embed>
									</object>
									';
					}
				return $code;
			}
	
			
		
		function determine_age($dob)
			{
				$arr		=	explode("-",$dob);
				$birth_date	=	date("d F Y",mktime(0,0,0,$arr[1],$arr[2],$arr[0]));		
				$birth_date_time = strtotime($birth_date);
				$to_date = date('m/d/Y', $birth_date_time);
				 
				list($birth_month, $birth_day, $birth_year) = explode('/', $to_date);
				 
				$now = time();
				 
				$current_year = date("Y");
				 
				$this_year_birth_date = $birth_month.'/'.$birth_day.'/'.$current_year;
				$this_year_birth_date_timestamp = strtotime($this_year_birth_date);
				 
				$years_old = $current_year - $birth_year;
				 
				if($now < $this_year_birth_date_timestamp)
					{
						/* his/her birthday hasn't yet arrived this year */
						$years_old = $years_old - 1;
					}
				 
				return $years_old;
			}
	function get_combo_number_desc($name,$start,$end,$selectd="",$params="",$n,$option="Select")
		{
			$str		=	"<select name='$name' $params>";
			if($selectd	==	"")		$str		.=	"<option selected value=''>".$option."</option>";
			else					$str		.=	"<option value=''>".$option."</option>";//displaying first value as select
			$str		.=	$options;
			for($i=$end;$i>=$start;$i-=$n)
				{
					if($selectd)
						{
							if($selectd	==	$i)		$str		.=	"<option selected value='".$i."'>".$i."</option>";
							else					$str		.=	"<option value='".$i."'>".$i."</option>";
						}
					else
						$str		.=	"<option value='".$i."'>".$i."</option>";
				}
			$str		.=	"</select>";
			return  $str;
		}	
			
		function get_age($dbirth)
			{
				$date_arr		=	explode("-",$dbirth);
				$month 			=	$date_arr[1];
				$day 			=	$date_arr[2];
				$year			=	$date_arr[0];
				$born_in 		= 	mktime(0, 0, 0, $month , $day, $year);
				$current_date	=	mktime(0, 0, 0, date("m") , date("d"), date("Y"));
				$age 			=	floor(($current_date - $born_in)/ 31536000);
				return $age;
			}	
			
	//only for matrimony interest expression
	function getdbcontents_ints($table,$cond1,$cond2)
		{
					$fn_sql	=	"select * from $table where sender='$cond1' and receiver='$cond2'";
					$fn_res		=	mysql_query($fn_sql);
					$arrcnt		=	-1;
					$dataarr	=	array();
					while($temp	= mysql_fetch_assoc($fn_res))
						{
							$arrcnt++;
							$dataarr[$arrcnt]	=	$temp;
						}
				
			return $dataarr;
		}
		
	function get_combo_number($name,$start,$end,$selectd="",$params="",$n)
		{
			$str		=	"<select name='$name' $params>";
			$str		.=	"<option selected value=''>Select</option>";//displaying first value as select
			$str		.=	$options;
			for($i=$start;$i<=$end;$i+=$n)
				{
					if($selectd)
						{
							if($selectd	==	$i)		$str		.=	"<option selected value='".$i."'>".$i."</option>";
							else					$str		.=	"<option value='".$i."'>".$i."</option>";
						}
					else
						$str		.=	"<option value='".$i."'>".$i."</option>";
				}
			$str		.=	"</select>";
			return  $str;
		}	
function date_con($date)
	{
		$str	=$date;
		$com = explode("-",$str);//print_r($com);		
		if($com[1] ==jan||$com[1] ==Jan||$com[1] ==january||$com[1] ==January||$com[1] ==JANUARY)$com[1]=1;
		if($com[1]==feb||$com[1]==Feb||$com[1]==february||$com[1]==February||$com[1]==FEBRUARY)$com[1]=2;
		if($com[1]==mar||$com[1]==Mar||$com[1]==march||$com[1]==March||$com[1]==MARCH)$com[1]=3;
		if($com[1]==apr||$com[1]==Apr||$com[1]==april||$com[1]==April||$com[1]==APRIL)$com[1]=4;
		if($com[1]==may||$com[1]==May||$com[1]==may || $com[1]==MAY)$com[1]=5;
		if($com[1]==jun||$com[1]==Jun||$com[1]==june||$com[1]==June||$com[1]==JUNE)$com[1]=6;
		if($com[1]==jul||$com[1]==Jul||$com[1]==july||$com[1]==July||$com[1]==JULY)$com[1]=7;
		if($com[1]==aug||$com[1]==Aug||$com[1]==august||$com[1]==August||$com[1]==AUGUST)$com[1]=8;
		if($com[1]==sep||$com[1]==Sep||$com[1]==september||$com[1]==September||$com[1]==SEPTEMBER)$com[1]=9;
		if($com[1]==oct||$com[1]==Oct||$com[1]==october||$com[1]==October||$com[1]==OCTOBER)$com[1]=10;
		if($com[1]==nov||$com[1]==Nov||$com[1]==november||$com[1]==November||$com[1]==NOVEMBER)$com[1]=11;
		if($com[1]==dec||$com[1]==Dec||$com[1]==december||$com[1]==December||$com[1]==DECEMBER)$com[1]=12;
		$newDate = mktime(0,0,0,$com[1],$com[0],$com[2]);
		$dateRe = date("Y-m-d",$newDate);
		return $dateRe;
	}	
	
	
						function getAds($ads_category)
				{
					$cat_arr 	=	$this->dbclass->getdbcontents_cond("pancinemas_adscategory","id='$ads_category'");
					$sql		=	"select * from pancinemas_banner_ads where ads_category ='$ads_category' and status=1 ORDER BY RAND() LIMIT 1";
					$data_arr 	=	$this->dbclass->getdbcontents_sql($sql);
					$width 		=	stripslashes($data_arr[0]["width"]);
					$height 	=	stripslashes($data_arr[0]["height"]);			
					$adimg 		=	stripslashes($data_arr[0]["ads_image"]);	
					$adname 	=	stripslashes($data_arr[0]["ads_name"]);	
					$adlink 	=	stripslashes($data_arr[0]["link"]);	
					$adcode 	=	stripslashes($data_arr[0]["code"]);
					$adid		=	$data_arr[0]["id"];		
					$type 		=	$data_arr[0]["file_type"];	// 0=img 1=flash 2=code
					if($type == 1)
						{
							$val		=	'<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" 
											codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0"
											width="'.$width.'" height="'.$height.'">								
											<param name="movie" value="AdsImage/'.$adimg.'"/>
											<param name="quality" value="high" />
											<embed src="AdsImage/'.$adimg.'" quality="high" 
											pluginspage="http://www.macromedia.com/go/getflashplayer" 
											type="application/x-shockwave-flash" width="'.$width.'" height="'.$height.'"></embed>
											</object>';
						}
					
					else if($type == 2) $val		=	$adcode;
					else $val		=	'<a href="'.$adlink.'" target="_blank"><img src="AdsImage/'.$adimg.'" alt="'.$adname.'" width="100%" border="0" 
					></a>';
					return $val;
					
				}
	
function paypalMembers($order)
	{
		$ord_arr		=	$this->dbclass->getdbcontents_sql(" select * from ta_order where id='$member_id'");

		$fname			=	stripcslashes($ord_arr[0]["name"]);
		$address		=	nl2br($ord_arr[0]["address"]);
	
		$cid			=	$ord_arr[0]["country"];	
		$total			=	$ord_arr[0]["grand_total"];	
		
		$temp_arr6		=	$this->dbclass->getdbcontents_sql(" select * from ta_country where id='$cid'");
		$country		=	stripcslashes($temp_arr6[0]['country']);
			
		$email			=	stripcslashes($ord_arr[0]["email"]);	
		$phone			=	stripcslashes($ord_arr[0]["phone"]);	
						
		$mem_arr				=	array();
		$mem_arr["fname"]		=	$fname;		
		$mem_arr["country"]		=	$country;
		$mem_arr["address"]		=	$address;
		$mem_arr["phone"]		=	$phone;
		$mem_arr["email"]		=	$email;
		$mem_arr["total"]		=	$total;
		
		return 	$mem_arr;	

	}


function paypalOrder($order)
	{
		$pdate			=	date("Y-m-d H:i:s");
		$ord_arr		=	$this->dbclass->getdbcontents_sql("ta_order","id='$order'");
		$this->dbclass->db_query("update ta_order set paid=1,paymode=1,payment_date='$pdate' where id='$order'");		
	}

function getCountryName($id)	
	{
		$data_arr		=	$this->dbclass->getdbcontents_sql(" select * from ta_country where id='$id'");
		return $data_arr[0]["country"];

	}
function quantityThere($id,$qty=1)	
	{
		$data_arr		=	$this->dbclass->getdbcontents_sql(" select * from ta_product where id='$id' and stock>=$qty");
		if($data_arr)		return true;
		else				return false;
	}
function orderStockReduce($order_id)
	{
		$dataCheck	=	$this->dbclass->getdbcontents_sql("select * from ta_order where id='$order_id' and paid=0");
		if($dataCheck)
			{
				$data		=	$this->dbclass->getdbcontents_sql("select * from ta_orderdetails where orderid='$order_id'");
				if($data)
					{
						foreach($data	as	$val)
							{
								$pid	=	$val["product"];
								$qty	=	$val["quantity"];						
								$this->dbclass->db_query("update ta_product set stock=stock-$qty where id='$pid'");
							}
					}			

			}
	}

function getCategory($cat_id,$cnt)
	{
		$cat_pid_arr			=	$this->dbclass->getdbcontents_cond("pancinemas_category","status='1' and id=$cat_id  order by preference asc");
		$cat_pid				=	$cat_pid_arr[0]["pid"];
		$this->category_arr[$cnt]		=	$cat_id;
		if($cat_pid	!=	0)
			$this->getCategory($cat_pid,$cnt+1);
	}
}



?>
