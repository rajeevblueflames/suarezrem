<?php
/*
 * @author	Sreeraj
 * @Date	30:4:2008
 * @file	dbclass.php	
 */
class dbclass
{
	private $siteclass	=	"";
	private $logclass	=	"";
	#################                Database Functions              ###########################
	//constructor
	function setsite($siteclass="")
		{
			if($siteclass)	$this->siteclass	=	& $GLOBALS[$siteclass];
		} 
	function setlog($logclass="")
		{
			if($logclass)	$this->logclass		=	& $GLOBALS[$logclass];
		} 
	//fetching with table name and conditions
	function getdbcontents_cond($table,$cond)
		{
	      	if($cond == "")  $cond	= '1';			
			$fn_sql	=	"select * from $table where $cond";
			//echo $fn_sql;exit;
			$fn_res		=	mysql_query($fn_sql);
			$arrcnt		=	-1;
			$dataarr	=	array();
			while($temp	= mysql_fetch_assoc($fn_res))
				{
					$arrcnt++;
					$dataarr[$arrcnt]	=	$temp;
				}
			return $dataarr;
		}
	//fetching with query
	function getdbcontents_sql($sql)
		{
			
			$fn_res		=	mysql_query($sql);
			$arrcnt		=	-1;
			$dataarr	=	array();
			while($temp	= mysql_fetch_assoc($fn_res))
				{
					$arrcnt++;
					$dataarr[$arrcnt]	=	$temp;
				}
			return $dataarr;
		}
	//fetching with res
	function getdbcontents_res($res)
		{
			$arrcnt		=	-1;
			$dataarr	=	array();
			while($temp	= mysql_fetch_assoc($res))
				{
					$arrcnt++;
					$dataarr[$arrcnt]	=	$temp;
				}
			return $dataarr;
		}
	//fetching without fieldname (using primary key)
	function getdbcontents_id($table,$cond)
		{
			
			$tmpstr		=	$this->get_primarykey($table);
			if($tmpstr	<>	"")
				{
					if($cond == "")  $cond	= '1';
					else $cond	= "$tmpstr = '$cond'"; 			
					$fn_sql	=	"select * from $table where $cond";
					$fn_res		=	mysql_query($fn_sql);
					$arrcnt		=	-1;
					$dataarr	=	array();
					while($temp	= mysql_fetch_assoc($fn_res))
						{
							$arrcnt++;
							$dataarr[$arrcnt]	=	$temp;
						}
				} 
			return $dataarr;
		}		
	function getdbcontents_uid($table,$cond)
		{
			
			$tmpstr		=	"uid";
			if($tmpstr	<>	"")
				{
					if($cond == "")  $cond	= '1';
					else $cond	= "$tmpstr = '$cond'"; 			
					$fn_sql	=	"select * from $table where $cond";
					$fn_res		=	mysql_query($fn_sql);
					$arrcnt		=	-1;
					$dataarr	=	array();
					while($temp	= mysql_fetch_assoc($fn_res))
						{
							$arrcnt++;
							$dataarr[$arrcnt]	=	$temp;
						}
				} 
			return $dataarr;
		}		
	function getdbcount_cond($table,$cond="")
		{
		  	if($cond == "")  $cond	= '1';			
			$fn_sql	=	"select * from $table where $cond";
			$fn_res		=	mysql_query($fn_sql);
			return mysql_num_rows($fn_res);
		}
	//fetching with query
	function getdbcount_sql($sql)
		{
			$fn_res		=	mysql_query($sql);
			return mysql_num_rows($fn_res);
		}
	//fetching with res
	function getdbcount_res($res)
		{
			return mysql_num_rows($fn_res);
		}
	//inserting
	function db_insert($table,$fields,$values)
		{
			
			$fieldsarr				=	split(",",$fields);//accept the values to array,seperated by commas
			$valuesarr				=	split(",",$values);//accept the values to array,seperated by commas
			if(count($fieldsarr)   !=	count($valuesarr))	return "Error Occured";
			$values_org				=	"";
			for($i=0;$i<count($valuesarr);$i++)
				{
					$valuesarr[$i]	=	$GLOBALS[$valuesarr[$i]];
					if(strtolower(substr($valuesarr[$i],-6))	==	"escape" &&	strtolower(substr($valuesarr[$i],0,6))	==	"escape")
						{
							$valuesarr[$i]	=	str_replace("escape","",strtolower($valuesarr[$i]));
							$flag_escape	=	"1";
						}
					else
						{
							$flag_escape	=	"0";
						}
											
					if($i	==	0)	
						{
							if($flag_escape	==	"0")
								$values_org	    =	"'". mysql_real_escape_string($valuesarr[$i])."'";
							else
								$values_org	    =	$valuesarr[$i];
						}
					else
						{
							if($flag_escape	==	"0")
								$values_org	   .=	",'".mysql_real_escape_string($valuesarr[$i])."'";
							else
								$values_org	   .=	",".$valuesarr[$i];
						}
				}
			
			//echo "insert into $table($fields) values($values_org)";exit;
			return (mysql_query("insert into $table($fields) values($values_org)")) ? mysql_insert_id() : "0";
		}
	//updating
	function db_update($tbname,$field,$log=0)
		{	
			$updatequery	= "update $tbname set $field";
			//echo $updatequery;exit;
			$query       	= mysql_query($updatequery) or die(mysql_error("Update failed"));
			if($log)
				{
					if($query)	
						{
							$this->logclass->log_update();
							$this->logclass->log_unsetupdate();
						}
					else	$this->logclass->log_unsetupdate();
				}
			
		}
	//query
	function db_query($qry)
		{
			return mysql_query($qry);
		}
	//To get the primary key of a table
	function get_primarykey($table)
		{
			$result	=	mysql_query("SELECT * FROM $table where 0");
			$num	=	mysql_num_fields($result);
			for($i=0;$i<$num;$i++)
				{
					if(strstr(mysql_field_flags($result, $i),"primary_key"))
						return mysql_field_name($result, $i);
				}
		}
//to get the fields present in a table
	function getdbfields($table,$field="")
		{
			$fn_sql		=	"select * from $table where 0";
			$fn_res		=	mysql_query($fn_sql);
			$i			=	0;
			$arr		=	array();
			if($field)
				{
					while($temp	= mysql_fetch_field($fn_res))
						{
							$arr[$i]["$field"]			=	$temp->$field;
							$i++;
						}
				}
			else
				{
					while($temp	= mysql_fetch_field($fn_res))
						{
							$arr[$i]["name"]			=	$temp->name;
							$arr[$i]["table"]			=	$temp->table;
							$arr[$i]["def"]				=	$temp->def;
							$arr[$i]["max_length"]		=	$temp->max_length;
							$arr[$i]["not_null"]		=	$temp->not_null;
							$arr[$i]["primary_key"]		=	$temp->primary_key;
							$arr[$i]["multiple_key"]	=	$temp->multiple_key;
							$arr[$i]["unique_key"]		=	$temp->unique_key;
							$arr[$i]["numeric"]			=	$temp->numeric;
							$arr[$i]["blob"]			=	$temp->blob;
							$arr[$i]["type"]			=	$temp->type;
							$arr[$i]["unsigned"]		=	$temp->unsigned;
							$arr[$i]["zerofill"]		=	$temp->zerofill;
							$arr[$i]["type"]			=	$temp->type;
							$i++;
						}
				}
		return $arr;
		}
}
?>
