<?php
	require_once 'init.php';err_status("init.php included");  
	$advantage_arr	=	$cls_db->getdbcontents_sql("select * from suarezrem_advantage");
	$meta_contents	=	$cls_db->getdbcontents_sql("Select * from suarezrem_advantage_tags");
?>
<meta name="description" content="<?php echo $meta_contents[0]['meta_description']; ?>" />
<meta name="keywords" content="<?php echo $meta_contents[0]['meta_keyword']; ?>" />
<title><?php echo $meta_contents[0]['meta_title']; ?></title>

<?php
	header_view1("Suarezrem- Advantage");err_status("header included");

?> 
<div class="spacing40 clearfix clearboth"></div>
      <!--Suarezrem Advantage start-->
<div style="padding-bottom:0px;" id="newyork" class="content clearfix">
	<div class="container">
		<div class="row">


			<div class="col-md-12">
				<h3 class="content-title1" style="">The SuarezREM Advantage.</h3>
			</div><!--/.col-md-8-->

			<hr>
			<div class="col-md-12" style="text-align: center">
				<img src="advantageImages/<?php echo $advantage_arr[0]["image"]; ?>" style="" alt="">
			</div>
			<div class="spacing20 clearfix clearboth"></div>

			<div class="col-md-12 boxs-relative">
				<p class="textJustification">
					<?php echo $advantage_arr[0]['content'];?>
				</p>
		</div><!--/row-->
	</div><!--/container-->
	<hr />
</div>
		<!--Suarezream Advantage Ends-->
<?php
	require_once("footer1.php");
?>
	