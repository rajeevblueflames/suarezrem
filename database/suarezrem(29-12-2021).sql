-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 29, 2021 at 05:34 AM
-- Server version: 5.7.31
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suarezrem`
--

-- --------------------------------------------------------

--
-- Table structure for table `bw_adminlog`
--

CREATE TABLE `bw_adminlog` (
  `id` bigint(11) NOT NULL,
  `actionid` bigint(11) NOT NULL COMMENT 'action performed by',
  `ip` varchar(225) NOT NULL COMMENT 'ip address',
  `actiontype` varchar(225) NOT NULL COMMENT 'insert,delete,update,select',
  `phpself` varchar(225) NOT NULL,
  `tablename` varchar(225) NOT NULL,
  `tableid` bigint(11) NOT NULL COMMENT 'primary key of table_name',
  `date` datetime NOT NULL,
  `notes` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for tracking the actions of admin';

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_admin`
--

CREATE TABLE `suarezrem_admin` (
  `id` bigint(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0= inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_admin`
--

INSERT INTO `suarezrem_admin` (`id`, `username`, `password`, `status`) VALUES
(1, 'admin', 'admins', 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_adminlog`
--

CREATE TABLE `suarezrem_adminlog` (
  `id` bigint(11) NOT NULL,
  `actionid` bigint(11) NOT NULL COMMENT 'action performed by',
  `ip` varchar(225) NOT NULL COMMENT 'ip address',
  `actiontype` varchar(225) NOT NULL COMMENT 'insert,delete,update,select',
  `phpself` varchar(225) NOT NULL,
  `tablename` varchar(225) NOT NULL,
  `tableid` bigint(11) NOT NULL COMMENT 'primary key of table_name',
  `date` datetime NOT NULL,
  `notes` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for tracking the actions of admin';

--
-- Dumping data for table `suarezrem_adminlog`
--

INSERT INTO `suarezrem_adminlog` (`id`, `actionid`, `ip`, `actiontype`, `phpself`, `tablename`, `tableid`, `date`, `notes`) VALUES
(1, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 0, '2021-09-11 12:33:40', 'image - <br>title - <br>description - <br>ip - <br>date_added - <br>preference - <br>'),
(2, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 0, '2021-09-11 12:41:29', 'image - <br>title - <br>tag_name - <br>description - <br>ip - <br>date_added - <br>preference - <br>'),
(3, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 2, '2021-09-11 12:51:00', 'image - 0<br>title - Product 2<br>tag_name - Product Tag 2<br>description - <p>test</p><br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>preference - 2<br>'),
(4, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 12:51:29', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(5, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 12:51:44', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(6, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 12:52:15', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(7, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 12:52:15', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(8, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 13:05:35', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(9, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 13:05:35', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(10, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 13:12:04', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(11, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 13:12:21', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(12, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 1, '2021-09-11 13:13:28', 'pid - 1<br>image - 0<br>title - test 1<br>ip - ::1<br>date_added - 2021-09-11 13:13:28<br>preference - 1<br>'),
(13, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 2, '2021-09-11 13:13:49', 'pid - 1<br>image - 0<br>title - test 2<br>ip - ::1<br>date_added - 2021-09-11 13:13:49<br>preference - 2<br>'),
(14, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 3, '2021-09-11 13:18:45', 'pid - 1<br>image - 0<br>title - test 3<br>ip - ::1<br>date_added - 2021-09-11 13:18:45<br>preference - 3<br>'),
(15, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 4, '2021-09-11 13:19:38', 'cid - 1<br>image - 0<br>title - test 4<br>ip - ::1<br>date_added - 2021-09-11 13:19:38<br>preference - 4<br>'),
(16, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 5, '2021-09-11 14:03:13', 'cid - 2<br>image - 0<br>title - test 5<br>ip - ::1<br>date_added - 2021-09-11 14:03:13<br>preference - 5<br>'),
(17, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 2, '2021-09-11 14:08:34', 'Before<br>image - 0<br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>After<br>image - 0<br>title - Product test 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>'),
(18, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 3, '2021-09-11 16:18:33', 'image - <br>title - Product Test 3<br>tag_name - Product Tag 3<br>description - <p>Testing 3</p><br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>preference - 3<br>'),
(19, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 1, '2021-09-11 16:18:52', 'Before<br>image - 1.jpg<br>title - test add<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>After<br>image - 1.jpg<br>title - Product test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>'),
(20, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 4, '2021-09-11 16:19:26', 'image - <br>title - Product Test 4<br>tag_name - Product Tag 4<br>description - <p>Testing 4</p><br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>preference - 4<br>'),
(21, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 5, '2021-09-11 16:20:01', 'image - <br>title - Product Test 5<br>tag_name - Product Tag 5<br>description - <p>Testing 5</p><br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>preference - 5<br>'),
(22, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 6, '2021-09-11 16:20:40', 'image - <br>title - Product Test 6<br>tag_name - Product Tag 6<br>description - <p>Testing 6</p><br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>preference - 6<br>'),
(23, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 7, '2021-09-11 16:21:10', 'image - <br>title - Product Test 7<br>tag_name - Product Tag 7<br>description - <p>Testing 7</p><br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>preference - 7<br>'),
(24, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 1, '2021-09-11 17:30:52', 'Before<br>image - 1.jpg<br>title - Product test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>After<br>image - 1.jpg<br>title - Product Test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>'),
(25, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 7, '2021-09-11 17:34:28', 'Before<br>cid - 0<br>image - 7.jpg<br>title - Product Test 7<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>After<br>cid - 1<br>image - 7.jpg<br>title - Product Test 7<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>'),
(26, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 6, '2021-09-11 17:35:02', 'Before<br>cid - 0<br>image - 6.jpg<br>title - Product Test 6<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>cid - 2<br>image - 6.jpg<br>title - Product Test 6<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(27, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 5, '2021-09-11 17:35:09', 'Before<br>cid - 0<br>image - 5.jpg<br>title - Product Test 5<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>After<br>cid - 3<br>image - 5.jpg<br>title - Product Test 5<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>'),
(28, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 4, '2021-09-11 17:35:18', 'Before<br>cid - 0<br>image - 4.jpg<br>title - Product Test 4<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>After<br>cid - 4<br>image - 4.jpg<br>title - Product Test 4<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>'),
(29, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 3, '2021-09-11 17:35:27', 'Before<br>cid - 0<br>image - 3.jpg<br>title - Product Test 3<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>After<br>cid - 4<br>image - 3.jpg<br>title - Product Test 3<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>'),
(30, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 2, '2021-09-11 17:35:36', 'Before<br>cid - 0<br>image - 2.jpg<br>title - Product test 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>After<br>cid - 5<br>image - 2.jpg<br>title - Product Test 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>'),
(31, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 1, '2021-09-11 17:35:44', 'Before<br>cid - 0<br>image - 1.jpg<br>title - Product Test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>After<br>cid - 6<br>image - 1.jpg<br>title - Product Test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>'),
(32, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 8, '2021-09-11 18:21:44', 'cid - 1<br>image - <br>title - Test Product 8<br>tag_name - Product Tag 8<br>description - <p>testing 8</p><br>ip - ::1<br>date_added - 2021-09-11 18:21:44<br>preference - 8<br>'),
(33, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 6, '2021-09-13 13:00:22', 'Before<br>cid - 2<br>image - 6.jpg<br>title - Product Test 6<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>cid - 1<br>image - 6.jpg<br>title - Product Test 6.1<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(34, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 1, '2021-09-13 13:44:32', 'Before<br>cid - 6<br>image - 1.jpg<br>title - Product Test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>After<br>cid - 1<br>image - 1.jpg<br>title - STY-LUX<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>'),
(35, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 2, '2021-09-13 13:46:06', 'Before<br>cid - 5<br>image - 2.jpg<br>title - Product Test 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>After<br>cid - 2<br>image - 2.jpg<br>title - JARVIS<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>'),
(36, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 3, '2021-09-13 13:47:13', 'Before<br>cid - 4<br>image - 3.jpg<br>title - Product Test 3<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>After<br>cid - 3<br>image - 3.jpg<br>title - IRONMAN<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>'),
(37, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 4, '2021-09-13 13:48:13', 'Before<br>cid - 4<br>image - 4.jpg<br>title - Product Test 4<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>After<br>cid - 4<br>image - 4.jpg<br>title - TOASTMAN<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>'),
(38, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 5, '2021-09-13 13:49:02', 'Before<br>cid - 3<br>image - 5.jpg<br>title - Product Test 5<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>After<br>cid - 5<br>image - 5.jpg<br>title - TUNE<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>'),
(39, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 6, '2021-09-13 13:49:57', 'Before<br>cid - 1<br>image - 6.jpg<br>title - Product Test 6.1<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>cid - 6<br>image - 6.jpg<br>title - VEGA<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(40, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 7, '2021-09-13 13:50:38', 'Before<br>cid - 1<br>image - 7.jpg<br>title - Product Test 7<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>After<br>cid - 7<br>image - 7.jpg<br>title - COMET<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>'),
(41, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 0, '2021-09-13 13:51:39', 'cid - <br>image - <br>title - <br>tag_name - <br>description - <br>ip - <br>date_added - <br>preference - <br>'),
(42, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 9, '2021-09-13 13:53:03', 'cid - 8<br>image - <br>title - GALAXY<br>tag_name - The Welcome Tray<br>description - <p><span>Stylish and luxurious, this tray allows for easy maintenance and better safety.</span></p>\r\n<p>&bull; Large size W450*D320*H40 for coffee machine<br />&bull; Solid wood in ultra-strong matt black lacquer<br />&bull; Compact all in one design with compartments for kettle, coffee capsules, tea bags, sugar/creamer sticks, spoons etc<br />&bull; Round corner treatment for compartments<br />&bull; Wire guidance and lock (anti-theft) for kettle<br />&bull; 4 furniture protective gliders at bottom<br />&bull; Kettle and Mugs can be provided upon request</p><br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>preference - 8<br>'),
(43, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 6, '2021-09-14 00:13:26', 'cid - 1<br>image - <br>title - product 5<br>ip - ::1<br>date_added - 2021-09-14 00:13:26<br>preference - 6<br>'),
(44, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 7, '2021-09-14 00:13:42', 'cid - 1<br>image - <br>title - product 6<br>ip - ::1<br>date_added - 2021-09-14 00:13:42<br>preference - 7<br>'),
(45, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 8, '2021-09-14 00:13:56', 'cid - 1<br>image - <br>title - product7<br>ip - ::1<br>date_added - 2021-09-14 00:13:56<br>preference - 8<br>'),
(46, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 9, '2021-09-14 00:14:09', 'cid - 1<br>image - <br>title - product 8<br>ip - ::1<br>date_added - 2021-09-14 00:14:09<br>preference - 9<br>'),
(47, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 10, '2021-09-14 00:14:25', 'cid - 1<br>image - <br>title - product 9<br>ip - ::1<br>date_added - 2021-09-14 00:14:25<br>preference - 10<br>'),
(48, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 11, '2021-09-14 00:14:40', 'cid - 1<br>image - <br>title - product 10<br>ip - ::1<br>date_added - 2021-09-14 00:14:40<br>preference - 11<br>'),
(49, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 12, '2021-09-14 00:14:51', 'cid - 1<br>image - <br>title - product 11<br>ip - ::1<br>date_added - 2021-09-14 00:14:51<br>preference - 12<br>'),
(50, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 13, '2021-09-14 00:15:02', 'cid - 1<br>image - <br>title - product 12<br>ip - ::1<br>date_added - 2021-09-14 00:15:02<br>preference - 13<br>'),
(51, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 14, '2021-09-14 00:15:12', 'cid - 1<br>image - <br>title - product 13<br>ip - ::1<br>date_added - 2021-09-14 00:15:12<br>preference - 14<br>'),
(52, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 15, '2021-09-14 00:15:25', 'cid - 1<br>image - <br>title - product14<br>ip - ::1<br>date_added - 2021-09-14 00:15:25<br>preference - 15<br>'),
(53, 1, '::1', 'UPDATE', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 5, '2021-09-14 00:16:19', 'Before<br>image - 0<br>title - test 5<br>ip - ::1<br>date_added - 2021-09-11 14:03:13<br>After<br>image - 0<br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-11 14:03:13<br>'),
(54, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 16, '2021-09-14 00:16:30', 'cid - 2<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:16:30<br>preference - 16<br>'),
(55, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 17, '2021-09-14 00:16:40', 'cid - 2<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:16:40<br>preference - 17<br>'),
(56, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 18, '2021-09-14 00:17:13', 'cid - 3<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:17:13<br>preference - 18<br>'),
(57, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 19, '2021-09-14 00:17:29', 'cid - 3<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:17:29<br>preference - 19<br>'),
(58, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 20, '2021-09-14 00:17:39', 'cid - 3<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:17:39<br>preference - 20<br>'),
(59, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 21, '2021-09-14 00:17:50', 'cid - 3<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:17:50<br>preference - 21<br>'),
(60, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 22, '2021-09-14 00:18:20', 'cid - 4<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:18:20<br>preference - 22<br>'),
(61, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 23, '2021-09-14 00:18:30', 'cid - 4<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:18:30<br>preference - 23<br>'),
(62, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 24, '2021-09-14 00:18:39', 'cid - 4<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:18:39<br>preference - 24<br>'),
(63, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 25, '2021-09-14 00:18:47', 'cid - 4<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:18:47<br>preference - 25<br>'),
(64, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 26, '2021-09-14 00:19:25', 'cid - 5<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:19:25<br>preference - 26<br>'),
(65, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 27, '2021-09-14 00:19:33', 'cid - 5<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:19:33<br>preference - 27<br>'),
(66, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 28, '2021-09-14 00:19:41', 'cid - 5<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:19:41<br>preference - 28<br>'),
(67, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 29, '2021-09-14 00:19:49', 'cid - 5<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:19:49<br>preference - 29<br>'),
(68, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 30, '2021-09-14 00:19:58', 'cid - 5<br>image - <br>title - Product 5<br>ip - ::1<br>date_added - 2021-09-14 00:19:58<br>preference - 30<br>'),
(69, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 31, '2021-09-14 00:20:29', 'cid - 6<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:20:29<br>preference - 31<br>'),
(70, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 32, '2021-09-14 00:20:37', 'cid - 6<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:20:37<br>preference - 32<br>'),
(71, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 33, '2021-09-14 00:20:45', 'cid - 6<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:20:45<br>preference - 33<br>'),
(72, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 34, '2021-09-14 00:20:53', 'cid - 6<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:20:53<br>preference - 34<br>'),
(73, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 35, '2021-09-14 00:21:02', 'cid - 6<br>image - <br>title - Product 5<br>ip - ::1<br>date_added - 2021-09-14 00:21:02<br>preference - 35<br>'),
(74, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 36, '2021-09-14 00:21:18', 'cid - 7<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:21:18<br>preference - 36<br>'),
(75, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 37, '2021-09-14 00:21:27', 'cid - 7<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:21:27<br>preference - 37<br>'),
(76, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 38, '2021-09-14 00:22:21', 'cid - 7<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:22:21<br>preference - 38<br>'),
(77, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 39, '2021-09-14 00:22:35', 'cid - 7<br>image - <br>title - product 4<br>ip - ::1<br>date_added - 2021-09-14 00:22:35<br>preference - 39<br>'),
(78, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 40, '2021-09-14 00:22:44', 'cid - 7<br>image - <br>title - product 5<br>ip - ::1<br>date_added - 2021-09-14 00:22:44<br>preference - 40<br>'),
(79, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 41, '2021-09-14 00:26:53', 'cid - 9<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:26:53<br>preference - 41<br>'),
(80, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 42, '2021-09-14 00:27:05', 'cid - 9<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:27:05<br>preference - 42<br>'),
(81, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 43, '2021-09-14 00:27:13', 'cid - 9<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:27:13<br>preference - 43<br>'),
(82, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 44, '2021-09-14 00:27:25', 'cid - 9<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:27:25<br>preference - 44<br>'),
(83, 1, '::1', 'UPDATE', '/suarezremworld/admin/office_location.php', 'suarezrem_office_location', 1, '2021-09-14 16:01:23', 'Before<br>latitude - 9.589770<br>longitude - 76.532420<br>After<br>latitude - 9.96867<br>longitude - 76.31909<br>'),
(84, 1, '::1', 'UPDATE', '/suarezremworld/admin/office_location.php', 'suarezrem_office_location', 1, '2021-09-14 16:05:18', 'Before<br>latitude - 9.96867<br>longitude - 76.31909<br>After<br>latitude - 25.18086<br>longitude - 55.26292<br>'),
(85, 1, '::1', 'UPDATE', '/suarezremworld/admin/office_location.php', 'suarezrem_office_location', 1, '2021-09-14 16:07:27', 'Before<br>latitude - 25.18086<br>longitude - 55.26292<br>After<br>latitude - 9.589770<br>longitude - 76.31909<br>');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_category`
--

CREATE TABLE `suarezrem_category` (
  `id` bigint(11) NOT NULL,
  `category` varchar(300) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `preference` bigint(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_category`
--

INSERT INTO `suarezrem_category` (`id`, `category`, `ip`, `preference`, `date_added`, `status`) VALUES
(1, 'Retractable Hair Dryer', '::1', 1, '2021-09-10 00:00:00', 1),
(2, 'Steam Iron', '::1', 2, '2021-09-10 15:57:34', 1),
(3, 'Ironing Center', '::1', 3, '2021-09-10 16:05:15', 1),
(4, 'Toaster', '::1', 4, '2021-09-10 16:05:27', 1),
(5, 'Capsule Coffee Machine', '::1', 5, '2021-09-10 16:05:41', 1),
(6, 'Kettle VEGA', '::1', 6, '2021-09-10 16:05:53', 1),
(7, 'Kettle COMET', '::1', 7, '2021-09-10 16:06:03', 1),
(8, 'Welcome Tray', '::1', 8, '2021-09-10 16:06:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_contactus`
--

CREATE TABLE `suarezrem_contactus` (
  `id` bigint(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(300) NOT NULL,
  `email` varchar(250) NOT NULL,
  `message` longtext NOT NULL,
  `ip` varchar(250) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_contactus`
--

INSERT INTO `suarezrem_contactus` (`id`, `name`, `phone`, `email`, `message`, `ip`, `date_added`, `preference`, `status`) VALUES
(2, 'Rajeev', '8891635880', 'meetrajeev@gmail.com', 'aSfdgsfhgjf', '127.0.0.1', '2018-09-17 20:48:12', 0, 1),
(3, 'test', '13124443', 'test@gmail.com', 'test', '::1', '2021-09-14 15:40:14', 1, 1),
(4, 'test 2', '13124443', 'test@gmail.com', 'For testing', '::1', '2021-09-14 15:49:26', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_cont_address`
--

CREATE TABLE `suarezrem_cont_address` (
  `id` bigint(11) NOT NULL,
  `phone` varchar(300) NOT NULL,
  `address` longtext NOT NULL,
  `address_title` varchar(300) NOT NULL,
  `fax` varchar(300) NOT NULL,
  `mobno1` varchar(300) NOT NULL,
  `mobno2` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_cont_address`
--

INSERT INTO `suarezrem_cont_address` (`id`, `phone`, `address`, `address_title`, `fax`, `mobno1`, `mobno2`, `email`, `date_added`) VALUES
(4, '+91 98099 88154', 'In Room<br>\r\n                                Churchill Executive Tower <br>\r\n                                3512 (35th floor) <br>\r\n                                Burj Khalifa District <br>\r\n                                Dubai, UAE', 'InRoom', '', '+91 91426 62744', '', 'info@suarezrem.in', '2021-09-14 16:21:09');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_designprocess`
--

CREATE TABLE `suarezrem_designprocess` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_designprocess`
--

INSERT INTO `suarezrem_designprocess` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'Our design team', '3.jpg', 'Our team is made up of some of the brightest minds in product design from across the world. We understand that each individual has their own strengths and we choose to collaborate with different designers based on their core expertise, thus allowing us to maintain our creative edge.                                ', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_designteam`
--

CREATE TABLE `suarezrem_designteam` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_designteam`
--

INSERT INTO `suarezrem_designteam` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'Our design team', '3.jpg', 'Our team is made up of some of the brightest minds in product design from across the world. We understand that each individual has their own strengths and we choose to collaborate with different designers based on their core expertise, thus allowing us to maintain our creative edge.                                ', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_emotion`
--

CREATE TABLE `suarezrem_emotion` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_emotion`
--

INSERT INTO `suarezrem_emotion` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'The Emotion', '1.jpg', 'We are sympathetic to a collective sense of stability, coherence, movement and proportion.', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_essence`
--

CREATE TABLE `suarezrem_essence` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_essence`
--

INSERT INTO `suarezrem_essence` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'The Essence', '6.jpg', 'We need our surroundings and the entities that inhabit them to reflect our innate astuteness and perception as social beings.', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_fleets_images`
--

CREATE TABLE `suarezrem_fleets_images` (
  `id` bigint(11) NOT NULL,
  `cid` bigint(11) NOT NULL,
  `image` varchar(300) NOT NULL,
  `title` varchar(300) NOT NULL,
  `ip` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_fleets_images`
--

INSERT INTO `suarezrem_fleets_images` (`id`, `cid`, `image`, `title`, `ip`, `date_added`, `preference`, `status`) VALUES
(159, 2, '1.1.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:35:31', 0, 1),
(160, 2, '1.2.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:35:57', 1, 1),
(161, 2, '1.1.jpg', 'Fleet Image 3', '127.0.0.1', '2018-09-05 13:35:57', 2, 1),
(162, 1, '1.2.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:36:13', 3, 1),
(163, 1, '1.1.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:36:13', 4, 1),
(164, 4, '1.1.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:35:31', 0, 1),
(165, 4, '1.2.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:35:57', 1, 1),
(166, 4, '1.3.jpg', 'Fleet Image 3', '127.0.0.1', '2018-09-05 13:35:57', 2, 1),
(167, 4, '1.2.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:36:13', 3, 1),
(168, 4, '1.1.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:36:13', 4, 1),
(169, 8, '1.1.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:35:31', 0, 1),
(170, 8, '1.2.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:35:57', 1, 1),
(171, 8, '1.1.jpg', 'Fleet Image 3', '127.0.0.1', '2018-09-05 13:35:57', 2, 1),
(172, 9, '1.2.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:36:13', 3, 1),
(173, 9, '1.1.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:36:13', 4, 1),
(174, 9, '1.1.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:35:31', 0, 1),
(175, 10, '1.2.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:35:57', 1, 1),
(176, 10, '1.3.jpg', 'Fleet Image 3', '127.0.0.1', '2018-09-05 13:35:57', 2, 1),
(177, 10, '1.2.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:36:13', 3, 1),
(178, 10, '1.1.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:36:13', 4, 1),
(179, 2, '1.1.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:36:13', 4, 1),
(180, 2, '1.1.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:35:31', 0, 1),
(181, 2, '1.2.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:35:57', 1, 1),
(182, 2, '1.3.jpg', 'Fleet Image 3', '127.0.0.1', '2018-09-05 13:35:57', 2, 1),
(183, 1, '1.2.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:36:13', 3, 1),
(184, 1, '1.3.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:36:13', 4, 1),
(185, 1, '1.1.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:35:31', 0, 1),
(186, 1, '1.2.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:35:57', 1, 1),
(187, 11, '1.3.jpg', 'Fleet Image 3', '127.0.0.1', '2018-09-05 13:35:57', 2, 1),
(188, 11, '1.2.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:36:13', 3, 1),
(189, 11, '1.1.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:36:13', 4, 1),
(190, 11, '1.1.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:35:31', 0, 1),
(191, 11, '1.2.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:35:57', 1, 1),
(192, 11, '1.3.jpg', 'Fleet Image 3', '127.0.0.1', '2018-09-05 13:35:57', 2, 1),
(193, 11, '1.2.jpg', 'Fleet Image 1', '127.0.0.1', '2018-09-05 13:36:13', 3, 1),
(194, 11, '1.1.jpg', 'Fleet Image 2', '127.0.0.1', '2018-09-05 13:36:13', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_fleet_enquiry`
--

CREATE TABLE `suarezrem_fleet_enquiry` (
  `id` bigint(11) NOT NULL,
  `fname` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `phone` varchar(300) NOT NULL,
  `checkin` varchar(300) NOT NULL,
  `place` varchar(300) NOT NULL,
  `requirements` longtext NOT NULL,
  `fleets` varchar(100) NOT NULL,
  `ip` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_fleet_enquiry`
--

INSERT INTO `suarezrem_fleet_enquiry` (`id`, `fname`, `email`, `phone`, `checkin`, `place`, `requirements`, `fleets`, `ip`, `date_added`, `status`) VALUES
(1, 'Rajeev', 'rajeev@gmail.com', '365463', '01/29/2018', 'My place', 'My requirements', '', '127.0.0.1', '2018-01-26 21:40:58', 1),
(2, 'sfdgf', '', 'afdsgf', '02/26/2018', '', '', '71880493', '127.0.0.1', '2018-02-21 19:09:00', 1),
(3, 'sfdgf', '', 'afdsgf', '02/26/2018', '', '', '13435974', '127.0.0.1', '2018-02-21 19:10:37', 1),
(4, 'Rajeev', 'asjbdgbvjhb@gmail.com', '9072881132', '02/21/2018', 'Ernakulam', 'no special requirements', '97162780', '127.0.0.1', '2018-02-21 19:17:12', 1),
(5, 'Rajeev', 'asjbdgbvjhb@gmail.com', '9072881132', '02/21/2018', 'Ernakulam', 'no special requirements', '17163085', '127.0.0.1', '2018-02-21 19:17:56', 1),
(6, 'asffdg', '', '231435', '02/21/2018', '', '', '66508178', '127.0.0.1', '2018-02-21 19:22:32', 1),
(7, 'Rajeev', 'meetrajeev@gmail.com', '9072881132', '02/28/2018', 'Ernakulam', 'nothing', '93117065', '127.0.0.1', '2018-02-28 12:07:27', 1),
(8, 'rajeev 20042017', 'meetrajeev@gmail.com', '1346576879', '04/23/2018', 'Ernakulam', 'ert', '22502441', '127.0.0.1', '2018-04-20 15:39:29', 1),
(9, 'Rajeev', 's@gmail.com', '34656', '04/30/2018', 'aghdgj', 'hdgjs', '42860107', '127.0.0.1', '2018-04-23 13:00:42', 1),
(10, 'alan', 'tour@kochitaxitour.com', '9072881132', '04/11/2018', 'Ernakulam', 'nk', '68749389', '127.0.0.1', '2018-04-24 14:15:54', 1),
(11, 'name', 'meetrajeev@gmail.com', '9645166079', '06', 'ekm', 'test', 'Fleet 2', '127.0.0.1', '2018-09-06 17:00:16', 1),
(12, 'rajeev', 'meetrajeev@gmail.com', '8891635880', '04/04/2018', 'sadg', 'nothing more', 'Fleet 3', '127.0.0.1', '2018-09-06 17:43:20', 1),
(13, 'Rajeev', 'myemail@gmail.com', '8891635880', '', 'Trivandrum', 'test requirement', 'Fleet 7', '127.0.0.1', '2018-09-14 19:00:21', 1),
(14, 'Rajeev', 'dkjsdghs@gmail.com', 'skdjghjk', '01/09/2018 - 09/09/2018', 'Trivandrum', 'second enquiry', 'Fleet 6', '127.0.0.1', '2018-09-14 19:04:27', 1),
(15, 'Box name', 'abcd@gmail.com', '21345678756', '07/09/2018 - 09/09/2018', 'Trivandrum', 'this is first box enquiry', 'Fleet 4', '127.0.0.1', '2018-09-14 21:19:46', 1),
(16, 'Rajeev', 'meetrajeev@gmail.com', '8891635880', '13/09/2018 - 14/09/2018', 'Trivandrum', 'jquery dialogue testing', 'Fleet 5', '127.0.0.1', '2018-09-14 21:26:04', 1),
(17, 'Rajeev', 'meetrajeev@gmail.com', '8891635880', '15/09/2018 - 16/09/2018', 'Trivandrum', 'jquery dialogue box testing', 'Fleet 6', '127.0.0.1', '2018-09-14 21:26:57', 1),
(18, 'Rajeev', 'meetrajeev@gmail.com', '8891635880', '', 'Trivandrum', 'This is current enquiry', 'AUTOMATIC CARS', '127.0.0.1', '2018-09-18 18:49:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_homeimage`
--

CREATE TABLE `suarezrem_homeimage` (
  `id` bigint(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `image` varchar(300) NOT NULL,
  `description` longtext NOT NULL,
  `date_added` datetime NOT NULL,
  `ip` varchar(300) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `preference` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_homeimage`
--

INSERT INTO `suarezrem_homeimage` (`id`, `title`, `image`, `description`, `date_added`, `ip`, `status`, `preference`) VALUES
(1, 'Home Image 1', '1.jpg', 'Home Image Description 1', '2017-10-04 21:14:21', '137.97.9.184', 1, 49),
(2, 'Home Image 2', '2.jpg', 'Home Image Description 2', '2018-02-02 01:24:05', '137.97.151.235', 1, 50),
(3, 'Home Image 3', '3.jpg', 'Home Image Description 3', '2018-07-26 10:44:10', '137.97.85.161', 1, 53),
(4, 'Home Image 4', '4.jpg', 'Home Image Description 4', '2018-07-26 10:45:10', '137.97.85.161', 1, 54);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_limitless`
--

CREATE TABLE `suarezrem_limitless` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_limitless`
--

INSERT INTO `suarezrem_limitless` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'Our design team', '3.jpg', 'Our team is made up of some of the brightest minds in product design from across the world. We understand that each individual has their own strengths and we choose to collaborate with different designers based on their core expertise, thus allowing us to maintain our creative edge.                                ', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_makeitwork`
--

CREATE TABLE `suarezrem_makeitwork` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_makeitwork`
--

INSERT INTO `suarezrem_makeitwork` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'Make it work. Make it work better.', '3.jpg', 'If you go with the first solution that works, you deprive yourself of the joy that comes from going the extra mile. You are then left to wonder what the project could have been. Thatâ€™s why we spare no efforts in keeping on trying for the better.\r\n                                ', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_manifesto`
--

CREATE TABLE `suarezrem_manifesto` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_manifesto`
--

INSERT INTO `suarezrem_manifesto` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'Our Manifesto', '4.jpg', 'We are the futurists. We embrace tomorrow.We are for sustainability. We are for the best in ergonomics. We are for brave designs and inspired aesthetics. We seek peace - inside and out.\r\nWe spend our time studying nature, art, architecture and music.\r\nOurs is a life that not just touches the world,\r\nbut move moves it along - Inspiring. Instigating. Leading', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_office_location`
--

CREATE TABLE `suarezrem_office_location` (
  `id` bigint(11) NOT NULL,
  `latitude` varchar(300) NOT NULL,
  `longitude` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_office_location`
--

INSERT INTO `suarezrem_office_location` (`id`, `latitude`, `longitude`, `date_added`) VALUES
(1, '9.589770', '76.31909', '2015-07-14 23:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_phylosophy`
--

CREATE TABLE `suarezrem_phylosophy` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_phylosophy`
--

INSERT INTO `suarezrem_phylosophy` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'The Phylosophy', '3.jpg', 'We nurture ourselves by exploring the principles of ergonomics and lifestyle and seek to understand the principles of nature and encourage sustainability in our thoughts.\r\n', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_product`
--

CREATE TABLE `suarezrem_product` (
  `id` bigint(11) NOT NULL,
  `cid` bigint(11) DEFAULT '0',
  `image` varchar(300) NOT NULL,
  `title` varchar(300) NOT NULL,
  `tag_name` varchar(300) NOT NULL,
  `description` longtext NOT NULL,
  `ip` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_product`
--

INSERT INTO `suarezrem_product` (`id`, `cid`, `image`, `title`, `tag_name`, `description`, `ip`, `date_added`, `preference`, `status`) VALUES
(1, 1, '1.jpg', 'STY-LUX', 'The Hairdryer', '<p><span>Indulgent comfort now comes with inbuilt safety, with a hairdryer specially designed for the hospitality industry.</span></p>\r\n<p>&bull; Specially designed for high-end true hospitality use<br />&bull; Professional salon AC motor - top speed 20m/second with strong wind. Dries hair within minutes<br />&bull; Unique air nozzle design, airflow boosting and cooling. Will not heat up when win wind blows for a long time<br />&bull; 304 stainless steel wind mesh at the air outlet, to make wind and temperature more well distributed<br />&bull; Stylish T-body balance design handles, no tilt forward for backward, easier &amp; more comfortable for users<br />&bull; The air inlets adopt seven swept-back wind wheels, giving you a more stable, continuous and powerful wind supply. (Regular hair dryers are equipped with four wind wheels)<br />&bull; Thermostat comes with a dual over-heat protection, emphasizing the safety aspect of this luxurious hairdryer<br />&bull; The press-on and release-off micro switch makes it safer and more energy-effective<br />&bull; Available with a choice of 3 heat selections and 2 air-speeds with ionic function<br />&bull; The metal cover at the back prevents hair pinching<br />&bull; The 250cm Elastic cord, provides optimized cord storage<br />&bull; Glossy white hairdryer with a luxuriously stylish finish (Matt black +2$US)<br />&bull; 1800W, 220-240V, 50-60Hz, UK plug</p>', '::1', '2021-09-11 00:00:00', 1, 1),
(2, 2, '2.jpg', 'JARVIS', 'The Handy Steam Iron', '<p><span>Jarvis is an elegant, multifunctional Steam Iron emphasizing hi-end extra safety standards. It is easy to maintain, comfortable to handle and comes with world-class ergonomics integrated into its design.</span></p>\r\n<p>&bull; Specially designed for true hospitality use;<br />&bull; Stearn / Spray / Dry Ironing controls, more variable Ironing experience;<br />&bull; Powerful burst of steam to make your garments more Docile &amp; smooth;<br />&bull; Auto shut-off (30 sec. when unused and horizontal, 8 min. when vertical), User Safety emphasis;<br />&bull; Overheat protection, Indicator light for auto off &amp; in use status, extra safe;<br />&bull; Adjustable thermostat for different garment ironing options;<br />&bull; Large soleplate 22.5.12cm for more effective Ironing;<br />&bull; Anti-drip, Anti-calc, Self-cleaning &amp; Non-stick soleplate, easy maintenance;<br />&bull; Soft rubber handle, a more comfortable hold;<br />&bull; 200m1 water tank, sufficient and water-saving;<br />&bull; 3 meters extra long cable, 360 degree revolving cable guard;<br />&bull; Product Size: 29.12.14cm;<br />&bull; 1600W, 220-240V, 50-60Hz, EU plug (UK +$0.5);<br />&bull; CE, CB, GS, RoHS certified, 2 Years Warranty.</p>', '::1', '2021-09-11 12:51:00', 2, 1),
(3, 3, '3.jpg', 'IRONMAN', 'The Smart Ironing Center', '<p><span>Ironman is a smart and dexterous ironing center, a study in world class ergonomic design. It is perfect for both left handed and right handed guests and provides a smooth and comfortable ironing experience.</span></p>\r\n<p>&bull; Easy-assembling solution to save hassles for hotels<br />&bull; 360 degree rotatable and organizer friendly for left or right handed guests<br />&bull; Three sides protection from the organizer to well secure irons, with anti-theft system for cable<br />&bull; Easy storage with hook hanging in closet 136*38*24cm (folded including steam iron DOCILE), space friendly<br />&bull; High quality strengthened steel structure with unique semi-matt black finish<br />&bull; Double-leg + Square-leg structure with non-skid feet covers for sturdy standing with 7 levels of adjustable height (68-85cm), mechanical locking system<br />&bull; Easy replacement fire-retardant elastic cover with 8mm high density cotton padding for a more comfortable ironing<br />&bull; Extra large ironing space 110*28cm<br />&bull; Stylish Black Grey color</p>', '::1', '2021-09-11 16:18:33', 3, 1),
(4, 4, '4.jpg', 'TOASTMAN', 'The Tasteful Toaster', '<p><span>Easy to maintain and effortless to handle, the Toastman provides a tasteful balance in protection and hi-end design. Safety never looked this good.</span></p>\r\n<p>&bull; Stainless steel housing brushed finish with a steady wide plastic base<br />&bull; Defrost/Reheat/Cancel functions with variable browning control<br />&bull; Auto shut-off, indicator light, safety emphasis<br />&bull; Neat cord storage and anti-slip feet<br />&bull; Removable crumb tray<br />&bull; Product size: 170*270*190mm<br />&bull; 900W/220V/50Hz. Cable length: 73cm EU plug (UK+0.5)<br />&bull; GS, CE, CB, GS RoHS, LFGB certified, 2 Years Warranty</p>', '::1', '2021-09-11 16:19:26', 4, 1),
(5, 5, '5.jpg', 'TUNE', 'The Capsule Coffee Machine', '<p><span>It sets the trend for memorable coffee experience and tops it with superior technology and ergonomics. Coffee aficionados in the hospitality industry now have a new machine to swear by.</span></p>\r\n<p>&bull; Trendy design for excellent coffee experience<br />&bull; Automation ejection system for Nespresso compatible capsules<br />&bull; 20 bar Italian pump, professional extraction technology for excellent coffee<br />&bull; Easy-press buttons to choose preferred coffee volume<br />&bull; Preheating function: fast warming up in 20 seconds to ensure coffee taste<br />&bull; Adjustable dip tray fro small, big or extra big cup (American cup)<br />&bull; Automatic power off after 5 min, ERP function for energy saving<br />&bull; 700 ml removable water reservoir<br />&bull; Recycle container for 12pcs used capsules<br />&bull; Simple stylish white, optional colours: black, red, silver<br />&bull; Compact size 354:116*242mm<br />&bull; 1499W/AC220-240V/50Hz EU plug (UK+0.5)<br />&bull; CE, CB, GS RoHS, LFGB certified, 2 Years Warranty</p>', '::1', '2021-09-11 16:20:01', 5, 1),
(6, 6, '6.jpg', 'VEGA', 'The Stylish Kettle', '<p><span>Safety now finds a new balance with style. Add dash of dexterity to it and you have Vega, the 1.2 L Kettle, perfect for your hotel.</span></p>\r\n<p>&bull; Capacity 1.2L<br />&bull; Food grade 304 stainless steel housing with luxury brushed finish<br />&bull; Dual Safety Strix Controller: Auto shut-off, boil-dry protection and accurate temperature control<br />&bull; Concealed heating element, removable filter with transparent water gauge for easy maintenance<br />&bull; User friendly 360 degree cordless base for left and right handed guests with neat cord storage<br />&bull; 1500W/220-240V/50-60Hz, Cable length 73cm EU plug (UK+0.5)<br />&bull; CE, GS certified, 2 Years Warranty</p>', '::1', '2021-09-11 16:20:40', 6, 1),
(7, 7, '7.jpg', 'COMET', 'The Friendly Kettle', '<p><span>Big things come in small packages. Presenting Comet, the safe, functional, durable and eco-friendly kettle.</span></p>\r\n<p>&bull; Anti-scald triple walled structure with food grade 304 stainless steel inner wall +insulation layer between+ stainless steel wall<br />&bull; Options in Brushed finish/Black/Champagne/White<br />&bull; Dual safety Strix controller, Auto shutoff, Boil dry protection and accurate temperature control<br />&bull; Concealed heating element, invisible cord storage, easy to maintain, user friendly<br />&bull; One-button press open lid design<br />&bull; 360 degree cordless base for left and right handed guests<br />&bull; 1350-1650W/220-240V/50-60Hz, Cable length 73cm EU plug (UK+0.5)<br />&bull; CE, GS certified, 2 Years Warranty</p>', '::1', '2021-09-11 16:21:10', 7, 1),
(9, 8, '8.jpg', 'GALAXY', 'The Welcome Tray', '<p><span>Stylish and luxurious, this tray allows for easy maintenance and better safety.</span></p>\r\n<p>&bull; Large size W450*D320*H40 for coffee machine<br />&bull; Solid wood in ultra-strong matt black lacquer<br />&bull; Compact all in one design with compartments for kettle, coffee capsules, tea bags, sugar/creamer sticks, spoons etc<br />&bull; Round corner treatment for compartments<br />&bull; Wire guidance and lock (anti-theft) for kettle<br />&bull; 4 furniture protective gliders at bottom<br />&bull; Kettle and Mugs can be provided upon request</p>', '::1', '2021-09-13 13:53:03', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_product_images`
--

CREATE TABLE `suarezrem_product_images` (
  `id` bigint(11) NOT NULL,
  `cid` bigint(11) NOT NULL,
  `image` varchar(300) NOT NULL,
  `title` varchar(300) NOT NULL,
  `ip` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_product_images`
--

INSERT INTO `suarezrem_product_images` (`id`, `cid`, `image`, `title`, `ip`, `date_added`, `preference`, `status`) VALUES
(1, 1, '1.1.jpg', 'product 1', '::1', '2021-09-11 13:13:28', 1, 1),
(2, 1, '1.2.jpg', 'product 2', '::1', '2021-09-11 13:13:49', 2, 1),
(3, 1, '1.3.jpg', 'product 3', '::1', '2021-09-11 13:18:45', 3, 1),
(4, 1, '1.4.jpg', 'product 4', '::1', '2021-09-11 13:19:38', 4, 1),
(5, 2, '2.1.jpg', 'Product 1', '::1', '2021-09-11 14:03:13', 5, 1),
(6, 1, '1.5.jpg', 'product 5', '::1', '2021-09-14 00:13:26', 6, 1),
(7, 1, '1.6.jpg', 'product 6', '::1', '2021-09-14 00:13:42', 7, 1),
(8, 1, '1.7.jpg', 'product7', '::1', '2021-09-14 00:13:56', 8, 1),
(9, 1, '1.8.jpg', 'product 8', '::1', '2021-09-14 00:14:09', 9, 1),
(10, 1, '1.9.jpg', 'product 9', '::1', '2021-09-14 00:14:25', 10, 1),
(11, 1, '1.10.jpg', 'product 10', '::1', '2021-09-14 00:14:40', 11, 1),
(12, 1, '1.11.jpg', 'product 11', '::1', '2021-09-14 00:14:51', 12, 1),
(13, 1, '1.12.jpg', 'product 12', '::1', '2021-09-14 00:15:02', 13, 1),
(14, 1, '1.13.jpg', 'product 13', '::1', '2021-09-14 00:15:12', 14, 1),
(15, 1, '1,14.jpg', 'product14', '::1', '2021-09-14 00:15:25', 15, 1),
(16, 2, '2.2.jpg', 'Product 2', '::1', '2021-09-14 00:16:30', 16, 1),
(17, 2, '2.3.jpg', 'Product 3', '::1', '2021-09-14 00:16:40', 17, 1),
(18, 3, '3.1.jpg', 'Product 1', '::1', '2021-09-14 00:17:13', 18, 1),
(19, 3, '3.2.jpg', 'Product 2', '::1', '2021-09-14 00:17:29', 19, 1),
(20, 3, '3.3.jpg', 'Product 3', '::1', '2021-09-14 00:17:39', 20, 1),
(21, 3, '3.4.jpg', 'Product 4', '::1', '2021-09-14 00:17:50', 21, 1),
(22, 4, '4.1.jpg', 'Product 1', '::1', '2021-09-14 00:18:20', 22, 1),
(23, 4, '4.2.jpg', 'Product 2', '::1', '2021-09-14 00:18:30', 23, 1),
(24, 4, '4.3.jpg', 'Product 3', '::1', '2021-09-14 00:18:39', 24, 1),
(25, 4, '4.4.jpg', 'Product 4', '::1', '2021-09-14 00:18:47', 25, 1),
(26, 5, '5.1.jpg', 'Product 1', '::1', '2021-09-14 00:19:25', 26, 1),
(27, 5, '5.2.jpg', 'Product 2', '::1', '2021-09-14 00:19:33', 27, 1),
(28, 5, '5.3.jpg', 'Product 3', '::1', '2021-09-14 00:19:41', 28, 1),
(29, 5, '5.4.jpg', 'Product 4', '::1', '2021-09-14 00:19:49', 29, 1),
(30, 5, '5.5.jpg', 'Product 5', '::1', '2021-09-14 00:19:58', 30, 1),
(31, 6, '6.1.jpg', 'Product 1', '::1', '2021-09-14 00:20:29', 31, 1),
(32, 6, '6,2.jpg', 'Product 2', '::1', '2021-09-14 00:20:37', 32, 1),
(33, 6, '6.3jpg', 'Product 3', '::1', '2021-09-14 00:20:45', 33, 1),
(34, 6, '6.4.jpg', 'Product 4', '::1', '2021-09-14 00:20:53', 34, 1),
(35, 6, '6.5.jpg', 'Product 5', '::1', '2021-09-14 00:21:02', 35, 1),
(36, 7, '7.1.jpg', 'Product 1', '::1', '2021-09-14 00:21:18', 36, 1),
(37, 7, '7.2.jpg', 'Product 2', '::1', '2021-09-14 00:21:27', 37, 1),
(38, 7, '7.3.jpg', 'Product 3', '::1', '2021-09-14 00:22:21', 38, 1),
(39, 7, '7.4.jpg', 'product 4', '::1', '2021-09-14 00:22:35', 39, 1),
(40, 7, '7.5.jpg', 'product 5', '::1', '2021-09-14 00:22:44', 40, 1),
(41, 9, '8.1.jpg', 'Product 1', '::1', '2021-09-14 00:26:53', 41, 1),
(42, 9, '8.2.jpg', 'Product 2', '::1', '2021-09-14 00:27:05', 42, 1),
(43, 9, '8.3.jpg', 'Product 3', '::1', '2021-09-14 00:27:13', 43, 1),
(44, 9, '8.4.jpg', 'Product 4', '::1', '2021-09-14 00:27:25', 44, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_thebrand`
--

CREATE TABLE `suarezrem_thebrand` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_thebrand`
--

INSERT INTO `suarezrem_thebrand` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'The Brand', '5.jpg', 'In Room is a brand created with a compelling vision driven by advanced technology and inspired by uncompromising design.The collection is designedly artistic in every detail. Each of them provides a new level of design efficiency to hoteliers and an aesthetic and practical appeal to their guests.', '2010-11-15 21:36:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bw_adminlog`
--
ALTER TABLE `bw_adminlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_admin`
--
ALTER TABLE `suarezrem_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_adminlog`
--
ALTER TABLE `suarezrem_adminlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_category`
--
ALTER TABLE `suarezrem_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_contactus`
--
ALTER TABLE `suarezrem_contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_cont_address`
--
ALTER TABLE `suarezrem_cont_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_designprocess`
--
ALTER TABLE `suarezrem_designprocess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_designteam`
--
ALTER TABLE `suarezrem_designteam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_emotion`
--
ALTER TABLE `suarezrem_emotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_essence`
--
ALTER TABLE `suarezrem_essence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_fleets_images`
--
ALTER TABLE `suarezrem_fleets_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_fleet_enquiry`
--
ALTER TABLE `suarezrem_fleet_enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_homeimage`
--
ALTER TABLE `suarezrem_homeimage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_limitless`
--
ALTER TABLE `suarezrem_limitless`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_makeitwork`
--
ALTER TABLE `suarezrem_makeitwork`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_manifesto`
--
ALTER TABLE `suarezrem_manifesto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_office_location`
--
ALTER TABLE `suarezrem_office_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_phylosophy`
--
ALTER TABLE `suarezrem_phylosophy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_product`
--
ALTER TABLE `suarezrem_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_product_images`
--
ALTER TABLE `suarezrem_product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_thebrand`
--
ALTER TABLE `suarezrem_thebrand`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bw_adminlog`
--
ALTER TABLE `bw_adminlog`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suarezrem_admin`
--
ALTER TABLE `suarezrem_admin`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suarezrem_adminlog`
--
ALTER TABLE `suarezrem_adminlog`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `suarezrem_category`
--
ALTER TABLE `suarezrem_category`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `suarezrem_contactus`
--
ALTER TABLE `suarezrem_contactus`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suarezrem_cont_address`
--
ALTER TABLE `suarezrem_cont_address`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suarezrem_fleets_images`
--
ALTER TABLE `suarezrem_fleets_images`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT for table `suarezrem_fleet_enquiry`
--
ALTER TABLE `suarezrem_fleet_enquiry`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `suarezrem_homeimage`
--
ALTER TABLE `suarezrem_homeimage`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suarezrem_office_location`
--
ALTER TABLE `suarezrem_office_location`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suarezrem_product`
--
ALTER TABLE `suarezrem_product`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `suarezrem_product_images`
--
ALTER TABLE `suarezrem_product_images`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
