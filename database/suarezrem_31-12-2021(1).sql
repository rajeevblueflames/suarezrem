-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 31, 2021 at 08:18 AM
-- Server version: 5.7.31
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suarezrem`
--

-- --------------------------------------------------------

--
-- Table structure for table `bw_adminlog`
--

CREATE TABLE `bw_adminlog` (
  `id` bigint(11) NOT NULL,
  `actionid` bigint(11) NOT NULL COMMENT 'action performed by',
  `ip` varchar(225) NOT NULL COMMENT 'ip address',
  `actiontype` varchar(225) NOT NULL COMMENT 'insert,delete,update,select',
  `phpself` varchar(225) NOT NULL,
  `tablename` varchar(225) NOT NULL,
  `tableid` bigint(11) NOT NULL COMMENT 'primary key of table_name',
  `date` datetime NOT NULL,
  `notes` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for tracking the actions of admin';

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_admin`
--

CREATE TABLE `suarezrem_admin` (
  `id` bigint(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0= inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_admin`
--

INSERT INTO `suarezrem_admin` (`id`, `username`, `password`, `status`) VALUES
(1, 'admin', 'admins', 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_adminlog`
--

CREATE TABLE `suarezrem_adminlog` (
  `id` bigint(11) NOT NULL,
  `actionid` bigint(11) NOT NULL COMMENT 'action performed by',
  `ip` varchar(225) NOT NULL COMMENT 'ip address',
  `actiontype` varchar(225) NOT NULL COMMENT 'insert,delete,update,select',
  `phpself` varchar(225) NOT NULL,
  `tablename` varchar(225) NOT NULL,
  `tableid` bigint(11) NOT NULL COMMENT 'primary key of table_name',
  `date` datetime NOT NULL,
  `notes` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for tracking the actions of admin';

--
-- Dumping data for table `suarezrem_adminlog`
--

INSERT INTO `suarezrem_adminlog` (`id`, `actionid`, `ip`, `actiontype`, `phpself`, `tablename`, `tableid`, `date`, `notes`) VALUES
(1, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 0, '2021-09-11 12:33:40', 'image - <br>title - <br>description - <br>ip - <br>date_added - <br>preference - <br>'),
(2, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 0, '2021-09-11 12:41:29', 'image - <br>title - <br>tag_name - <br>description - <br>ip - <br>date_added - <br>preference - <br>'),
(3, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 2, '2021-09-11 12:51:00', 'image - 0<br>title - Product 2<br>tag_name - Product Tag 2<br>description - <p>test</p><br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>preference - 2<br>'),
(4, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 12:51:29', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(5, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 12:51:44', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(6, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 12:52:15', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(7, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 12:52:15', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(8, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 13:05:35', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(9, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 13:05:35', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(10, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 13:12:04', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(11, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 0, '2021-09-11 13:12:21', 'cid - <br>image - <br>title - <br>ip - <br>date_added - <br>preference - <br>'),
(12, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 1, '2021-09-11 13:13:28', 'pid - 1<br>image - 0<br>title - test 1<br>ip - ::1<br>date_added - 2021-09-11 13:13:28<br>preference - 1<br>'),
(13, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 2, '2021-09-11 13:13:49', 'pid - 1<br>image - 0<br>title - test 2<br>ip - ::1<br>date_added - 2021-09-11 13:13:49<br>preference - 2<br>'),
(14, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 3, '2021-09-11 13:18:45', 'pid - 1<br>image - 0<br>title - test 3<br>ip - ::1<br>date_added - 2021-09-11 13:18:45<br>preference - 3<br>'),
(15, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 4, '2021-09-11 13:19:38', 'cid - 1<br>image - 0<br>title - test 4<br>ip - ::1<br>date_added - 2021-09-11 13:19:38<br>preference - 4<br>'),
(16, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 5, '2021-09-11 14:03:13', 'cid - 2<br>image - 0<br>title - test 5<br>ip - ::1<br>date_added - 2021-09-11 14:03:13<br>preference - 5<br>'),
(17, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 2, '2021-09-11 14:08:34', 'Before<br>image - 0<br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>After<br>image - 0<br>title - Product test 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>'),
(18, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 3, '2021-09-11 16:18:33', 'image - <br>title - Product Test 3<br>tag_name - Product Tag 3<br>description - <p>Testing 3</p><br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>preference - 3<br>'),
(19, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 1, '2021-09-11 16:18:52', 'Before<br>image - 1.jpg<br>title - test add<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>After<br>image - 1.jpg<br>title - Product test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>'),
(20, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 4, '2021-09-11 16:19:26', 'image - <br>title - Product Test 4<br>tag_name - Product Tag 4<br>description - <p>Testing 4</p><br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>preference - 4<br>'),
(21, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 5, '2021-09-11 16:20:01', 'image - <br>title - Product Test 5<br>tag_name - Product Tag 5<br>description - <p>Testing 5</p><br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>preference - 5<br>'),
(22, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 6, '2021-09-11 16:20:40', 'image - <br>title - Product Test 6<br>tag_name - Product Tag 6<br>description - <p>Testing 6</p><br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>preference - 6<br>'),
(23, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 7, '2021-09-11 16:21:10', 'image - <br>title - Product Test 7<br>tag_name - Product Tag 7<br>description - <p>Testing 7</p><br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>preference - 7<br>'),
(24, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 1, '2021-09-11 17:30:52', 'Before<br>image - 1.jpg<br>title - Product test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>After<br>image - 1.jpg<br>title - Product Test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>'),
(25, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 7, '2021-09-11 17:34:28', 'Before<br>cid - 0<br>image - 7.jpg<br>title - Product Test 7<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>After<br>cid - 1<br>image - 7.jpg<br>title - Product Test 7<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>'),
(26, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 6, '2021-09-11 17:35:02', 'Before<br>cid - 0<br>image - 6.jpg<br>title - Product Test 6<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>cid - 2<br>image - 6.jpg<br>title - Product Test 6<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(27, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 5, '2021-09-11 17:35:09', 'Before<br>cid - 0<br>image - 5.jpg<br>title - Product Test 5<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>After<br>cid - 3<br>image - 5.jpg<br>title - Product Test 5<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>'),
(28, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 4, '2021-09-11 17:35:18', 'Before<br>cid - 0<br>image - 4.jpg<br>title - Product Test 4<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>After<br>cid - 4<br>image - 4.jpg<br>title - Product Test 4<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>'),
(29, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 3, '2021-09-11 17:35:27', 'Before<br>cid - 0<br>image - 3.jpg<br>title - Product Test 3<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>After<br>cid - 4<br>image - 3.jpg<br>title - Product Test 3<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>'),
(30, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 2, '2021-09-11 17:35:36', 'Before<br>cid - 0<br>image - 2.jpg<br>title - Product test 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>After<br>cid - 5<br>image - 2.jpg<br>title - Product Test 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>'),
(31, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 1, '2021-09-11 17:35:44', 'Before<br>cid - 0<br>image - 1.jpg<br>title - Product Test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>After<br>cid - 6<br>image - 1.jpg<br>title - Product Test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>'),
(32, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 8, '2021-09-11 18:21:44', 'cid - 1<br>image - <br>title - Test Product 8<br>tag_name - Product Tag 8<br>description - <p>testing 8</p><br>ip - ::1<br>date_added - 2021-09-11 18:21:44<br>preference - 8<br>'),
(33, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 6, '2021-09-13 13:00:22', 'Before<br>cid - 2<br>image - 6.jpg<br>title - Product Test 6<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>cid - 1<br>image - 6.jpg<br>title - Product Test 6.1<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(34, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 1, '2021-09-13 13:44:32', 'Before<br>cid - 6<br>image - 1.jpg<br>title - Product Test 1<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>After<br>cid - 1<br>image - 1.jpg<br>title - STY-LUX<br>ip - ::1<br>date_added - 2021-09-11 00:00:00<br>'),
(35, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 2, '2021-09-13 13:46:06', 'Before<br>cid - 5<br>image - 2.jpg<br>title - Product Test 2<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>After<br>cid - 2<br>image - 2.jpg<br>title - JARVIS<br>ip - ::1<br>date_added - 2021-09-11 12:51:00<br>'),
(36, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 3, '2021-09-13 13:47:13', 'Before<br>cid - 4<br>image - 3.jpg<br>title - Product Test 3<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>After<br>cid - 3<br>image - 3.jpg<br>title - IRONMAN<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>'),
(37, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 4, '2021-09-13 13:48:13', 'Before<br>cid - 4<br>image - 4.jpg<br>title - Product Test 4<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>After<br>cid - 4<br>image - 4.jpg<br>title - TOASTMAN<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>'),
(38, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 5, '2021-09-13 13:49:02', 'Before<br>cid - 3<br>image - 5.jpg<br>title - Product Test 5<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>After<br>cid - 5<br>image - 5.jpg<br>title - TUNE<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>'),
(39, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 6, '2021-09-13 13:49:57', 'Before<br>cid - 1<br>image - 6.jpg<br>title - Product Test 6.1<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>cid - 6<br>image - 6.jpg<br>title - VEGA<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(40, 1, '::1', 'UPDATE', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 7, '2021-09-13 13:50:38', 'Before<br>cid - 1<br>image - 7.jpg<br>title - Product Test 7<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>After<br>cid - 7<br>image - 7.jpg<br>title - COMET<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>'),
(41, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 0, '2021-09-13 13:51:39', 'cid - <br>image - <br>title - <br>tag_name - <br>description - <br>ip - <br>date_added - <br>preference - <br>'),
(42, 1, '::1', 'INSERT', '/suarezrem/admin/product_manage.php', 'suarezrem_product', 9, '2021-09-13 13:53:03', 'cid - 8<br>image - <br>title - GALAXY<br>tag_name - The Welcome Tray<br>description - <p><span>Stylish and luxurious, this tray allows for easy maintenance and better safety.</span></p>\r\n<p>&bull; Large size W450*D320*H40 for coffee machine<br />&bull; Solid wood in ultra-strong matt black lacquer<br />&bull; Compact all in one design with compartments for kettle, coffee capsules, tea bags, sugar/creamer sticks, spoons etc<br />&bull; Round corner treatment for compartments<br />&bull; Wire guidance and lock (anti-theft) for kettle<br />&bull; 4 furniture protective gliders at bottom<br />&bull; Kettle and Mugs can be provided upon request</p><br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>preference - 8<br>'),
(43, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 6, '2021-09-14 00:13:26', 'cid - 1<br>image - <br>title - product 5<br>ip - ::1<br>date_added - 2021-09-14 00:13:26<br>preference - 6<br>'),
(44, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 7, '2021-09-14 00:13:42', 'cid - 1<br>image - <br>title - product 6<br>ip - ::1<br>date_added - 2021-09-14 00:13:42<br>preference - 7<br>'),
(45, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 8, '2021-09-14 00:13:56', 'cid - 1<br>image - <br>title - product7<br>ip - ::1<br>date_added - 2021-09-14 00:13:56<br>preference - 8<br>'),
(46, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 9, '2021-09-14 00:14:09', 'cid - 1<br>image - <br>title - product 8<br>ip - ::1<br>date_added - 2021-09-14 00:14:09<br>preference - 9<br>'),
(47, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 10, '2021-09-14 00:14:25', 'cid - 1<br>image - <br>title - product 9<br>ip - ::1<br>date_added - 2021-09-14 00:14:25<br>preference - 10<br>'),
(48, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 11, '2021-09-14 00:14:40', 'cid - 1<br>image - <br>title - product 10<br>ip - ::1<br>date_added - 2021-09-14 00:14:40<br>preference - 11<br>'),
(49, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 12, '2021-09-14 00:14:51', 'cid - 1<br>image - <br>title - product 11<br>ip - ::1<br>date_added - 2021-09-14 00:14:51<br>preference - 12<br>'),
(50, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 13, '2021-09-14 00:15:02', 'cid - 1<br>image - <br>title - product 12<br>ip - ::1<br>date_added - 2021-09-14 00:15:02<br>preference - 13<br>'),
(51, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 14, '2021-09-14 00:15:12', 'cid - 1<br>image - <br>title - product 13<br>ip - ::1<br>date_added - 2021-09-14 00:15:12<br>preference - 14<br>'),
(52, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 15, '2021-09-14 00:15:25', 'cid - 1<br>image - <br>title - product14<br>ip - ::1<br>date_added - 2021-09-14 00:15:25<br>preference - 15<br>'),
(53, 1, '::1', 'UPDATE', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 5, '2021-09-14 00:16:19', 'Before<br>image - 0<br>title - test 5<br>ip - ::1<br>date_added - 2021-09-11 14:03:13<br>After<br>image - 0<br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-11 14:03:13<br>'),
(54, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 16, '2021-09-14 00:16:30', 'cid - 2<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:16:30<br>preference - 16<br>'),
(55, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 17, '2021-09-14 00:16:40', 'cid - 2<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:16:40<br>preference - 17<br>'),
(56, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 18, '2021-09-14 00:17:13', 'cid - 3<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:17:13<br>preference - 18<br>'),
(57, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 19, '2021-09-14 00:17:29', 'cid - 3<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:17:29<br>preference - 19<br>'),
(58, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 20, '2021-09-14 00:17:39', 'cid - 3<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:17:39<br>preference - 20<br>'),
(59, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 21, '2021-09-14 00:17:50', 'cid - 3<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:17:50<br>preference - 21<br>'),
(60, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 22, '2021-09-14 00:18:20', 'cid - 4<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:18:20<br>preference - 22<br>'),
(61, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 23, '2021-09-14 00:18:30', 'cid - 4<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:18:30<br>preference - 23<br>'),
(62, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 24, '2021-09-14 00:18:39', 'cid - 4<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:18:39<br>preference - 24<br>'),
(63, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 25, '2021-09-14 00:18:47', 'cid - 4<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:18:47<br>preference - 25<br>'),
(64, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 26, '2021-09-14 00:19:25', 'cid - 5<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:19:25<br>preference - 26<br>'),
(65, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 27, '2021-09-14 00:19:33', 'cid - 5<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:19:33<br>preference - 27<br>'),
(66, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 28, '2021-09-14 00:19:41', 'cid - 5<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:19:41<br>preference - 28<br>'),
(67, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 29, '2021-09-14 00:19:49', 'cid - 5<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:19:49<br>preference - 29<br>'),
(68, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 30, '2021-09-14 00:19:58', 'cid - 5<br>image - <br>title - Product 5<br>ip - ::1<br>date_added - 2021-09-14 00:19:58<br>preference - 30<br>'),
(69, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 31, '2021-09-14 00:20:29', 'cid - 6<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:20:29<br>preference - 31<br>'),
(70, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 32, '2021-09-14 00:20:37', 'cid - 6<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:20:37<br>preference - 32<br>'),
(71, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 33, '2021-09-14 00:20:45', 'cid - 6<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:20:45<br>preference - 33<br>'),
(72, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 34, '2021-09-14 00:20:53', 'cid - 6<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:20:53<br>preference - 34<br>'),
(73, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 35, '2021-09-14 00:21:02', 'cid - 6<br>image - <br>title - Product 5<br>ip - ::1<br>date_added - 2021-09-14 00:21:02<br>preference - 35<br>'),
(74, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 36, '2021-09-14 00:21:18', 'cid - 7<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:21:18<br>preference - 36<br>'),
(75, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 37, '2021-09-14 00:21:27', 'cid - 7<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:21:27<br>preference - 37<br>'),
(76, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 38, '2021-09-14 00:22:21', 'cid - 7<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:22:21<br>preference - 38<br>'),
(77, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 39, '2021-09-14 00:22:35', 'cid - 7<br>image - <br>title - product 4<br>ip - ::1<br>date_added - 2021-09-14 00:22:35<br>preference - 39<br>'),
(78, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 40, '2021-09-14 00:22:44', 'cid - 7<br>image - <br>title - product 5<br>ip - ::1<br>date_added - 2021-09-14 00:22:44<br>preference - 40<br>'),
(79, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 41, '2021-09-14 00:26:53', 'cid - 9<br>image - <br>title - Product 1<br>ip - ::1<br>date_added - 2021-09-14 00:26:53<br>preference - 41<br>'),
(80, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 42, '2021-09-14 00:27:05', 'cid - 9<br>image - <br>title - Product 2<br>ip - ::1<br>date_added - 2021-09-14 00:27:05<br>preference - 42<br>'),
(81, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 43, '2021-09-14 00:27:13', 'cid - 9<br>image - <br>title - Product 3<br>ip - ::1<br>date_added - 2021-09-14 00:27:13<br>preference - 43<br>'),
(82, 1, '::1', 'INSERT', '/suarezrem/admin/productimage_manage.php', 'suarezrem_product_images', 44, '2021-09-14 00:27:25', 'cid - 9<br>image - <br>title - Product 4<br>ip - ::1<br>date_added - 2021-09-14 00:27:25<br>preference - 44<br>'),
(83, 1, '::1', 'UPDATE', '/suarezremworld/admin/office_location.php', 'suarezrem_office_location', 1, '2021-09-14 16:01:23', 'Before<br>latitude - 9.589770<br>longitude - 76.532420<br>After<br>latitude - 9.96867<br>longitude - 76.31909<br>'),
(84, 1, '::1', 'UPDATE', '/suarezremworld/admin/office_location.php', 'suarezrem_office_location', 1, '2021-09-14 16:05:18', 'Before<br>latitude - 9.96867<br>longitude - 76.31909<br>After<br>latitude - 25.18086<br>longitude - 55.26292<br>'),
(85, 1, '::1', 'UPDATE', '/suarezremworld/admin/office_location.php', 'suarezrem_office_location', 1, '2021-09-14 16:07:27', 'Before<br>latitude - 25.18086<br>longitude - 55.26292<br>After<br>latitude - 9.589770<br>longitude - 76.31909<br>'),
(86, 1, '::1', 'INSERT', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 10, '2021-12-29 12:16:14', 'image - 0<br>title - retert<br>tag_name - rwrt<br>description - <p>rywryry</p><br>ip - ::1<br>date_added - 2021-12-29 12:16:14<br>preference - 9<br>'),
(87, 1, '::1', 'UPDATE', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 9, '2021-12-29 12:31:50', 'Before<br>image - 8.jpg<br>title - GALAXY<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>After<br>image - 8.jpg<br>title - GALAXY<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>'),
(88, 1, '::1', 'UPDATE', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 9, '2021-12-29 12:33:37', 'Before<br>image - 8.jpg<br>title - GALAXY<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>After<br>image - 8.jpg<br>title - WEBSITE DESIGN<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>'),
(89, 1, '::1', 'UPDATE', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 7, '2021-12-29 12:33:58', 'Before<br>image - 7.jpg<br>title - COMET<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>After<br>image - 7.jpg<br>title - SOCIAL MEDIA CONTENT AND MARKETING<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>'),
(90, 1, '::1', 'UPDATE', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 6, '2021-12-29 12:35:10', 'Before<br>image - 6.jpg<br>title - VEGA<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>image - 6.jpg<br>title - DIGITAL MARKETING<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(91, 1, '::1', 'UPDATE', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 5, '2021-12-29 12:35:46', 'Before<br>image - 5.jpg<br>title - TUNE<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>After<br>image - 5.jpg<br>title - WEB HOSTING<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>'),
(92, 1, '::1', 'UPDATE', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 5, '2021-12-29 14:02:15', 'Before<br>image - 5.jpg<br>title - WEB HOSTING<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>After<br>image - 5.jpg<br>title - WEB HOSTING<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>'),
(93, 1, '::1', 'UPDATE', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 9, '2021-12-29 14:26:30', 'Before<br>image - 8.jpg<br>title - WEBSITE DESIGN<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>After<br>image - 8.jpg<br>title - THE HISTORY<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>'),
(94, 1, '::1', 'UPDATE', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 7, '2021-12-29 14:27:03', 'Before<br>image - 7.jpg<br>title - SOCIAL MEDIA CONTENT AND MARKETING<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>After<br>image - 7.jpg<br>title - THE CENTRE OF THE WORLD<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>'),
(95, 1, '::1', 'UPDATE', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 6, '2021-12-29 14:28:15', 'Before<br>image - 6.jpg<br>title - DIGITAL MARKETING<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>image - 6.jpg<br>title - HOME TO THE BEST<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(96, 1, '::1', 'INSERT', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 10, '2021-12-29 14:34:12', 'image - <br>title - rtert<br>description - <p>rtyrty</p><br>ip - ::1<br>date_added - 2021-12-29 14:34:12<br>preference - 9<br>'),
(97, 1, '::1', 'UPDATE', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 9, '2021-12-29 15:09:38', 'Before<br>image - 8.jpg<br>title - GALAXY<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>After<br>image - 8.jpg<br>title - MUKESH DESAI, CPA<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>'),
(98, 1, '::1', 'UPDATE', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 7, '2021-12-29 15:10:04', 'Before<br>image - 7.jpg<br>title - COMET<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>After<br>image - 7.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-09-11 16:21:10<br>'),
(99, 1, '::1', 'UPDATE', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 6, '2021-12-29 15:10:31', 'Before<br>image - 6.jpg<br>title - VEGA<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>After<br>image - 6.jpg<br>title - LAUREN J. SUAREZ<br>ip - ::1<br>date_added - 2021-09-11 16:20:40<br>'),
(100, 1, '::1', 'UPDATE', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 5, '2021-12-29 15:10:56', 'Before<br>image - 5.jpg<br>title - TUNE<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>After<br>image - 5.jpg<br>title - ALEXANDER TAN<br>ip - ::1<br>date_added - 2021-09-11 16:20:01<br>'),
(101, 1, '::1', 'UPDATE', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 4, '2021-12-29 15:11:28', 'Before<br>image - 4.jpg<br>title - TOASTMAN<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>After<br>image - 4.jpg<br>title - FRED MORRIS, LUTCF<br>ip - ::1<br>date_added - 2021-09-11 16:19:26<br>'),
(102, 1, '::1', 'UPDATE', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 3, '2021-12-29 15:12:09', 'Before<br>image - 3.jpg<br>title - IRONMAN<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>After<br>image - 3.jpg<br>title - WALY DOLATY, ESQ<br>ip - ::1<br>date_added - 2021-09-11 16:18:33<br>'),
(103, 1, '::1', 'INSERT', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 1, '2021-12-29 15:22:23', 'image - <br>title - MUKESH DESAI, CPA<br>tag_name - Non-executive Board Member<br>description - <p><span>Mukesh is a non-executive board member of Suarez REM. As our Chief Financial Analyst, he guides the company by organizing information, consolidating reports, and reviewing all non-legal pertinent information about prospective deals. He examines the feasibility of every deal and is a hands-on member of the team, helping us with cost reduction, accounting, reconciliations, advising clients, assisting auditors, networking with industry professionals, reviewing budgets, checking the integrity of data, forecasting, helping the business in its investments and lease analysis and rental expense projections.</span></p><br>ip - ::1<br>date_added - 2021-12-29 15:22:23<br>preference - 1<br>'),
(104, 1, '::1', 'INSERT', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 2, '2021-12-29 15:22:45', 'image - <br>title - JIMMY RODRIQUEZ<br>tag_name - Chief Operating Officer<br>description - <p><span>As COO of Suarez REM, Jimmy oversees the day-to-day operations of the office. He designs and implements business operations, establish policies that promote company culture and vision, and oversee operations of the company and the work of executives. He is responsible for preparing budgets for assignments undertaken by the company for clients.</span></p><br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>preference - 2<br>'),
(105, 1, '::1', 'INSERT', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 3, '2021-12-29 15:23:08', 'image - <br>title - LAUREN J. SUAREZ<br>tag_name - Director, Administration<br>description - <div class="boxs-relative clearfix">\r\n<p class="textJustification">Lauren works as Administrative Manager to the team and is responsible for tenant searches and market data provided to clients of lease availabilities. She is also responsible for preparing and executing operating budgets and also oversees coordination of Suarez REM&rsquo;s administration and general workflow.</p>\r\n</div><br>ip - ::1<br>date_added - 2021-12-29 15:23:08<br>preference - 3<br>'),
(106, 1, '::1', 'INSERT', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 4, '2021-12-29 15:23:29', 'image - <br>title - ALEXANDER TAN<br>tag_name - In-house Developer<br>description - <div class="container">\r\n<div class="col-md-12">\r\n<div class="boxs-relative clearfix">\r\n<p class="textJustification">Alexander is the in-house developer specializing in sale-lease back transactions. Using the capital of Tan International and Suarez REM, he facilitates sale-leaseback transactions for clients who deem a sale-leaseback as a favorable strategy for its long term occupancy.</p>\r\n</div>\r\n</div>\r\n</div><br>ip - ::1<br>date_added - 2021-12-29 15:23:29<br>preference - 4<br>'),
(107, 1, '::1', 'INSERT', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 5, '2021-12-29 15:23:55', 'image - <br>title - FRED MORRIS, LUTCF<br>tag_name - Managing Director<br>description - <div class="container">\r\n<div class="col-md-12">\r\n<div class="boxs-relative clearfix">\r\n<p class="textJustification">As managing director, Fred leads the department that works with Suarez&rsquo; clients to arrange office insurance coverage which lists the landlord as an additionally insured party and is a customary requirement for office tenants in commercial leases. Fred is committed to sourcing and obtaining the lowest premiums while selecting the highest rated carriers possible. Fred undertakes a rigorous analysis of the insurance clause and its listed perils and offers solutions to common as well as complex underwriting issues surrounding the commercial lease clauses contained in the leases for major buildings.</p>\r\n</div>\r\n</div>\r\n</div><br>ip - ::1<br>date_added - 2021-12-29 15:23:55<br>preference - 5<br>'),
(108, 1, '::1', 'INSERT', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 6, '2021-12-29 15:24:21', 'image - <br>title - WALY DOLATY, ESQ<br>tag_name - Managing Director<br>description - <div class="container">\r\n<div class="col-md-12">\r\n<div class="boxs-relative clearfix">\r\n<p class="textJustification">Waly is an international corporate and investment lawyer with in depth knowledge and experience in banking, tax planning, and Islamic finance. He has held various senior positions in legal and compliance departments. He has held the position of chief legal counsel for multinational investment corporations, banks, and financial institutions including at the law firm of Baker &amp; McKenzie, and Burgan Bank, Kuwait. He has held the position of assistant lecturer at Cairo University where he taught corporate law and headed the legal department at the University. He is the company&rsquo;s in house legal advisor on matters related to the company and its activities.</p>\r\n</div>\r\n</div>\r\n</div><br>ip - ::1<br>date_added - 2021-12-29 15:24:21<br>preference - 6<br>'),
(109, 1, '::1', 'UPDATE', '/suarezrem/admin/team_manage.php', 'suarezrem_team', 6, '2021-12-29 15:26:25', 'Before<br>image - 1.png<br>title - WALY DOLATY, ESQ<br>ip - ::1<br>date_added - 2021-12-29 15:24:21<br>After<br>image - 1.png<br>title - WALY DOLATY, ESQ<br>ip - ::1<br>date_added - 2021-12-29 15:24:21<br>'),
(110, 1, '::1', 'UPDATE', '/suarezrem/admin/office_location.php', 'suarezrem_office_location', 1, '2021-12-29 15:37:30', 'Before<br>latitude - 9.589770<br>longitude - 76.31909<br>After<br>latitude - 40.75168<br>longitude - -73.97955<br>'),
(111, 1, '::1', 'UPDATE', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 9, '2021-12-29 16:17:13', 'Before<br>image - 8.jpg<br>title - THE HISTORY<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>After<br>image - 8.jpg<br>title - THE HISTORY<br>ip - ::1<br>date_added - 2021-09-13 13:53:03<br>'),
(112, 1, '::1', 'INSERT', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 1, '2021-12-29 16:25:39', 'image - <br>title - THE HISTORY<br>description - <p><span>In the precolonial era, the area of present-day New York City was home to Algonquian Native Americans, including the Lenape. Their homeland, known as Lenapehoking, included Staten Island, Manhattan, the Bronx, the western portion of Long Island, including the areas that would later become the boroughs of Brooklyn and Queens, and the Lower Hudson Valley.</span><br /><br /><span>Colonial era New York City traces its origins to a trading post founded on the southern tip of Manhattan Island by Dutch colonists in around 1624. The settlement was named New Amsterdam in 1626 and was chartered as a city in 1653. The city came under English control in 1664 and was renamed New York after King Charles II of England granted the lands to his brother, the Duke of York.</span><br /><br /><span>The city was regained by the Dutch in July 1673 and was renamed New Orange for one year and three months; the city has been continuously named New York since November 1674. New York City was the capital of the United States from 1785 until 1790, and has been the largest U.S. city since 1790.</span><br /><br /><span>The Statue of Liberty greeted millions of immigrants as they came to the United States by ship in the late 19th and early 20th centuries, and is a symbol of the ideals of liberty and peace.</span><br /><br /><span>By the 21st century, New York has emerged as a global node of creativity, entrepreneurship, and environmental sustainability, and as a symbol of freedom and cultural diversity. In 2019, New York was voted the greatest city in the world per a survey of over 30,000 people from 48 cities worldwide, citing in large part, its cultural diversity.</span></p><br>ip - ::1<br>date_added - 2021-12-29 16:25:39<br>preference - 1<br>'),
(113, 1, '::1', 'INSERT', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 2, '2021-12-29 16:28:15', 'image - <br>title - THE CENTRE OF THE WORLD<br>description - <p><span>New York is one of the world\'s most populous megacities and is described as the cultural, financial, and media capital of the world, significantly influencing commerce, entertainment, research, technology, education, politics, tourism, dining, art, fashion, and sports, and is the most photographed city in the world. Many districts and monuments in New York City are major landmarks, including some of the world\'s ten most visited tourist attractions.</span><br /><br /><span>A record 66.6 million tourists visited New York City in 2019, with Times Square being one of the world\'s busiest pedestrian intersections, and a center of the world\'s entertainment industry. (Luis Suarez, our Founder and CEO was an integral member of the team that worked on the Times Square Re-Development Project with Park Realty and George Klein.</span><br /><br /><span>Many of the city\'s landmarks, skyscrapers, and parks are known around the world. The Empire State Building has become the global standard of reference to describe the height and length of other structures. Manhattan\'s real estate market is amongst the most expensive and exclusive in the world.</span><br /><br /><span>The New York City Subway is the largest single-operator rapid transit system worldwide, with 472 rail stations. The city has over 120 colleges and universities, including the prestigious Columbia University, New York University, Rockefeller University, and the City University of New York system, which is the largest urban public university system in the United States.</span><br /><br /><span>Anchored by Wall Street in the Financial District of Lower Manhattan, New York City has been called both the world\'s leading financial center and the most financially powerful city in the world. It is home to the world\'s two largest stock exchanges by total market capitalization, the New York Stock Exchange and NASDAQ.</span></p><br>ip - ::1<br>date_added - 2021-12-29 16:28:15<br>preference - 2<br>'),
(114, 1, '::1', 'INSERT', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 3, '2021-12-29 16:31:32', 'image - <br>title - HOME TO THE BEST<br>description - <p><span>New York city is home to the world&rsquo;s best restaurants, incredible museums with significant collections of all types. It has the best art galleries, best restaurants, best hospitals and best public transportation system in the world.</span><br /><br /><span>New York is home to great and historic teams such as the NY Jets, New York Giants, New York Mets, New York Yankees, New York Knickerbockers, the New York Rangers and the New York Islanders. Some of the best jazz clubs in the world find their home here. New York is where the top national and global talent in virtually every sector come to find opportunity and employment.</span></p><br>ip - ::1<br>date_added - 2021-12-29 16:31:32<br>preference - 3<br>'),
(115, 1, '::1', 'UPDATE', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 3, '2021-12-29 16:44:01', 'Before<br>image - 3.jpg<br>title - HOME TO THE BEST<br>ip - ::1<br>date_added - 2021-12-29 16:31:32<br>After<br>image - 3.jpg<br>title - HOME TO THE BEST<br>ip - ::1<br>date_added - 2021-12-29 16:31:32<br>'),
(116, 1, '::1', 'UPDATE', '/suarezrem/admin/newyork_manage.php', 'suarezrem_newyork', 3, '2021-12-29 16:50:37', 'Before<br>image - 3.jpg<br>title - HOME TO THE BEST<br>ip - ::1<br>date_added - 2021-12-29 16:31:32<br>After<br>image - 3.jpg<br>title - HOME TO THE BEST<br>ip - ::1<br>date_added - 2021-12-29 16:31:32<br>'),
(117, 1, '::1', 'INSERT', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 1, '2021-12-30 12:46:30', 'image - <br>title - BRANDING STRATEGY<br>description - <p><span>A branding strategy is a long-term plan for the development of a successful brand to achieve specific goals, which directly links to consumer needs and demands, perception and competitive environments. It also has the power to amplify a brand amongst its competitors and stand out on the market.</span><br /><span>At Suarez SEM, we can set up your brand or organization in a fiercely competitive market, with years of branding development experience powered by AI based market intelligence platforms.</span></p><br>ip - ::1<br>date_added - 2021-12-30 12:46:30<br>preference - 1<br>'),
(118, 1, '::1', 'INSERT', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 2, '2021-12-30 12:46:58', 'image - <br>title - CORPORATE ID DEVELOPMENT<br>description - <p><span>A strategic corporate identity boosts the company&rsquo;s worth and helps it expand. It builds trust and loyalty since customers tend to trust established companies. It brings in more sales as more and more users come to know your business. Suarez REM helps develop your corporate identity by capitalizing on the concept of brand development.</span><br /><span>Corporate ID design services by Suarez REM includes Concept Development, Brand Identity Design, Packaging Design, Brand Guidelines and Collateral Design.</span></p><br>ip - ::1<br>date_added - 2021-12-30 12:46:58<br>preference - 2<br>'),
(119, 1, '::1', 'INSERT', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 3, '2021-12-30 12:47:15', 'image - <br>title - WEBSITE DESIGN<br>description - <p><span>The website is the most important digital real estate asset for any brand, entity or organization. A well-designed website based on AI intelligence can reach the right audience and help create a good impression about your brand to your customers. It can nurture your leads, and get more conversions. Our website design team has a wide range of experience, developing engaging websites for clients across the world.</span></p><br>ip - ::1<br>date_added - 2021-12-30 12:47:15<br>preference - 3<br>'),
(120, 1, '::1', 'INSERT', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 4, '2021-12-30 12:47:32', 'image - <br>title - SOCIAL MEDIA CONTENT AND MARKETING<br>description - <p><span>Social media networks are open to all, giving every business a chance to follow their consumers\' activities or potential buyers. This helps brand owners to be more informed about their target audience, their likes, dislikes, and their interests so that they can create an engaging environment that appeals to their ideal customers. Our Social Media Team can help you connect with your Target Audience by creating appealing content including videos, posts, blogs, vlogs and stories to ensure that they always have you on top of their minds, when they make their buying decisions.</span></p><br>ip - ::1<br>date_added - 2021-12-30 12:47:32<br>preference - 4<br>'),
(121, 1, '::1', 'INSERT', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 5, '2021-12-30 12:48:00', 'image - <br>title - DIGITAL MARKETING<br>description - <p><span>Digital marketing, also called online marketing, is the promotion of brands to connect with potential customers using the internet and other forms of digital communication.</span><br /><br /><span>&bull;&nbsp;</span><span>Social Media Contenting</span><br /><span>&bull;&nbsp;</span><span>Google AdWords</span><br /><span>&bull;&nbsp;</span><span>SEO</span></p><br>ip - ::1<br>date_added - 2021-12-30 12:48:00<br>preference - 5<br>'),
(122, 1, '::1', 'INSERT', '/suarezrem/admin/services_manage.php', 'suarezrem_services', 6, '2021-12-30 12:48:17', 'image - <br>title - WEB HOSTING<br>description - <p><span>Web hosting is an online service that allows you to publish your website files onto the internet. So, anyone who has access to the internet has access to your website.</span><br /><br /><span>&bull;&nbsp;</span><span>Email Marketing</span></p><br>ip - ::1<br>date_added - 2021-12-30 12:48:17<br>preference - 6<br>'),
(123, 1, '::1', 'INSERT', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 0, '2021-12-31 10:51:00', 'image - <br>title - <br>tag_name - <br>description - <br>ip - <br>date_added - <br>preference - <br>'),
(124, 1, '::1', 'INSERT', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 7, '2021-12-31 10:52:59', 'image - 0<br>title - u<br>description - <p>uotuotuo</p><br>ip - ::1<br>date_added - 2021-12-31 10:52:59<br>preference - 7<br>'),
(125, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 1, '2021-12-31 10:54:08', 'Before<br>image - 1.png<br>title - MUKESH DESAI, CPA<br>ip - ::1<br>date_added - 2021-12-29 15:22:23<br>After<br>image - 1.png<br>title - Phillips & Drew(Bought out by Union Bank of Switzerland)<br>ip - ::1<br>date_added - 2021-12-29 15:22:23<br>'),
(126, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 1, '2021-12-31 11:10:11', 'Before<br>image - 1.jpg<br>title - Phillips & Drew(Bought out by Union Bank of Switzerland)<br>ip - ::1<br>date_added - 2021-12-29 15:22:23<br>After<br>image - 1.jpg<br>title - Phillips & Drew(Bought Out By Union Bank Of Switzerland)<br>ip - ::1<br>date_added - 2021-12-29 15:22:23<br>'),
(127, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 2, '2021-12-31 11:12:20', 'Before<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>After<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>'),
(128, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 2, '2021-12-31 11:12:38', 'Before<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>After<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>'),
(129, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 2, '2021-12-31 11:13:13', 'Before<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>After<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>'),
(130, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 2, '2021-12-31 11:23:43', 'Before<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>After<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>'),
(131, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 2, '2021-12-31 11:24:28', 'Before<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>After<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>'),
(132, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 2, '2021-12-31 11:25:46', 'Before<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>After<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>'),
(133, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 2, '2021-12-31 11:26:38', 'Before<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>After<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>'),
(134, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 3, '2021-12-31 11:28:05', 'Before<br>image - 3.jpg<br>title - LAUREN J. SUAREZ<br>ip - ::1<br>date_added - 2021-12-29 15:23:08<br>After<br>image - 3.jpg<br>title - LAUREN J. SUAREZ<br>ip - ::1<br>date_added - 2021-12-29 15:23:08<br>'),
(135, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 2, '2021-12-31 11:30:03', 'Before<br>image - 2.jpg<br>title - JIMMY RODRIQUEZ<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>After<br>image - 2.jpg<br>title - Solomon Roberts Inc<br>ip - ::1<br>date_added - 2021-12-29 15:22:45<br>'),
(136, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 3, '2021-12-31 11:32:27', 'Before<br>image - 3.jpg<br>title - LAUREN J. SUAREZ<br>ip - ::1<br>date_added - 2021-12-29 15:23:08<br>After<br>image - 3.jpg<br>title - Johns Hopkins Hospital<br>ip - ::1<br>date_added - 2021-12-29 15:23:08<br>'),
(137, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 4, '2021-12-31 11:37:50', 'Before<br>image - 4.jpg<br>title - ALEXANDER TAN<br>ip - ::1<br>date_added - 2021-12-29 15:23:29<br>After<br>image - 4.jpg<br>title - Chase Manhattan Bank<br>ip - ::1<br>date_added - 2021-12-29 15:23:29<br>'),
(138, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 4, '2021-12-31 11:38:36', 'Before<br>image - 4.jpg<br>title - Chase Manhattan Bank<br>ip - ::1<br>date_added - 2021-12-29 15:23:29<br>After<br>image - 4.jpg<br>title - Chase Manhattan Bank<br>ip - ::1<br>date_added - 2021-12-29 15:23:29<br>'),
(139, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 5, '2021-12-31 11:40:33', 'Before<br>image - 5.jpg<br>title - FRED MORRIS, LUTCF<br>ip - ::1<br>date_added - 2021-12-29 15:23:55<br>After<br>image - 5.jpg<br>title - Commercial Bank of Kuwait<br>ip - ::1<br>date_added - 2021-12-29 15:23:55<br>'),
(140, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 6, '2021-12-31 11:42:53', 'Before<br>image - 6.jpg<br>title - WALY DOLATY, ESQ<br>ip - ::1<br>date_added - 2021-12-29 15:24:21<br>After<br>image - 6.jpg<br>title - Banco Mercantil Y Agricola<br>ip - ::1<br>date_added - 2021-12-29 15:24:21<br>'),
(141, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 6, '2021-12-31 11:43:22', 'Before<br>image - 6.jpg<br>title - Banco Mercantil Y Agricola<br>ip - ::1<br>date_added - 2021-12-29 15:24:21<br>After<br>image - 6.jpg<br>title - Banco Mercantil Y Agricola<br>ip - ::1<br>date_added - 2021-12-29 15:24:21<br>');
INSERT INTO `suarezrem_adminlog` (`id`, `actionid`, `ip`, `actiontype`, `phpself`, `tablename`, `tableid`, `date`, `notes`) VALUES
(142, 1, '::1', 'INSERT', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 8, '2021-12-31 11:51:14', 'image - <br>title - Standard Oil Company, Indiana (Amoco)<br>description - <p>\r\n<div>\r\n<div><strong>Tenant: Standard Oil Company, Indiana (Amoco)</strong></div>\r\n<div><strong>Transaction Size:</strong> 2,500 Sq. Ft.</div>\r\n<div><strong>Building:</strong> 30 Rockefeller Plaza</div>\r\n<div><strong>Owner: </strong>Rockefeller Center</div>\r\n</div>\r\n</p><br>ip - ::1<br>date_added - 2021-12-31 11:51:14<br>preference - 7<br>'),
(143, 1, '::1', 'INSERT', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 7, '2021-12-31 11:52:44', 'image - <br>title - fgfg<br>description - <p>fghfgh</p><br>ip - ::1<br>date_added - 2021-12-31 11:52:44<br>preference - 7<br>'),
(144, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 7, '2021-12-31 11:55:01', 'Before<br>image - <br>title - fgfg<br>ip - ::1<br>date_added - 2021-12-31 11:52:44<br>After<br>image - <br>title - Fgfg<br>ip - ::1<br>date_added - 2021-12-31 11:52:44<br>'),
(145, 1, '::1', 'UPDATE', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 7, '2021-12-31 11:55:53', 'Before<br>image - <br>title - Fgfg<br>ip - ::1<br>date_added - 2021-12-31 11:52:44<br>After<br>image - <br>title - Standard Oil Company, Indiana (Amoco)<br>ip - ::1<br>date_added - 2021-12-31 11:52:44<br>'),
(146, 1, '::1', 'INSERT', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 8, '2021-12-31 11:58:05', 'image - <br>title - UBAF Arab American Bank<br>description - <p>&nbsp;</p>\r\n<div>\r\n<div><strong>Tenant: UBAF Arab American Bank</strong></div>\r\n<div><strong>Transaction Size: </strong>60,000 Sq. Ft.</div>\r\n<div><strong>Building: </strong>40 East 52nd St</div>\r\n<div><strong>Owner: </strong>Rudin Management</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p><br>ip - ::1<br>date_added - 2021-12-31 11:58:05<br>preference - 8<br>'),
(147, 1, '::1', 'INSERT', '/suarezrem/admin/development_manage.php', 'suarezrem_development', 9, '2021-12-31 12:02:19', 'image - <br>title - EF Hutton<br>description - <p>&nbsp;</p>\r\n<div>\r\n<div><strong>Tenant: EF Hutton</strong></div>\r\n<div><strong>Transaction Size: </strong>660,000 Sq. Ft.</div>\r\n<div><strong>Building:</strong> 31 West 5nd Street</div>\r\n<div><strong>Owner: </strong>Gerald D Hines Interests</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p><br>ip - ::1<br>date_added - 2021-12-31 12:02:19<br>preference - 9<br>'),
(148, 1, '::1', 'INSERT', '/suarezrem/admin/gallery_manage.php', 'suarezrem_gallery', 0, '2021-12-31 12:28:12', 'title - <br>image - <br>ip - <br>date_added - <br>preference - <br>'),
(149, 1, '::1', 'UPDATE', '/suarezrem/admin/gallery_manage.php', 'suarezrem_gallery', 3, '2021-12-31 12:37:07', 'Before<br>title - lafashionista 3<br>image - 3.jpg<br>ip - 137.97.85.161<br>date_added - 2018-07-26 10:44:10<br>After<br>title - <strong>Luis Suarez at a NYC Municipal (City Owned<br>image - 3.jpg<br>ip - ::1<br>date_added - 2018-07-26 10:44:10<br>'),
(150, 1, '::1', 'INSERT', '/suarezrem/admin/gallery_manage.php', 'suarezrem_gallery', 0, '2021-12-31 12:42:18', 'title - <br>image - <br>ip - <br>date_added - <br>preference - <br>'),
(151, 1, '::1', 'INSERT', '/suarezrem/admin/gallery_manage.php', 'suarezrem_gallery', 4, '2021-12-31 13:09:33', 'title -  tytyeryeryr<br>image - <br>ip - ::1<br>date_added - 2021-12-31 13:09:33<br>preference - 4<br>');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_advantage`
--

CREATE TABLE `suarezrem_advantage` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_advantage`
--

INSERT INTO `suarezrem_advantage` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'ADVANTAGE SUAREZ', '1.jpg', '<p>We believe commercial tenants maximize their negotiating leverage with landlords by setting up a bidding matrix wherein landlords compete for the tenant in question by offering generous landlord concessions and alluring tenants with competitive rents and fair rent escalations. Regrettably, in the initial phases of any commercial lease negotiations, the outcome is unavoidably a zero-sum game. We say the initial phases are zero-sum because as a transaction is being negotiated, every business term must be evaluated by both sides. A business benefit to the landlord is a commensurate disadvantage to the tenant and visa-versa. This is precisely why it is fundamentally important for a tenant to insist the broker define its fiduciary allegiance. It is our opinion that selecting a local boutique brokerage firm is the best course of action for any commercial tenant, particularly in New York. Consider that a tenant is active in a given market for a relatively brief period of time during the office space search and when the project is completed is out of the market for the duration of the lease term which often spans a decade or more; putting aside the possible need for expansion or contraction which can sometimes arise.<br /><br /> While it is understandable to want the services of a global or national firm, particularly if a tenant has multiple locations spread out over a wide geographic area; it does not diminish the importance of avoiding conflicts of interest that a broker may encounter. It is not possible to serve two masters. New York state law allows for dual representation, so long as there is full disclosure and acceptance of such dual agency by the tenant and landlord but it is nonetheless important to recognize that global or national realtors generate significant revenue from landlords year in and year out by serving the interests of their client landlords.<br /><br /> Conversely, tenant rep brokers serve only the interests of tenants. Tenants looking for office space are well advised to recognize it is the landlord and the lease document itself that deserve the tenant\'s fullest scrutiny with the assistance of a local expert that is free of any conflict. We also strongly suggest that your legal advisor (for the lease negotiations) is an expert in commercial leases and real estate.<br /><br /> A commercial lease (be it a capital or expense lease) has an important impact on a company&rsquo;s financial statement for a long period. At SuarezREM, we use the strength of a company&rsquo;s financial statements to entice landlords to make the most competitive effort to induce our client to sign a lease for space in their property.<br /><br /> We are of the opinion that a tenant with strong financials adds tangible value to a property in any future financing, any future disposition of a property and in the properties underlying capitalization.<br /><br /> Each lease either adds or subtracts from the underlying financial strength of a property and its cash flow from operations. At SuarezREM, we leverage the financial strength of our clients in negotiations to obtain the most favorable business terms and conditions.</p>', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_contactus`
--

CREATE TABLE `suarezrem_contactus` (
  `id` bigint(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(300) NOT NULL,
  `email` varchar(250) NOT NULL,
  `message` longtext NOT NULL,
  `ip` varchar(250) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_contactus`
--

INSERT INTO `suarezrem_contactus` (`id`, `name`, `phone`, `email`, `message`, `ip`, `date_added`, `preference`, `status`) VALUES
(2, 'Rajeev', '8891635880', 'meetrajeev@gmail.com', 'aSfdgsfhgjf', '127.0.0.1', '2018-09-17 20:48:12', 0, 1),
(3, 'test', '13124443', 'test@gmail.com', 'test', '::1', '2021-09-14 15:40:14', 1, 1),
(4, 'test 2', '13124443', 'test@gmail.com', 'For testing', '::1', '2021-09-14 15:49:26', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_cont_address`
--

CREATE TABLE `suarezrem_cont_address` (
  `id` bigint(11) NOT NULL,
  `phone` varchar(300) NOT NULL,
  `address` longtext NOT NULL,
  `address_title` varchar(300) NOT NULL,
  `fax` varchar(300) NOT NULL,
  `mobno1` varchar(300) NOT NULL,
  `mobno2` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_cont_address`
--

INSERT INTO `suarezrem_cont_address` (`id`, `phone`, `address`, `address_title`, `fax`, `mobno1`, `mobno2`, `email`, `date_added`) VALUES
(4, '(646)727.4833', 'Suarez Real Estate Management\r\n295 Madison Avenue\r\nNew York, NY 10017', 'Suarezrem', '', '', '', 'luis@suarezrem.com', '2021-12-29 15:30:22');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_development`
--

CREATE TABLE `suarezrem_development` (
  `id` bigint(11) NOT NULL,
  `image` varchar(300) NOT NULL,
  `title` varchar(300) NOT NULL,
  `description` longtext NOT NULL,
  `ip` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_development`
--

INSERT INTO `suarezrem_development` (`id`, `image`, `title`, `description`, `ip`, `date_added`, `preference`, `status`) VALUES
(1, '1.jpg', 'Phillips & Drew(Bought Out By Union Bank Of Switzerland)', '<b>Tenant:&nbsp;Solomon Roberts Inc</b><br>\r\n<b>Transaction Size:&nbsp;</b>5,700 Sq. Ft.<br>\r\n<b>Building:&nbsp;</b>610 Fifth Avenue<br>\r\n<b>Owner:&nbsp;</b>Rockefeller Center<br>', '::1', '2021-12-29 15:22:23', 1, 1),
(2, '2.jpg', 'Solomon Roberts Inc', '<div><strong>Tenant: Solomon Roberts Inc</strong></br>\r\n<strong>Transaction Size:</strong> 5,700 Sq. Ft.</br>\r\n<strong>Building:</strong> 610 Fifth Avenue</br>\r\n<strong>Owner:</strong> Rockefeller Center</div>\r\n', '::1', '2021-12-29 15:22:45', 2, 1),
(3, '3.jpg', 'Johns Hopkins Hospital', '<div><strong>Tenant: Johns Hopkins Hospital</strong></div>\r\n<div><strong>Transaction Size: </strong>100,000 Sq. Ft.</div>\r\n<div><strong>Building: </strong>Asian Olympic Tower</div>\r\n<div><strong>Owner:</strong> United Resources Kuwait</div>\r\n\r\n', '::1', '2021-12-29 15:23:08', 3, 1),
(4, '4.jpg', 'Chase Manhattan Bank', '<div><strong>Tenant: Chase Manhattan Ban&nbsp;</strong></div>\r\n<div><strong>Transaction Size:</strong> $ 3 million sale to Loew&rsquo;s Corp. of Chase Branch&nbsp;</div>\r\n<div><strong>Building:</strong> 857, 10th Avenue</div>', '::1', '2021-12-29 15:23:29', 4, 1),
(5, '5.jpg', 'Commercial Bank of Kuwait', '<div><strong>Tenant: Commercial Bank of Kuwait</strong></div>\r\n<div><strong>Transaction Size: </strong>5,000 Sq. Ft.</div>\r\n<div><strong>Building: </strong>1180 Avenue of the Americas</div>\r\n<div><strong>Owner: </strong>Murray Hill Properties</div>\r\n\r\n', '::1', '2021-12-29 15:23:55', 5, 1),
(6, '6.jpg', 'Banco Mercantil Y Agricola', '<div><strong>Tenant: Banco Mercantil Y Agricola</strong></div>\r\n<div><strong>Transaction Size: </strong>8,500 Sq. Ft.</div>\r\n<div><strong>Building: </strong>450 park Avenue</div>\r\n<div><strong>Owner:</strong> Peter Sharp &amp; Co Inc.</div>', '::1', '2021-12-29 15:24:21', 6, 1),
(7, '7.jpg', 'Standard Oil Company, Indiana (Amoco)', '<div><strong>Tenant: Standard Oil Company, Indiana (Amoco)</strong></div>\r\n<div><strong>Transaction Size: </strong>2,500 Sq. Ft.</div>\r\n<div><strong>Building: </strong>30 Rockefeller Plaza</div>\r\n<div><strong>Owner: </strong>Rockefeller Center</div>\r\n<p>&nbsp;</p>', '::1', '2021-12-31 11:52:44', 7, 1),
(8, '8.jpg', 'UBAF Arab American Bank', '<div><strong>Tenant: UBAF Arab American Bank</strong></div>\r\n<div><strong>Transaction Size: </strong>60,000 Sq. Ft.</div>\r\n<div><strong>Building: </strong>40 East 52nd St</div>\r\n<div><strong>Owner: </strong>Rudin Management</div>\r\n', '::1', '2021-12-31 11:58:05', 8, 1),
(9, '9.jpg', 'EF Hutton', '\r\n<div><strong>Tenant: EF Hutton</strong></div>\r\n<div><strong>Transaction Size: </strong>660,000 Sq. Ft.</div>\r\n<div><strong>Building:</strong> 31 West 5nd Street</div>\r\n<div><strong>Owner: </strong>Gerald D Hines Interests</div>\r\n</div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', '::1', '2021-12-31 12:02:19', 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_gallery`
--

CREATE TABLE `suarezrem_gallery` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `ip` varchar(300) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `preference` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_gallery`
--

INSERT INTO `suarezrem_gallery` (`id`, `title`, `image`, `date_added`, `ip`, `status`, `preference`) VALUES
(1, '<strong>Luis Suarez with another born and raised New Yorker, Colin Luther Powell - 65th United States Secretary of State, 12th Chairman of the Joint Chiefs of Staff, 16th United States National Security Advisor.\r\n</strong>', '1.jpg', '2017-10-04 21:14:21', '137.97.9.184', 1, 1),
(2, '<strong>Luis Suarez with Michael Bloomberg, erstwhile Mayor of New York</strong>', '2.jpg', '2018-02-02 01:24:05', '137.97.151.235', 1, 2),
(3, '<strong>Luis Suarez at a NYC Municipal (City Owned) golf course. He holds a single digit handicap. <br> A scratch golfer, Luis loves playing the New York City municipal courses; especially Pelham/Split Rock in the Bronx owned by the City of New York and the New York State owned golf courses; especially the world renowned Bethpage Black in Long Island and the Robert Trent Jones designed Montauk Downs;  these golf courses offer an incredible value for golf lovers.</strong>', '3.jpg', '2018-07-26 10:44:10', '::1', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_homeimage`
--

CREATE TABLE `suarezrem_homeimage` (
  `id` bigint(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `image` varchar(300) NOT NULL,
  `description` longtext NOT NULL,
  `date_added` datetime NOT NULL,
  `ip` varchar(300) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `preference` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_homeimage`
--

INSERT INTO `suarezrem_homeimage` (`id`, `title`, `image`, `description`, `date_added`, `ip`, `status`, `preference`) VALUES
(1, 'Home Image 1', '1.jpg', 'Home Image Description 1', '2017-10-04 21:14:21', '137.97.9.184', 1, 49),
(2, 'Home Image 2', '2.jpg', 'Home Image Description 2', '2018-02-02 01:24:05', '137.97.151.235', 1, 50),
(3, 'Home Image 3', '3.jpg', 'Home Image Description 3', '2018-07-26 10:44:10', '137.97.85.161', 1, 53),
(4, 'Home Image 4', '4.jpg', 'Home Image Description 4', '2018-07-26 10:45:10', '137.97.85.161', 1, 54);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_newyork`
--

CREATE TABLE `suarezrem_newyork` (
  `id` bigint(11) NOT NULL,
  `image` varchar(300) NOT NULL,
  `title` varchar(300) NOT NULL,
  `description` longtext NOT NULL,
  `ip` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_newyork`
--

INSERT INTO `suarezrem_newyork` (`id`, `image`, `title`, `description`, `ip`, `date_added`, `preference`, `status`) VALUES
(1, '1.jpg', 'THE HISTORY', 'In the precolonial era, the area of present-day New York City was home to Algonquian Native Americans, including the Lenape. Their homeland, known as Lenapehoking, included Staten Island, Manhattan, the Bronx, the western portion of Long Island, including the areas that would later become the boroughs of Brooklyn and Queens, and the Lower Hudson Valley.</span><br /><br /><span>Colonial era New York City traces its origins to a trading post founded on the southern tip of Manhattan Island by Dutch colonists in around 1624. The settlement was named New Amsterdam in 1626 and was chartered as a city in 1653. The city came under English control in 1664 and was renamed New York after King Charles II of England granted the lands to his brother, the Duke of York.</span><br /><br /><span>The city was regained by the Dutch in July 1673 and was renamed New Orange for one year and three months; the city has been continuously named New York since November 1674. New York City was the capital of the United States from 1785 until 1790, and has been the largest U.S. city since 1790.</span><br /><br /><span>The Statue of Liberty greeted millions of immigrants as they came to the United States by ship in the late 19th and early 20th centuries, and is a symbol of the ideals of liberty and peace.</span><br /><br /><span>By the 21st century, New York has emerged as a global node of creativity, entrepreneurship, and environmental sustainability, and as a symbol of freedom and cultural diversity. In 2019, New York was voted the greatest city in the world per a survey of over 30,000 people from 48 cities worldwide, citing in large part, its cultural diversity.', '::1', '2021-12-29 16:25:39', 1, 1),
(2, '2.jpg', 'THE CENTRE OF THE WORLD', 'New York is one of the world\'s most populous megacities and is described as the cultural, financial, and media capital of the world, significantly influencing commerce, entertainment, research, technology, education, politics, tourism, dining, art, fashion, and sports, and is the most photographed city in the world. Many districts and monuments in New York City are major landmarks, including some of the world\'s ten most visited tourist attractions.</span><br /><br /><span>A record 66.6 million tourists visited New York City in 2019, with Times Square being one of the world\'s busiest pedestrian intersections, and a center of the world\'s entertainment industry. (Luis Suarez, our Founder and CEO was an integral member of the team that worked on the Times Square Re-Development Project with Park Realty and George Klein.</span><br /><br /><span>Many of the city\'s landmarks, skyscrapers, and parks are known around the world. The Empire State Building has become the global standard of reference to describe the height and length of other structures. Manhattan\'s real estate market is amongst the most expensive and exclusive in the world.</span><br /><br /><span>The New York City Subway is the largest single-operator rapid transit system worldwide, with 472 rail stations. The city has over 120 colleges and universities, including the prestigious Columbia University, New York University, Rockefeller University, and the City University of New York system, which is the largest urban public university system in the United States.</span><br /><br /><span>Anchored by Wall Street in the Financial District of Lower Manhattan, New York City has been called both the world\'s leading financial center and the most financially powerful city in the world. It is home to the world\'s two largest stock exchanges by total market capitalization, the New York Stock Exchange and NASDAQ.', '::1', '2021-12-29 16:28:15', 2, 1),
(3, '3.jpg', 'HOME TO THE BEST', 'New York city is home to the world&rsquo;s best restaurants, incredible museums with significant collections of all types. It has the best art galleries, best restaurants, best hospitals and best public transportation system in the world.</span><br /><br /><span>New York is home to great and historic teams such as the NY Jets, New York Giants, New York Mets, New York Yankees, New York Knickerbockers, the New York Rangers and the New York Islanders. Some of the best jazz clubs in the world find their home here. New York is where the top national and global talent in virtually every sector come to find opportunity and employment.<br/><br/>\r\n<div class="col-md-12 boxs-relative">\r\n<div class="headingUnderline"><h3 class="boxs-title"><b>Why New York is the greatest</b> </h3></div>\r\n<strong>&bull;&nbsp;Tap Water</strong>\r\n<div style="padding-left: 5%;">\r\n<p class="textJustification">New York City\'s tap water received first-place honors at the annual statewide taste test, held at the New York State Fair this August.<br>\r\n </p>\r\n</div>\r\n<strong>&bull;&nbsp;World Class Local Museums - affordable and easily accessible.</strong>\r\n<div style="padding-left: 5%;">\r\n<p class="textJustification">With the new <a href="https://www.culturepass.nyc/" style="color: blue;">Culture Pass </a>program, New York Public Library, Queens Library and Brooklyn Library cardholders get free access to dozens of the world\'s best museums.<br>\r\n</p>	  \r\n</div>\r\n<strong>&bull;&nbsp;Free concert every day</strong><br>\r\n<div style="padding-left: 5%">\r\n<p class="textJustification">All summer long, BRIC and SummerStage throw gratis gigs in the city\'s parks, and year-round venues 	like Jalopy Theatre and Union Pool host no-cover nights.<br>\r\n								</p>\r\n							</div>\r\n					<strong>&bull;&nbsp;King County\'s giant new park</strong>\r\n							<div style="padding-left: 5%">\r\n								<p class="textJustification">New York is expanding its park system with a 407-acre green space. The park will open in Brooklyn by 	2022.<br>\r\n							</p>\r\n							</div>\r\n					<strong>&bull;&nbsp;Skyscaper Museum</strong>\r\n							<div style="padding-left: 5%">\r\n								<p class="textJustification">Widely recognized around the world, the cityscape is a trademark of New York city. Watching iconic 		skyscrapers like the Empire State Building glisten at sunset can bring a tear to even a jaded New Yorker\'s eye. All this many-storied awesomeness is appreciated at the Skyscraper Museum.<br>\r\n								</p>\r\n							</div>\r\n					<strong>&bull;&nbsp;Rent a super cheap 100% electic moped</strong>\r\n							<div style="padding-left: 5%">\r\n								<p class="textJustification">The Brooklyn start-up <a href="https://gorevel.com/" target="_blank" style="color: blue;">Revel</a> lets you rent electric mopeds in NYC, starting at $4. Enjoy the sustainable 	ride-shares, mopeds, E-bikes and of course the superhubs.<br>\r\n								</p>\r\n							</div>\r\n					<strong>&bull;&nbsp;New York\'s indie-bookstore scene is on the upswing.</strong>\r\n							<div style="padding-left: 5%">\r\n								<p class="textJustification">Local favourite <a href="https://www.timeout.com/newyork/shopping/books-are-magic" target="_blank" style="color: blue;">Books Are Magic</a> is thriving in Carroll Gardens and old favorites<a href="https://www.bookculture.com/" target="_blank" style="color: blue;">Book Culture</a>  and <a href="https://www.mcnallyjackson.com/" target="_blank" style="color: blue;">McNally Jackson</a>have even opened new sister locations this year in Long Island City and Williamsburg.<br>\r\n								</p>\r\n							</div>\r\n					<strong>&bull;&nbsp;Hayden Planetarium Space Theater</strong>\r\n							<div style="padding-left: 5%">\r\n								<p class="textJustification">An integral part of the American Museum of Natural History. This is a favorite of ours.\r\n								</p>\r\n							</div>\r\n\r\n<!--							</p>-->\r\n		</div>', '::1', '2021-12-29 16:31:32', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_office_location`
--

CREATE TABLE `suarezrem_office_location` (
  `id` bigint(11) NOT NULL,
  `latitude` varchar(300) NOT NULL,
  `longitude` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_office_location`
--

INSERT INTO `suarezrem_office_location` (`id`, `latitude`, `longitude`, `date_added`) VALUES
(1, '40.75168', '-73.97955', '2015-07-14 23:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_services`
--

CREATE TABLE `suarezrem_services` (
  `id` bigint(11) NOT NULL,
  `image` varchar(300) NOT NULL,
  `title` varchar(300) NOT NULL,
  `description` longtext NOT NULL,
  `ip` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_services`
--

INSERT INTO `suarezrem_services` (`id`, `image`, `title`, `description`, `ip`, `date_added`, `preference`, `status`) VALUES
(1, '1.jpg', 'BRANDING STRATEGY', 'A branding strategy is a long-term plan for the development of a successful brand to achieve specific goals, which directly links to consumer needs and demands, perception and competitive environments. It also has the power to amplify a brand amongst its competitors and stand out on the market.</span><br /><span>At Suarez SEM, we can set up your brand or organization in a fiercely competitive market, with years of branding development experience powered by AI based market intelligence platforms.', '::1', '2021-12-30 12:46:30', 1, 1),
(2, '4.jpg', 'CORPORATE ID DEVELOPMENT', 'A strategic corporate identity boosts the company&rsquo;s worth and helps it expand. It builds trust and loyalty since customers tend to trust established companies. It brings in more sales as more and more users come to know your business. Suarez REM helps develop your corporate identity by capitalizing on the concept of brand development.</span><br /><span>Corporate ID design services by Suarez REM includes Concept Development, Brand Identity Design, Packaging Design, Brand Guidelines and Collateral Design.', '::1', '2021-12-30 12:46:58', 2, 1),
(3, '3.jpg', 'WEBSITE DESIGN', 'The website is the most important digital real estate asset for any brand, entity or organization. A well-designed website based on AI intelligence can reach the right audience and help create a good impression about your brand to your customers. It can nurture your leads, and get more conversions. Our website design team has a wide range of experience, developing engaging websites for clients across the world.', '::1', '2021-12-30 12:47:15', 3, 1),
(4, '2.jpg', 'SOCIAL MEDIA CONTENT AND MARKETING', 'Social media networks are open to all, giving every business a chance to follow their consumers\' activities or potential buyers. This helps brand owners to be more informed about their target audience, their likes, dislikes, and their interests so that they can create an engaging environment that appeals to their ideal customers. Our Social Media Team can help you connect with your Target Audience by creating appealing content including videos, posts, blogs, vlogs and stories to ensure that they always have you on top of their minds, when they make their buying decisions.', '::1', '2021-12-30 12:47:32', 4, 1),
(5, '5.jpg', 'DIGITAL MARKETING', 'Digital marketing, also called online marketing, is the promotion of brands to connect with potential customers using the internet and other forms of digital communication.</span><br /><br /><b><span>&bull;&nbsp;</span><span>Social Media Contenting</span></b><br /><b><span>&bull;&nbsp;</span><span>Google AdWords</span></b><br /><b><span>&bull;&nbsp;</span><span>SEO</b>', '::1', '2021-12-30 12:48:00', 5, 1),
(6, '6.jpg', 'WEB HOSTING', 'Web hosting is an online service that allows you to publish your website files onto the internet. So, anyone who has access to the internet has access to your website.</span><br /><br /><span><b>&bull;&nbsp;</span><span>Email Marketing</span></b>', '::1', '2021-12-30 12:48:17', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_team`
--

CREATE TABLE `suarezrem_team` (
  `id` bigint(11) NOT NULL,
  `image` varchar(300) NOT NULL,
  `title` varchar(300) NOT NULL,
  `tag_name` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `ip` varchar(300) NOT NULL,
  `date_added` datetime NOT NULL,
  `preference` bigint(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active 0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suarezrem_team`
--

INSERT INTO `suarezrem_team` (`id`, `image`, `title`, `tag_name`, `description`, `ip`, `date_added`, `preference`, `status`) VALUES
(1, '1.png', 'MUKESH DESAI, CPA', 'Non-executive Board Member', '<p><span>Mukesh is a non-executive board member of Suarez REM. As our Chief Financial Analyst, he guides the company by organizing information, consolidating reports, and reviewing all non-legal pertinent information about prospective deals. He examines the feasibility of every deal and is a hands-on member of the team, helping us with cost reduction, accounting, reconciliations, advising clients, assisting auditors, networking with industry professionals, reviewing budgets, checking the integrity of data, forecasting, helping the business in its investments and lease analysis and rental expense projections.</span></p>', '::1', '2021-12-29 15:22:23', 1, 1),
(2, '1.png', 'JIMMY RODRIQUEZ', 'Chief Operating Officer', '<p><span>As COO of Suarez REM, Jimmy oversees the day-to-day operations of the office. He designs and implements business operations, establish policies that promote company culture and vision, and oversee operations of the company and the work of executives. He is responsible for preparing budgets for assignments undertaken by the company for clients.</span></p>', '::1', '2021-12-29 15:22:45', 2, 1),
(3, '1.png', 'LAUREN J. SUAREZ', 'Director, Administration', 'Lauren works as Administrative Manager to the team and is responsible for tenant searches and market data provided to clients of lease availabilities. She is also responsible for preparing and executing operating budgets and also oversees coordination of Suarez REM&rsquo;s administration and general workflow\n', '::1', '2021-12-29 15:23:08', 3, 1),
(4, '1.png', 'ALEXANDER TAN', 'In-house Developer', 'Alexander is the in-house developer specializing in sale-lease back transactions. Using the capital of Tan International and Suarez REM, he facilitates sale-leaseback transactions for clients who deem a sale-leaseback as a favorable strategy for its long term occupancy.', '::1', '2021-12-29 15:23:29', 4, 1),
(5, '1.png', 'FRED MORRIS, LUTCF', 'Managing Director', 'As managing director, Fred leads the department that works with Suarez&rsquo; clients to arrange office insurance coverage which lists the landlord as an additionally insured party and is a customary requirement for office tenants in commercial leases. Fred is committed to sourcing and obtaining the lowest premiums while selecting the highest rated carriers possible. Fred undertakes a rigorous analysis of the insurance clause and its listed perils and offers solutions to common as well as complex underwriting issues surrounding the commercial lease clauses contained in the leases for major buildings.', '::1', '2021-12-29 15:23:55', 5, 1),
(6, '3.jpg', 'WALY DOLATY, ESQ', 'Managing Director', 'Waly is an international corporate and investment lawyer with in depth knowledge and experience in banking, tax planning, and Islamic finance. He has held various senior positions in legal and compliance departments. He has held the position of chief legal counsel for multinational investment corporations, banks, and financial institutions including at the law firm of Baker &amp; McKenzie, and Burgan Bank, Kuwait. He has held the position of assistant lecturer at Cairo University where he taught corporate law and headed the legal department at the University. He is the company&rsquo;s in house legal advisor on matters related to the company and its activities.', '::1', '2021-12-29 15:24:21', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_webelive`
--

CREATE TABLE `suarezrem_webelive` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_webelive`
--

INSERT INTO `suarezrem_webelive` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'WE BELIEVE', '1.jpg', 'We believe that New York is the best place in the world to live and work. It is surrounded by world class suburbs, the world renowned Hamptons and other world class weekend escapes.', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_wesee`
--

CREATE TABLE `suarezrem_wesee` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_wesee`
--

INSERT INTO `suarezrem_wesee` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'We See', '1.jpg', 'We know a good deal for our client when we see one. Because we have an in-depth knowledge of their requirements, tastes and budgets; supported by the best data collection capability in the market.', '2010-11-15 21:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `suarezrem_weserve`
--

CREATE TABLE `suarezrem_weserve` (
  `id` bigint(11) NOT NULL,
  `title` longtext NOT NULL,
  `image` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suarezrem_weserve`
--

INSERT INTO `suarezrem_weserve` (`id`, `title`, `image`, `content`, `date_added`) VALUES
(1, 'We Serve', '1.jpg', 'We serve the commercial tenants of New York. We believe it is not possible to one day represent the interests of landlords and the next, the interests of tenants without creating divided loyalties. We are tenant representation brokers, thereby avoiding conflicts of interest that can arise when representing both landlords and tenants.', '2010-11-15 21:36:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bw_adminlog`
--
ALTER TABLE `bw_adminlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_admin`
--
ALTER TABLE `suarezrem_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_adminlog`
--
ALTER TABLE `suarezrem_adminlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_advantage`
--
ALTER TABLE `suarezrem_advantage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_contactus`
--
ALTER TABLE `suarezrem_contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_cont_address`
--
ALTER TABLE `suarezrem_cont_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_development`
--
ALTER TABLE `suarezrem_development`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_gallery`
--
ALTER TABLE `suarezrem_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_homeimage`
--
ALTER TABLE `suarezrem_homeimage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_newyork`
--
ALTER TABLE `suarezrem_newyork`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_office_location`
--
ALTER TABLE `suarezrem_office_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_services`
--
ALTER TABLE `suarezrem_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_team`
--
ALTER TABLE `suarezrem_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_webelive`
--
ALTER TABLE `suarezrem_webelive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_wesee`
--
ALTER TABLE `suarezrem_wesee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suarezrem_weserve`
--
ALTER TABLE `suarezrem_weserve`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bw_adminlog`
--
ALTER TABLE `bw_adminlog`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suarezrem_admin`
--
ALTER TABLE `suarezrem_admin`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suarezrem_adminlog`
--
ALTER TABLE `suarezrem_adminlog`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT for table `suarezrem_contactus`
--
ALTER TABLE `suarezrem_contactus`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suarezrem_cont_address`
--
ALTER TABLE `suarezrem_cont_address`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suarezrem_development`
--
ALTER TABLE `suarezrem_development`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `suarezrem_gallery`
--
ALTER TABLE `suarezrem_gallery`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `suarezrem_homeimage`
--
ALTER TABLE `suarezrem_homeimage`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suarezrem_newyork`
--
ALTER TABLE `suarezrem_newyork`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `suarezrem_office_location`
--
ALTER TABLE `suarezrem_office_location`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suarezrem_services`
--
ALTER TABLE `suarezrem_services`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `suarezrem_team`
--
ALTER TABLE `suarezrem_team`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
