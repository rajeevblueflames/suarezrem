<?php
	require_once 'init.php';err_status("init.php included");

	$webelive_arr			=	$cls_db->getdbcontents_sql("select * from suarezrem_webelive");
	$weserve_arr			=	$cls_db->getdbcontents_sql("select * from suarezrem_weserve");
	$wesee_arr				=	$cls_db->getdbcontents_sql("select * from suarezrem_wesee");
	$team_arr			 	=	$cls_db->getdbcontents_sql("select * from suarezrem_team");
	$development_arr		=	$cls_db->getdbcontents_sql("select * from suarezrem_development");
    $office_Address        	=   $cls_db->getdbcontents_sql("select * from suarezrem_cont_address");
    $office_location        =   $cls_db->getdbcontents_sql("select * from suarezrem_office_location");

$cont_add_arr	=	$cls_db->getdbcontents_sql("Select * from suarezrem_cont_address");
$email_address	=	$cont_add_arr[0]['email'];

    if(isset($_POST["submit"]))
	{
		$ip 					=	$_SERVER['REMOTE_ADDR'];
		$date_added				=	"escape now() escape";
		foreach($_POST as $key	=>	$val) if(!is_array($val))	 $_POST[$key]	=	trim($val);extract($_POST);
		if($name == "" || $email == "")  $_SESSION["sess_err"]	=	"Please enter all details correctly";
		else
			{
			$pref_cnt	=	$cls_db->getdbcontents_sql("SELECT max(`preference`)+1 as maxpref FROM suarezrem_contactus ");
				$pref_cnt	=	$pref_cnt[0]["maxpref"];	
			
				$field		=	"name,email,phone,message,ip,date_added,preference";
				$val		=	"name,email,phone,message,ip,date_added,pref_cnt";
              
				//$ins="insert into suarezrem_contactus($field) values('$name','$email','$phone','$message','$ip','$date_added','$pref_cnt')";
                
				$ro_id		=	$cls_db->db_insert("suarezrem_contactus",$field,$val);	
				$_SESSION["sess_err"]	=	"Details sent successfully";
				
							
				$message_admin	=	"
									Name 	: $name <br><br>
									Email 	: $email <br><br>
                                    Phone   : $phone
									Message : $message 
								";

                $message_user	=	"Your enquiry has been sent successfully, Our team will contact you shortly....";
				
				//echo "<br>".$connect_address;echo "<br>".$message;exit;
				
				$cls_site->sendmail($email_address,$email,"Suarezrem - Contact Message",$message_admin);
				$cls_site->sendmail($email,$email_address,"Suarezrem - Thanks Message",$message_user);
				$_SESSION["sess_err"]	=	"Your request has been sent successfully, Our team will contact you shortly.";
				echo '	<script>alert("Your request has been sent successfully, Our team will contact you shortly.");</script>
				<meta HTTP-EQUIV="REFRESH" content="0; url=index.php">';
				
				//header("location:/contactus.php");
			}
		
	}

	header_view("Suarezrem- Home");err_status("header included");

?>
		<!--VIDEO HOME START-->
	<div class="slider-box clearfix">
		<div class="slider home-slider clearfix" data-auto-play="8000">

			<video class="desktop" autoplay loop muted width="100%" playsinline class="video-background">
			  <source src="videos/Suarez_Real_Estate_Management.mp4" type="video/mp4">
			</video>

			<video class="mobile" autoplay loop muted width="100%" playsinline class="video-background">
				<source src="videos/suarez_vertical_video.mp4" type="video/mp4">
			  </video>

		</div><!--/.slider-->
	</div><!--/.slider-box-->
	<!--VIDEO HOME END-->

</div><!--/home end-->
<div style="padding-bottom:0px;" id="about" class=" clearfix">
	<div class="container">
		<div class="row">
			<div class="spacing40 clearfix clearboth"></div>

		    <div class="col-md-12">

				<h6 class="content-title1" style="line-height: 20px;">New York’s Premium Tenant Representative Boutique</h6>
		    </div>
			</div><!--/.col-md-8-->


			<div class="col-md-4 boxs-relative">
				<h3 class="boxs-title"><b><?php echo $webelive_arr[0]['title']; ?></b></h3>
				<img src="WeBeliveImages/<?php echo $webelive_arr[0]['image'];  ?>" >
				<p class="textJustification">
					<?php echo $webelive_arr[0]['content'];  ?>
				</p>
			</div><!--/.col-md-4-->

			<div class="col-md-4 boxs-relative">
				<h3 class="boxs-title"><b><?php echo $weserve_arr[0]['title']; ?></b></h3>
				<img src="weserveImages/<?php echo $webelive_arr[0]['image']; ?>">
				<p class="textJustification">
					<?php echo $weserve_arr[0]['content'];  ?>
				</p>
			</div><!--/.col-md-4-->

			<div class="col-md-4 boxs-relative">
				<h3 class="boxs-title"><b><?php echo $wesee_arr[0]['title'];  ?></b></h3>
				<img src="weseeImages/<?php echo $wesee_arr[0]['image']; ?>">
				<p class="textJustification">
					<?php echo $wesee_arr[0]['content'];  ?>                       
				</p>
			</div><!--/.col-md-4-->
			<hr>

			<!--/.boxs-relative-->

			<div>&nbsp;</div>
<!--
			<div class="col-md-12">
				<h3 class="content-title">About</h3>
			</div>
-->
<div style="padding-bottom:0px;" id="design" class=" clearfix">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="boxs-title1"><b>Our founder</b></h3>
			</div><!--/.col-md-8-->
			<div class=" boxs-relative clearfix" style="line-height:2.2;!important;">
				<div class="col-md-8">

				<p class="textJustification">
					Luis Suarez is a life long New Yorker and a true ambassador of the greatest city in the world. Right from the time Suarez REM was set up, his guiding principle has been to focus on being the best local commercial real estate provider to commercial tenants in the New York market.         <br>
					<br>
					Luis has no desire to turn Suarez REM into a behemoth as is the trend among many providers in the marketplace. It will remain a boutique firm with a singular focus on tenant representation. We believe it is not possible to one day represent the interest of landlords and the next, the interests of tenants without creating divided loyalties. It is not possible to serve two masters and our only focus is on New York and we advocate only for the commercial interests of our commercial tenant clients. 
				</p>	

				</div>
				<div class="col-md-4 boxs-relative" >
						<img src="images/about/1.jpg">
					&nbsp;<h3 class="" style=""><b>Luis Suarez</b>,Founder, CEO
<!--							<h4 style="text-align: right;">Founder, CEO</h4>-->
					</h3>
				</div>

				<div class="col-md-12">

					<h3><b>Experience</b></h3>
				<p class="textJustification">
						&bull; 	Approved Real Estate Instructor with the New York Department of State, Bureau of Education Standards.<br>
						&bull; 	Visiting faculty at the Broker and Salespersons Real Estate Licensing Course for the State of New York, at the REEDC Real Estate School.<br>
						&bull; 	Former Chairman of American Business Council of Kuwait which operates under the auspices of the U.S. Embassy<br>
						&bull;  Former Board Member of the Cromwell Hospital in the U.K. London’s largest private hospital<br>
						&bull; 	Former Board Member, Community Board(# 11) – Appointed by the Borough President Hon. Andrew Stein.
				</p>

				</div>

			</div><!--/.col-md-4-->
			<!--/.col-md-4-->
<!--                    <div class="spacing40 clearfix clearboth"></div>-->
		</div><!--/row-->
	</div><!--/container-->
	<hr />
</div>
</div><!--/row-->
</div><!--/container-->
		<div class="spacing40 clearfix clearboth"></div>

	<!--Development Experience Start-->
		  <div style="padding-bottom:0px;" id="development" class=" clearfix">
        	<div class="container">
            	<div class="row">
                    <div class="col-md-12">
                    	<h3 class="content-title1" style="">Transaction Resume </h3>
<!--						<h4 class="" style="text-align: center;">New York’s Premium Tenant Representative Boutique THE SUAREZ REM WAY</h4>-->
                    </div><!--/.col-md-8-->
                    
			<?php
				if($development_arr)
				{
					foreach($development_arr as $key=>$val)
					{
			?>		<div class="col-md-4 boxs-relative boxs-relative-development">
<!--                        <h3 class="boxs-title">We believe</h3>-->
                        <img src="developmentImages/<?php echo $val['image']; ?>" alt="<?php echo $val['title']; ?>">
                        	<?php echo $val['description']; ?>
                    </div><!--/.col-md-4-->
			<?php
					}
				}
			?>
        			
                  
                    <!--/.boxs-relative-->
        	</div><!--/container-->
            <hr />
        	</div>  
		</div>
		<div style="" id="team1" class="">
        	<div class="container">
            	<div class="row">
					<div class="col-md-12">
						<h3 class="content-title1" style=""><b>Team Suarez</b></h3>
					</div><!--/.col-md-8-->
					<div>&nbsp;</div>
		<?php
			if($team_arr)
			{
				foreach($team_arr as $key=>$val)
				{
		?>
			   <div class="boxs-relative clearfix">
				<div class="col-md-6 boxs-relative boxs-relative-development">
					<img src="teamImages/<?php echo $val['image']; ?>" alt="<?php echo $val['title']; ?>">
					<p>
						
					</p>
				</div>
				   <div class="container" style="background-color: #f9f5f5;">
						<div class="col-md-12 text_right">
							<div class="boxs-relative clearfix">
								<h3 class="boxs-title1"><b><?php echo $val['title']; ?></b></h3>
								<h4><b><?php echo $val['tag_name']; ?></b></h4>
								<p class="textJustification">
									<?php echo $val['description']; ?>
								</p>
							</div><!--/.bordering-->
							<div class="spacing20 clearfix"></div>
						</div><!--/.col-md-8-->
					</div>
				</div>
		<?php
				}
			}
		?>
			</div>
		</div>
	</div>
		<!--Team Ends-->	  
        <!--CONTACT START--> 
        <div id="contact" class="clearfix">
        	<div class="container">
            	<div class="row">
                    <div class="col-md-12">
                    	<h3 style="" class="content-title1">Get in touch with us</h3>
                    </div><!--/.col-md-8-->
              </div><!--/.row-->
              <div id="map_canvas" class="map_canvas">
					<iframe width="100%" height="400" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $office_location[0]['latitude']?>, <?php echo $office_location[0]['longitude']?>&amp;key=AIzaSyB1VjVpSCmyVhpOIQS2Rir-Cqgw8DpeiY4"></iframe>
			  </div>
                <!--MAP MARKER CONTENT-->
                <div class="hidden map-content">
                    <div class="box-map">
                        <img src="images/maps.jpg" alt="">
                        <h3>Our Office</h3>
                        <p><b>Address:</b>Suarezrem</p>
                        <p><b>Phone:</b> (646)727.4833 </p>
                        <p><b>Email:</b> luis@suarezrem.com</p>
                    </div>
                </div>
                <!--MAP MARKER CONTENT END-->
              <div class="row">
                    <div class="col-md-8">
                        <div id="form-wrapper">
                             <div id="form-inner">
                                 <div id="ErrResults"><!-- retrive Error Here --></div>
                                 <div id="MainResult"><!-- retrive response Here --></div>
                                 <div id="MainContent">
                                     <form id="MyContactForm" name="MyContactForm" method="post">
                                         <p class="name">
                                         <input type="text" name="name" id="name" placeholder="Your Name ...">
                                         <label for="name" id="nameLb"><span class="error">*Name Field Required</span></label>
                                         </p>
                                         <p>
                                         <input type="email" name="email" id="email" placeholder="Your Email ...">
                                         <label for="email" id="emailLb">
                                         <span class="error error1">*Email Field Required</span> 
                                         <span class="error error2">*Email Not Valid</span>
                                         </label> 
                                         </p>
                                         <p> 
                                         <input type="text" name="phone" id="phone" placeholder="Your Telephone ...">
                                         <label for="phone" id="phoneLb"><span class="error">*Telephone Field Required</span></label>
                                         </p>
                                         <p class="textarea">
                                         <textarea name="message" id="message" placeholder="Your Message ..." rows="5"></textarea>
                                         <label for="message" id="messageLb"><span class="error">*Message Field Required</span></label>
                                         </p>
                                         <div class="clearfix"></div>
                                         <a href="#" class="contact-btn">Send Message</a>
                                     </form>
                                 </div><!--MainContent-->
                             </div><!--form-inner-->
                        </div><!--form-wrapper-->
            </div>
                    
                    <div class="col-md-4 contact-box">
                    
                    	<div class="boxs-relative-contact clearfix">
                            <i class="boxs-icon fa fa-building-o"></i>
							<h3 class="boxs-title">Address</h3>
                            <p>
                               <?php echo $cls_site->lbreak($office_Address[0]['address'])?>
                               
                            </p>
                        </div><!--/.boxs-relative-->
                        
                        <div class="boxs-relative-contact clearfix">
                            <i class="boxs-icon fa fa-tty"></i>
							<h3 class="boxs-title">Telephone</h3>
                            <p>
                            	Phone:&nbsp;<?php echo $office_Address[0]['phone'];?><br>
<!--                                Fax : +971 4 4565814-->
                            </p>
                        </div><!--/.boxs-relative-->
                        
                        <div class="boxs-relative-contact clearfix">
                            <i class="boxs-icon fa fa-envelope"></i>
							<h3 class="boxs-title">Email</h3>
                            <p><a href="<?php echo $office_Address[0]['email'];?>"><?php echo $office_Address[0]['email'];?></a></p>
							<div class="spacing30"></div>
                        </div><!--/.boxs-relative-->
                        
                    </div>
                    
                </div><!--/.row-->
              
        	</div><!--/.container--> 
        </div><!--/contact-->
        <!--CONTACT END--> 
	<div class="spacing20 clearfix"></div>

<?php
	require_once("footer.php");
?>